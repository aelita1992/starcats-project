﻿using UnityEngine;
using System.Collections;

public class LerpMovementAI : MovementAI
{
    public LerpMovementAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate) { }

    Vector3 startPosition = Vector3.zero;
    Vector3 lastTargetPos = Vector3.zero;

    float timeToTarget = 0f;
    float timeElapsed = 0f;
    private void updateMovementPlan() {
        Vector3 targetVector = lastTargetPos - startPosition;
        float distance = targetVector.magnitude;
        float maxVelocityMagnitude = MaxVelocity.magnitude;

        timeToTarget = distance / ( maxVelocityMagnitude/2 );
        timeElapsed = 0;
        getNextTimePoint(true);
    }
    float easeMovement(float t)
    {
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        return t;
    }

    float getNextTimePoint(bool updateElapsed = false)
    {
        float nextElapsed = timeElapsed + (timeToTarget / 100);
        float timePoint = (2 / (1 + timeToTarget / nextElapsed));

        if (updateElapsed)
        {
            timeElapsed = nextElapsed;
        }

        return timePoint;
    }

    public override void updateAcceleration()
    {
        if (!updateState()) return;

        float timePoint = getNextTimePoint();
        Vector3 intendedPosition = Vector3.Lerp(startPosition, lastTargetPos, easeMovement(timePoint));
        Vector3 intendedChange = intendedPosition - currentPos;

        Vector3 targetVelocity = Vector3.zero;

        targetVelocity.x = Mathf.Clamp(intendedChange.x, MaxVelocity.x * -1f, MaxVelocity.x);
        targetVelocity.y = Mathf.Clamp(intendedChange.y, MaxVelocity.y * -1f, MaxVelocity.y);
        targetVelocity.z = Mathf.Clamp(intendedChange.z, MaxVelocity.z * -1f, MaxVelocity.z);

        Vector3 velocityChange = currentVelocity - targetVelocity;

        currentAccel.x = Mathf.Clamp(velocityChange.x, MaxAccel.x * -1f, MaxAccel.x);
        currentAccel.y = Mathf.Clamp(velocityChange.y, MaxAccel.y * -1f, MaxAccel.y);
        currentAccel.z = Mathf.Clamp(velocityChange.z, MaxAccel.z * -1f, MaxAccel.z);
    }

    public override void updateVelocity()
    {
        if (!updateState()) return;

        if (    Target.position != lastTargetPos 
            && (    (timeElapsed == 0 || ((timeToTarget / timeElapsed) > 0.3f)) 
                &&  ((lastTargetPos - Target.position).magnitude > 5f)
                )
            )
        {
            lastTargetPos = Target.position;
            startPosition = Self.position;
            updateMovementPlan();
        }


        float timePoint = getNextTimePoint(true);
        Vector3 intendedPosition = Vector3.Lerp(startPosition, lastTargetPos, easeMovement(timePoint));
        Vector3 intendedChange = intendedPosition - currentPos;

        currentVelocity.x = Mathf.Clamp(intendedChange.x, MaxVelocity.x * -1f, MaxVelocity.x);
        currentVelocity.y = Mathf.Clamp(intendedChange.y, MaxVelocity.y * -1f, MaxVelocity.y);
        currentVelocity.z = Mathf.Clamp(intendedChange.z, MaxVelocity.z * -1f, MaxVelocity.z);
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position += currentVelocity;
    }
}
