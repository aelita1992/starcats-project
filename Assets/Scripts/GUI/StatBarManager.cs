﻿using UnityEngine;
using System.Collections;

public class StatBarManager : MonoBehaviour 
{
	public EnergyBar shieldBar;
	public EnergyBar healthBar;

	int targetDisplayedShield;
	int targetDisplayedHealth;

	int currentlyDisplayedShield;
	int currentlyDisplayedHealth;

	public void SetTargetShield(int shield)
	{
		targetDisplayedShield = shield;
		shieldBar.SetValueCurrent(shield);
	}

	public void SetTargetHealth(int health)
	{
		currentlyDisplayedHealth = health;
		healthBar.SetValueCurrent(health);
	}

	void Update()
	{

	}
}
