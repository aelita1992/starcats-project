using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class XFormation : Formation
{
	int numberOfShipsLeft;
	int iterator;
	
	public override void SetFormation(List<GameObject> formationShips, FormationsController.Direction direction, Vector3 centerPosition, Vector2 xySpread)
	{
		numberOfShipsLeft = formationShips.Count;
		iterator = 0;
		this.xySpread = xySpread;

		SetCenterAndBottomWing(formationShips, centerPosition);
		SetTopWing(formationShips, centerPosition);
	}
	
	private void SetCenterAndBottomWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		int noOfBottomShips = (numberOfShipsLeft - 1)/2;
		Vector3 currentCenterPos = centerPosition;

		formationShips[iterator].transform.position = currentCenterPos;
		iterator++;

		for(; iterator <= noOfBottomShips/2; iterator++)
		{
			currentCenterPos.x -= xySpread.x;
			currentCenterPos.y -= xySpread.y;
			formationShips[iterator].transform.position = currentCenterPos;
		}

		currentCenterPos = centerPosition;

		for(; iterator <= noOfBottomShips; iterator++)
		{
			currentCenterPos.x += xySpread.x;
			currentCenterPos.y -= xySpread.y;
			formationShips[iterator].transform.position = currentCenterPos;
		}

		numberOfShipsLeft -= (noOfBottomShips + 1);
		
	}
	
	private void SetTopWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		int noOfBottomShips = numberOfShipsLeft;
		Vector3 currentCenterPos = centerPosition;

		for(int i = 0; i < noOfBottomShips/2;i++)
		{
			currentCenterPos.x -= xySpread.x;
			currentCenterPos.y += xySpread.y;
			formationShips[iterator].transform.position = currentCenterPos;
			iterator++;
		}

		currentCenterPos = centerPosition;

		for(int i = 0; i < noOfBottomShips/2;i++)
		{
			currentCenterPos.x += xySpread.x;
			currentCenterPos.y += xySpread.y;
			formationShips[iterator].transform.position = currentCenterPos;
			iterator++;
		}
	}
}