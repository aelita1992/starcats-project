﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ShieldPool : JSONAssetManager<ShieldPool, ShieldController>
{
    public ShieldPool()
    {
        fileName = "JSON/shieldManagerConfig";
        listName = "shields";
        typeName = "shields";
    }

    protected override GameObject processAsset(GameObject asset, JSONClass config)
    {
        asset.GetComponent<ShieldController>().offline();
        return asset;
    }


    protected override GameObject preparePrefab(GameObject prefab, JSONClass config)
    {
        return prefab;
    }

    public override GameObject _getPrefab(string name, Vector3 position, Quaternion rotation)
    {
        GameObject prefab = getPrefab(name);
        if (prefab != null)
        {
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            return prefab;
        }
        else
        {
            if (logWarnings) Debug.LogWarning("Prefab not found:" + name);
        }

        return null;
    }
}