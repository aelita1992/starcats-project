﻿using UnityEngine;
using System.Collections;

public class DeflectorScript : MonoBehaviour
{

    private ShieldController shield = null;

    public void attach(ShieldController shield)
    {
        this.shield = shield;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (shield != null) shield.handleColliderImpact(collision);
    }
}
