﻿using UnityEngine;
using System.Collections;

public class GodModeIndicatorScript : MonoBehaviour 
{
	float timer;
	
	void Start()
	{
		timer = 5;
	}
	
	public void ResetTimer()
	{
		timer = 5;
	}
	
	public void ReduceTimer(float value)
	{
		timer -= value;
	}
	
	void OnGUI()
	{
		GUI.backgroundColor = Color.cyan;
        GUI.HorizontalScrollbar(new Rect(25, Screen.height - 150, 400, 20), 0, timer, 0, 5);
	}
}
