﻿using UnityEngine;
using System.Collections;

public class MissileLauncher : MonoBehaviour
{
    public string targetTag = "EnemyShip";
    public string rocketName = "RPG";
    //public string launcherRef = "testGun3";
    public float damage = 50f;
    public float launchRate = 5f;

    public Transform target = null;
    public shipSpawner targetSpawner = null;

    public int launchRockets = 25;
    private int launchLeft = 0;

    void Awake()
    {
        //rocketType = ProjectileManager.get().getScript(rocketName);
        //launcher = WeaponManager.get().getScript(launcherRef); 
    }

    // Use this for initialization
    void Start()
    {
        launchLeft = launchRockets;
    }

    float nextFire = 0;
    public bool canFire()
    {
        return Time.time > nextFire;
    }
    void Update()
    {
        if (canFire() && launchLeft > 0)
        {
            projectileScript projectilePrefabClone = null;
            projectilePrefabClone = ProjectilePool.get().getScript(rocketName);
            if (projectilePrefabClone != null)
            {
                //////////////////////////////////
                projectilePrefabClone.transform.position = this.transform.position;
                projectilePrefabClone.transform.rotation = this.transform.rotation;
                projectilePrefabClone.projectileDamage = damage; // launcher.damage;

                projectilePrefabClone.rigidbody.WakeUp();
                projectilePrefabClone.rigidbody.AddForce(projectilePrefabClone.projectileSpeed * new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)), ForceMode.VelocityChange);

                if (targetSpawner != null && targetSpawner.ship != null)
                {
                    projectilePrefabClone.trackTarget(targetSpawner.ship.transform);
                }
                else if (target != null)
                {
                    projectilePrefabClone.trackTarget(target);
                }
                else
                {
                    projectilePrefabClone.trackTag(targetTag);
                }
                //////////////////////////////////

                nextFire = Time.time + 1f / launchRate;
            }
        }
    }
}
