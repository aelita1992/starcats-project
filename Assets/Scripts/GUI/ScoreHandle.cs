﻿using UnityEngine;
using System.Collections;

public class ScoreHandle : MonoBehaviour 
{
	TextMesh scoreTextMesh;
	int multipler;
	float multiplerTime;

	public GameObject multiplerIndicator;
	MultiplerIndicatorScript multiplerIndicatorScript;

	private static ScoreHandle pseudoSingleton;
	public static ScoreHandle Get()
	{
		return pseudoSingleton;
	}

	void Start()
	{
		multipler = 1;
		pseudoSingleton = this;
		scoreTextMesh = gameObject.GetComponent<TextMesh> ();
		multiplerIndicatorScript = multiplerIndicator.GetComponent<MultiplerIndicatorScript> ();
	}

	public void SetMultipler(int newMultipler)
	{
		multiplerIndicator.SetActive (true);
		multipler = newMultipler;
		multiplerTime = 5;
		multiplerIndicatorScript.ResetTimer ();
		TimeManager.Get ().timeDependantUpdate += ControlMultiplerTime;
	}

	public void ControlMultiplerTime()
	{
		multiplerTime -= Time.deltaTime;
		multiplerIndicatorScript.ReduceTimer (Time.deltaTime);

		if (multiplerTime <= 0)
		{
			TimeManager.Get ().timeDependantUpdate -= ControlMultiplerTime;
			ReduceMultipler();
		}
	}

	public void ReduceMultipler()
	{
		multiplerIndicator.SetActive (false);
		multipler = 1;
	}

	public void AddScore(int addedScore)
	{
		int currentScore = int.Parse(scoreTextMesh.text);
		currentScore += addedScore * multipler;
		scoreTextMesh.text = currentScore.ToString();
		GameOverPanel.Get ().score = currentScore;
	}

	public void SetScore(int newScore)
	{
		scoreTextMesh.text = newScore.ToString ();
	}

}
