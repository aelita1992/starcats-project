﻿using UnityEngine;
using System.Collections;

public class AsteroidBehaviour : MonoBehaviour
{
    int asteroidID;
    Vector3 movementVector;
    Vector3 rotationMultiplers;

    const float minSize = 30f;
    const float maxSize = 200f;
    const float massPerSizeUnit = 25f;              // masa (dla rigidBody) na jednostke rozmiaru
    const float HPPerSizeUnit = 3f;

    public float asteroidCollisionDamageMultiplier = 0.25f;
    public float asteroidCollisionFrequency = 2f;
    public float playerCollisionFrequency = 0.25f;


    public float chunkDistanceMultiplier = 0.0f;      // jak daleko spawnujemy odlamki od zrodla
    public float chunkForceMultiplier = 0.0f;        // jak szybko odlatuja odlamki
    public float massRetentionMultiplier = 1f;    // ile wiecej masy dajemy po podziale

    float sizeModifier = minSize;
    float HP = minSize * HPPerSizeUnit;

    public AsteroidsManager manager = null;
    public GameObject ExplosionPrefab;

	void Awake()
	{
		PrefabCollectionsManager.Get ().AddToManager(PrefabCollectionsManager.PrefabType.ASTEROID, this.gameObject);
	}

    void OnEnable()
    {
        InitiateMovement();
        InitiateRotation();
    }

    void OnDisable()
    {

    }

    private float GenerateMultipler()
    {
        return Random.Range(-1f, 1f);
    }

    public void GenerateRandomParameters()
    {
        transform.rigidbody.velocity = Vector3.zero;
        transform.rigidbody.angularVelocity = Vector3.zero;

        float basePosition = Random.Range(-480, 480);
        float distanceDelta = Random.Range(-75, 75);
        transform.position = new Vector3(basePosition, 400 + distanceDelta, -80);

        sizeModifier = Random.Range(minSize, maxSize);
        transform.localScale = new Vector3(sizeModifier, sizeModifier, sizeModifier);
        transform.rigidbody.mass = massPerSizeUnit * sizeModifier;
        HP = sizeModifier * HPPerSizeUnit;

        movementVector = new Vector3(Random.Range(-2f, 2f) * (-2500) * transform.rigidbody.mass, Random.Range(1f, 2f) * (-2500) * transform.rigidbody.mass, 0);
        rotationMultiplers = new Vector3(GenerateMultipler() * 50000, GenerateMultipler() * 50000, GenerateMultipler() * 50000);
    }

    public void SetCustomParameters(Vector3 position, Vector3 _movementVector)
    {
        transform.rigidbody.velocity = Vector3.zero;
        transform.rigidbody.angularVelocity = Vector3.zero;

        transform.position = position;
        movementVector = _movementVector;
        rotationMultiplers = new Vector3(GenerateMultipler() * 50000, GenerateMultipler() * 50000, GenerateMultipler() * 50000);
    }

    public void SetCustomParameters(Vector3 position, Vector3 _movementVector, float _sizeModifier)
    {
        SetCustomParameters(position, _movementVector);

        sizeModifier = _sizeModifier;        
        transform.localScale = new Vector3(sizeModifier, sizeModifier, sizeModifier);
        transform.rigidbody.mass = massPerSizeUnit * sizeModifier;
        HP = sizeModifier * HPPerSizeUnit;
    }

    void InitiateRotation()
    {
        transform.rigidbody.AddTorque(rotationMultiplers);
    }

    void InitiateMovement()
    {
        transform.rigidbody.AddRelativeForce(movementVector);
    }

    bool splitBasedOnSizeModifier(float parts)
    {
        float chunkSizeModifier = (sizeModifier * massRetentionMultiplier) / parts;

        if (minSize > chunkSizeModifier)
            return false;

        float chunkAngle = 360f / parts;
        float chunkDistance = Mathf.Clamp(chunkDistanceMultiplier * chunkSizeModifier, minSize, 200f);

        Debug.Log("chunkAngle = " + chunkAngle + "; chunkDistance = " + chunkDistance);

        Vector3 basePosition = transform.position;
        float roidMass = massPerSizeUnit * chunkSizeModifier;
        AsteroidBehaviour roid = null;
        for (int i = 0; i < parts; i++)
        {

            if (i == 0)
            {
                roid = this;
                this.transform.rotation = Quaternion.identity;
                this.rigidbody.velocity = Vector3.zero;
            }
            else
            {
                roid = manager.getNextAsteroid();
            }

            if (roid != null)
            {

                Vector3 chunkOffset = new Vector3(chunkDistance * Mathf.Cos(chunkAngle * i) - chunkDistance * 0.5f, chunkDistance * Mathf.Sin(chunkAngle * i) - chunkDistance * 0.5f, 0);
                //Debug.Log("chunkOffset = " + chunkOffset.x + "/" + chunkOffset.y + "/" + chunkOffset.z);

                Vector3 chunkPosition = new Vector3(basePosition.x + chunkOffset.x, basePosition.y + chunkOffset.y, basePosition.z);

                //Debug.Log("chunkPosition = " + transform.position.x + "/" + transform.position.y + "/" + transform.position.z + " to " + chunkPosition.x + "/" + chunkPosition.y + "/" + chunkPosition.z);

                Vector3 roidMovementVector = chunkOffset * roidMass * chunkForceMultiplier;

                //Debug.Log("roidMovementVector = " + roidMovementVector.x + "/" + roidMovementVector.y + "/" + roidMovementVector.z);

                roid.SetCustomParameters(chunkPosition, roidMovementVector, chunkSizeModifier);
                roid.gameObject.SetActive(true);
                roid.InitiateRotation();
                roid.InitiateMovement();
            }
        }
        return true;
    }

    float nextAsteroidCollision = 0;
    float nextPlayerCollision = 0;
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            ProjectilePool.get().returnToPool(col.gameObject);
            HP -= col.collider.rigidbody.mass;
        }
        else if (Time.time > nextPlayerCollision && col.gameObject.tag == "Player") 
        {
            HP -= col.collider.rigidbody.mass * 0.01f;
            nextPlayerCollision = Time.time + 1 / playerCollisionFrequency;
        }
        else if (Time.time > nextAsteroidCollision && col.gameObject.tag == "Asteroid")
        {
            HP -= col.collider.rigidbody.mass * asteroidCollisionDamageMultiplier;
            nextAsteroidCollision = Time.time + 1 / asteroidCollisionFrequency;
        }
        
        if( HP < 1 )
        {
            float chunks = Mathf.Clamp(sizeModifier / 30, 3f, 6f);
            if (splitBasedOnSizeModifier(chunks) == false)
            {
                gameObject.SetActive(false);
            }
        }
    }
}
