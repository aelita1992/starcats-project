﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShieldManager : MonoBehaviour {
    List<ShieldController> shields = new List<ShieldController>();
    bool logEverything = false;

    bool shieldsActivated = true;

    public int shieldsRegistered()
    {
        return shields.Count;
    }

    public bool areShieldsUp() {
        if (shieldsActivated)
        {
            foreach (ShieldController s in shields) {
                if (s.isUp()) return true;
            }            
        }
        return false;
    }

    public void activateShields()
    {
        //if (shieldsActivated) return;

        foreach (ShieldController s in shields)
        {
            s.online();
        }
    }

    public void deactivateShields()
    {
        //if (!shieldsActivated) return;

        foreach (ShieldController s in shields)
        {
            s.offline();
        }
    }

    public void registerShield(ShieldController shield)
    {
        shields.Add(shield);

        if (logEverything)
            Debug.Log("registerShield: " + shield.name);
    }
    public void unregisterShield(ShieldController shield)
    {
        shields.Remove(shield);

        if (logEverything)
            Debug.Log("unregisterShield: " + shield.name);
    }


    internal float maxShieldPower()
    {
        float maxPower = 0f;
        foreach (ShieldController s in shields)
            maxPower += s.StartHP;

        return maxPower;
    }

    internal float currentShieldPower()
    {
        float currentPower = 0f;
        foreach (ShieldController s in shields)
            currentPower += s.CurrentHP;

        return currentPower;
    }


}
