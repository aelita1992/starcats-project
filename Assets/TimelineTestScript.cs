﻿using UnityEngine;
using System.Collections;

public class TimelineTestScript : MonoBehaviour {

    Scenario s = null;

	// Use this for initialization
	void Start () {
        Debug.Log("TimelineTestScript init...");
        s = Scenario.fromFile("campaign");
        s.start(Time.time);
        Debug.Log("TimelineTestScript init: "+s.ToString());
	}
	
	// Update is called once per frame
	void Update () {
        if( !s.IsDone )
            s.update(Time.time);
	}
}
