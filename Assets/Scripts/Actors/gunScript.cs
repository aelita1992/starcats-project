﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public abstract class gunScript : JSONMonoBehaviour
{
    public ShipScript parentShip = null;
	protected bool initializedBase = false;
	protected bool initialized = false;

	protected bool logEverything = false;
	protected bool logWarnings = true;

	protected string modelName = null;
	protected string baseName = null;

	protected Transform attachBone = null;
	protected string attachBoneName = null;
	protected string attachBonePath = null;

	protected Transform muzzleBone = null;
	protected string muzzleBoneName = null;
	protected string muzzleBonePath = null;

	protected float rateOfFire = 0;
	protected float damage = 25f;

	protected int burstSize = 1;
	protected float burstRate = 1;

    protected JSONArray projectileSpread = null;
	protected JSONArray projectileScale = null;

	protected int burstCount = 0;
	protected float nextBurst = 0.0F;
	protected float nextFire = 0.0F;

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        if (logEverything) Debug.Log("applyConfig: " + this);

        if (logEverything) listBones(transform);

        name = config["gunName"].Value;
        modelName = config["modelName"].Value;
        baseName = config["baseName"].Value;
        damage = config["damage"].AsFloat;
        if (damage <= 1f) damage = 25f;

        rateOfFire = config["rateOfFire"].AsFloat;

        // rozrzut i zmiana rozmiaru pocisku
        projectileSpread = config["spread"].AsArray;
        projectileScale = config["scale"].AsArray;

        // strzelanie seriami
        burstSize = config["burstSize"].AsInt;
        burstRate = config["burstRate"].AsFloat;

        if (burstSize < 1) burstSize = 1;
        if (burstRate < 1) burstRate = 1;

        // kość na której przyczepia sie broń

        attachBoneName = config["attachBone"].Value;
        attachBonePath = config["attachBonePath"].Value;

        if (attachBonePath.Length > 0)
            attachBone = transform.Find(attachBonePath + "/" + attachBoneName);
        else
            attachBone = transform.Find(attachBoneName);

        if (attachBone != null)
        {
            if (logEverything) Debug.Log("fund attachBone: " + attachBoneName + " at bone " + attachBonePath + "/" + attachBoneName, attachBone);
        }
        else
        {
            if (logWarnings) Debug.LogWarning("attachBone missing: " + attachBoneName);
            listBones(transform);
        }

        // kość na której przyczepia sie pocisk

        muzzleBoneName = config["muzzleBone"].Value;
        muzzleBonePath = config["muzzleBonePath"].Value;

        if (muzzleBonePath.Length > 0)
            muzzleBone = transform.Find(muzzleBonePath + "/" + muzzleBoneName);
        else
            muzzleBone = transform.Find(muzzleBoneName);
        

        if (muzzleBone != null)
        {
            if (logEverything) Debug.Log("fund muzzleBone: " + muzzleBoneName + " at bone " + muzzleBonePath + "/" + muzzleBoneName, muzzleBone);
        }
        else
        {
            if (logWarnings) Debug.LogWarning("muzzleBone missing: " + muzzleBonePath + "/" + muzzleBoneName);
        }

		initializedBase = true;

        return this;
    }

    public Transform getAttachBone()
    {
        if (logEverything) Debug.Log("getAttachBone", this);

        if (this.initialized == false)
        {
            Debug.LogError("gun not initialized @ getAttachBone()");
            return null;
        }
        else
        {
            if (logWarnings && attachBone == null) Debug.LogWarning("gun initialized, attachBone == null");
        }

        return attachBone;
    }
	
    public bool canFire()
    {
        return Time.time > nextFire;
    }

    public void fireIfReady()
    {
        if (canFire()) fire();
    }

	protected void updateFireTimer(){
		// liczymy czas do nast strzalu:
		// jezeli strzelamy seria, liczymy wg szybkostrzelnosci w serii
		// jezeli skonczylismy serie (albo burstRate == 1) to liczymy wg globalnej szybkostrzelnosci
		if (burstSize == 1)
		{
			nextFire = Time.time + 1f / rateOfFire;
		} else {
			
			if (Time.time > nextBurst){
				burstCount = 0;
				nextBurst = Time.time + 1f / rateOfFire;
			}
			
			burstCount++;
			if (burstCount >= burstSize ){
				burstCount = 0;
				nextFire = Time.time + 1f / rateOfFire;
			} else {
				nextFire = Time.time + 1f / burstRate;
			}
		}
	}

	public abstract void fire();

}
