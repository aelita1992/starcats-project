﻿using UnityEngine;
using System.Collections;

public class PlayerConnectorScript : MonoBehaviour {

    ShipPool ShipManagerInstance = null;
    ShipScript selectedShip = null;
    GameObject selectedShipGameObject = null;

    public GameObject shipStatisticsGUI_Shield;
    public GameObject shipStatisticsGUI_Health;

	float teslaCooldown;

	public delegate void DeathDelegate();
	public event DeathDelegate OnDeath;

    public GameObject SelectedShipGameObject
    {
        get { return selectedShipGameObject; }
        set { selectedShipGameObject = value; }
    }

    public int shipType = 1;

	void Start () {
		transform.localScale = new Vector3(3.5f,3.5f,3.5f);
        ShipManagerInstance = ShipPool.get();
        selectShip(shipType);
		teslaCooldown = 0;
	}

    protected void selectShip(int type)
    {

        string shipName = "LOLShip";

        switch (type) { 
            case 1:
                shipName = "PlayerShip";
                //shipName = "LOLShip";
                break;
            case 2:
                shipName = "ROTFLShip";
                break;
            case 3:
                shipName = "LOLShipAlt";
                break;
            case 4:
                shipName = "ROTFLShipAlt";
                break;
            case 5:
                shipName = "BigShip";
                break;
            case 6:
                shipName = "CheatShip";
                break;
        }

        //if (selectedShipGameObject!=null) Destroy(selectedShipGameObject);
        if (selectedShipGameObject != null)
        {
            ShipPool.get().returnToPool(selectedShipGameObject);
            selectedShipGameObject = null;
        }

        selectedShip = ShipPool.get().getScript(shipName);
        selectedShipGameObject = selectedShip.gameObject;

        selectedShipGameObject.transform.position = transform.position;
        selectedShipGameObject.transform.rotation = transform.rotation;
        selectedShipGameObject.transform.localScale = transform.localScale;

        shipStatisticsGUI_Health.GetComponent<EnergyBar>().SetValueF(selectedShip.CurrentHP);
    }

    bool deathState = false;

	// Update is called once per frame
	void Update () {
        /*
        if (Input.GetKey(KeyCode.F1)) selectShip(1);
        if (Input.GetKey(KeyCode.F2)) selectShip(2);
        if (Input.GetKey(KeyCode.F3)) selectShip(3);
        if (Input.GetKey(KeyCode.F4)) selectShip(4);
        if (Input.GetKey(KeyCode.F5)) selectShip(5);
        if (Input.GetKey(KeyCode.F6)) selectShip(6);
        */

        //if (Input.GetKey(KeyCode.LeftControl))
          //  selectedShip.fireWeaponsGroup(1);
        if (!selectedShip.IsAlive)
        {
            //Debug.LogError("Dead");

            if (deathState == false)
            {
                deathState = true;
                Debug.LogError("OnDeath (" + deathState + " // " + (deathState == false) + ")");

                // OnDeath();                
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.LogError("Restart");
                selectShip(1);
                scenarioPlayer.get().restartLevel();
                deathState = false;
            }
        }
        else {
            if (Input.GetKeyDown(KeyCode.X)) {
                selectedShip.kill();
            }
        }

		if(teslaCooldown > 0)
		{
			teslaCooldown -= Time.deltaTime;
		}

        if (Input.GetKey(KeyCode.LeftShift))
		{
			if(!TeslaAmmoManager.Get ().IsEnoughAmmo())
				return;

			if(teslaCooldown > 0)
			{
				return;
			}

			TeslaAmmoManager.Get ().ReduceAmmo();
			teslaCooldown = 2;
            selectedShip.fireWeaponsGroup(2);
			AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.ENERGY_WEAPON);
		}

        if (Input.GetKey (KeyCode.Space))
		{
			selectedShip.fireWeaponsGroup(1);
			//selectedShip.fireAllWeapons();
		}
            

	}

    // http://unity3d.com/earn/tutorials/projects/space-shooter/moving-the-player

    [System.Serializable]
    public class Boundary
    {
        public float xMin, xMax, yMin, yMax;
    }

    public float speed;
    public float tilt;
    public Boundary boundary;

    void FixedUpdate()
    {
        if (selectedShipGameObject != null)
        {
            Rigidbody rigidbody = selectedShipGameObject.rigidbody;

            float speedMultipler = 1.5f;
            float moveHorizontal = Input.GetAxis("Horizontal") * speedMultipler;
            float moveVertical = Input.GetAxis("Vertical") * speedMultipler;

            Vector3 movement = new Vector3(moveHorizontal, moveVertical, 0.0f);
            rigidbody.velocity = movement * speed;

            rigidbody.position = new Vector3
            (
                Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
                Mathf.Clamp(rigidbody.position.y, boundary.yMin, boundary.yMax),
                -65.0f
            );

            rigidbody.rotation = Quaternion.Euler(270.0f, 0.0f, rigidbody.velocity.x * -tilt);

            Quaternion statisticsRotation = shipStatisticsGUI_Shield.transform.rotation;
            statisticsRotation.y = selectedShipGameObject.transform.rotation.y;
            shipStatisticsGUI_Shield.transform.rotation = statisticsRotation;
            shipStatisticsGUI_Health.transform.rotation = statisticsRotation;

            Vector3 newGuiPosition = shipStatisticsGUI_Shield.transform.position;
            Vector3 shipPosition = selectedShipGameObject.transform.position;

            newGuiPosition.x = shipPosition.x;
            newGuiPosition.y = shipPosition.y;

            shipStatisticsGUI_Shield.transform.position = newGuiPosition;
            shipStatisticsGUI_Health.transform.position = newGuiPosition;
        }
	}

    internal bool isAlive()
    {
        return selectedShip != null ? selectedShip.IsAlive : false;
    }
}
