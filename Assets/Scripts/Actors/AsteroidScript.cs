﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class AsteroidScript : JSONMonoBehaviour
{
    private static float zPlaneHeight = -65f;
    private bool initialized = false;

    private bool logEverything = true;
    //private bool logWarnings = true;
    //
    const float minSize = 30f;
    const float maxSize = 200f;

    const float massPerSizeUnit = 25f;              // masa (dla rigidBody) na jednostke rozmiaru
    const float HPPerSizeUnit = 3f;

    const float asteroidCollisionDamageMultiplier = 0.25f;
    const float asteroidCollisionFrequency = 2f;
    const float playerCollisionFrequency = 0.25f;


    
    const float massRetentionMultiplier = 1f;    // ile wiecej masy dajemy po podziale

    float chunkDistanceMultiplier = 0.5f;      // jak daleko spawnujemy odlamki od zrodla
    Vector3 chunkForce = Vector3.one;        // jak szybko odlatuja odlamki


    const float scaleHPFactor = 10f;
    const float scaleChunkFactor = 0.125f;

    // roid settings
    Vector3 roidScale = Vector3.one;

    float initialHP = 1f;
    float currentHP = 1f;

    bool explodeIntoChunks = true;

    float chunks = 0f; // 0 = calculate based on roidScale.magnitude
    string chunkConfig = "SmallRoid";

    public override void resetPrefab()
    {
        currentHP = initialHP;
        gameObject.SetActive(true);

        rigidbody.velocity = Vector3.zero;
        rigidbody.angularVelocity = Vector3.zero;
    }

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        if (logEverything) Debug.Log("applyConfig: " + this);
        if (initialized) return this;
        ///

        // scale
        JSONArray roidScaleConfig = config["scale"].AsArray;
        if (roidScaleConfig != null && roidScaleConfig.Count == 3) {
            roidScale = new Vector3(roidScaleConfig[0].AsFloat, roidScaleConfig[1].AsFloat, roidScaleConfig[2].AsFloat);
            this.transform.localScale = roidScale;
        }

        float configHP = config["HP"].AsFloat;
        if (configHP > 0f)
        {
            initialHP = configHP;
            currentHP = configHP;
        }
        else {
            initialHP = scaleHPFactor * roidScale.magnitude;
            currentHP = scaleHPFactor * roidScale.magnitude;
        }

        if (config["split"].Value.Length > 0)
        {
            explodeIntoChunks = config["split"].AsBool;
        }

        if (explodeIntoChunks)
        {
            JSONClass splitData = config["splitParams"].AsObject;
            if (splitData != null && splitData.Count > 0)
            {
                string splitInto = splitData["splitInto"].Value;
                int splitCount = splitData["count"].AsInt;
                JSONArray splitForce = splitData["splitForce"].AsArray;

                if (splitInto.Length > 0) chunkConfig = splitInto;
                if (splitCount > 0) chunks = splitCount;
                if (splitForce != null && splitForce.Count == 3) {
                    chunkForce = new Vector3(splitForce[0].AsFloat, splitForce[1].AsFloat, splitForce[2].AsFloat);
                }
            }

        }

        this.rigidbody.mass = scaleHPFactor * roidScale.magnitude * 10f;
        ///
        Debug.Log("Roid setup: HP="+currentHP +"; splode=" + explodeIntoChunks +"; chunks="+ chunks +"; chunkName="+ chunkConfig);
        ///
        initialized = true;
        return this;
    }
   
    public static void moveRoid(AsteroidScript roid, Vector3 rotationMultipler, Vector3 movementVector)
    {
        roid.transform.rigidbody.AddTorque(rotationMultipler, ForceMode.VelocityChange);
        roid.transform.rigidbody.AddRelativeForce(movementVector, ForceMode.VelocityChange);
    }

    float nextAsteroidCollision = 0;
    float nextPlayerCollision = 0;

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile" || col.gameObject.tag == "EnemyProjectile")
        {
            ProjectilePool.get().returnToPool(col.gameObject);
            currentHP -= 10f; //col.collider.rigidbody.mass;
        }
        else if (Time.time > nextPlayerCollision && col.gameObject.tag == "Player")
        {
            currentHP -= col.collider.rigidbody.mass * 0.01f;
            nextPlayerCollision = Time.time + 1 / playerCollisionFrequency;
        }
        else if (Time.time > nextAsteroidCollision && col.gameObject.tag == "Asteroid")
        {
            currentHP -= col.collider.rigidbody.mass * asteroidCollisionDamageMultiplier;
            nextAsteroidCollision = Time.time + 1 / asteroidCollisionFrequency;
        }

        if (currentHP < 1f)
        {
            float splitCount = 0f;
            if (chunks == 0f) {
                splitCount = chunks;
            }
            else
            {
                splitCount =  Mathf.Clamp(roidScale.magnitude * scaleChunkFactor, 1f, 6f);
            }

            if (splitCount > 1f)
                split(splitCount);

            AsteroidPool.get().returnToPool(this.gameObject);
        }
    }

    void split(float parts)
    {
        float chunkSizeModifier = (roidScale.magnitude * massRetentionMultiplier * 10f) / parts;


        if (minSize > chunkSizeModifier)
            return;
        
        
        float chunkAngle = 360f / parts;
        float chunkDistance = Mathf.Clamp(chunkDistanceMultiplier * chunkSizeModifier, minSize, 500f);

        Debug.Log("chunkAngle = " + chunkAngle + "; chunkDistance = " + chunkDistance + "; chunkSizeModifier = "+chunkSizeModifier);

        Vector3 basePosition = transform.position;
        // float roidMass = massPerSizeUnit * chunkSizeModifier;
        AsteroidScript roid = null;
        for (int i = 0; i < parts; i++)
        {

            roid = AsteroidPool.get().getScript(chunkConfig);
            
            if (roid != null)
            {
                Vector3 chunkOffset = new Vector3(chunkDistance * Mathf.Cos(chunkAngle * i) - chunkDistance * 0.5f, chunkDistance * Mathf.Sin(chunkAngle * i) - chunkDistance * 0.5f, 0);
                Vector3 chunkPosition = new Vector3(basePosition.x + chunkOffset.x, basePosition.y + chunkOffset.y, zPlaneHeight);
                Vector3 roidMovementVector = chunkOffset;
                roidMovementVector.Scale(chunkForce);

                roid.transform.position = chunkPosition;
                AsteroidScript.moveRoid(roid, roidMovementVector, roidMovementVector);
            }
        }
        
    }

	void handleImpact(DamageInfo info)
	{
		Debug.LogError("trafione");
	}
}
