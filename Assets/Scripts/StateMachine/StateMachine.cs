﻿using UnityEngine;
using System.Collections;

public class StateMachine : MonoBehaviour 
{
	protected abstract class State
	{
		protected StateMachine stateMachineAccess;

		public State()
		{
			this.stateMachineAccess = StateMachine.Get();
		}

		public abstract void Init();
		public virtual void Exit(){}
	}

    //-------------------------------------------------------------

    class MainMenuState : State
    {
		MainMenuPanel mainMenuPanel;
		DemoPanel demoPanel;

        public override void Init()
        {
			// EnemyPool.Get ();
			AudioManager.Get ().PlayMusicClip(AudioManager.MusicClipName.MAIN_MENU_THEME);

			//TimeManager.Get ().PauseGame();
			mainMenuPanel = stateMachineAccess.mainMenuPanel;
			demoPanel = stateMachineAccess.demoPanel;

			mainMenuPanel.Expand();
			demoPanel.Expand();

			mainMenuPanel.start.OnButtonPressed += OnStart;
			mainMenuPanel.settings.OnButtonPressed += OnSettings;
			mainMenuPanel.exit.OnButtonPressed += OnExit;
        }

        public override void Exit()
        {
			mainMenuPanel.Collapse();

			mainMenuPanel.start.OnButtonPressed -= OnStart;
			mainMenuPanel.settings.OnButtonPressed -= OnSettings;
			mainMenuPanel.exit.OnButtonPressed -= OnExit;
        }

		private void OnStart()
		{
			StateMachine.Get ().SetState(new MainGameState());
			demoPanel.Collapse();
		}

		private void OnSettings()
		{
			StateMachine.Get ().SetState(new SettingsState());
		}

		private void OnExit()
		{
			Application.Quit ();
		}
    }

	class SettingsState : State
	{
		SettingsPanel settingsPanel;

		public override void Init()
		{
			settingsPanel = stateMachineAccess.settingsPanel;

			settingsPanel.Expand();
			settingsPanel.returnButton.OnButtonPressed += OnReturn;
		}
		
		public override void Exit()
		{
			settingsPanel.returnButton.OnButtonPressed -= OnReturn;
			settingsPanel.Collapse();
		}

		private void OnReturn()
		{
			StateMachine.Get ().SetState(new MainMenuState());
		}
	}

	class MainGameState : State
	{
		MainGamePanel mainGamePanel;
		LevelsPanel levelsPanel;
		GuiPanel guiPanel;
		InGameGUIPanel inGameGUI;
		ScoreDisplayPanel scoreDisplayPanel;
		PlayerConnectorScript playerConnector;

		public override void Init()
		{
			AudioManager.Get ().PlayMusicClip(AudioManager.MusicClipName.INGAME_THEME);

			playerConnector = stateMachineAccess.playerConnector;
			mainGamePanel = stateMachineAccess.mainGamePanel;
			levelsPanel = stateMachineAccess.levelsPanel;
			guiPanel = stateMachineAccess.guiPanel;
			inGameGUI = stateMachineAccess.inGameGUI;
			scoreDisplayPanel = stateMachineAccess.scoreDisplayPanel;	

			playerConnector.OnDeath += OnDeath;
			ComponentShipScript.OnDeath += OnWin;

			scoreDisplayPanel.Expand();
			inGameGUI.Expand();
			guiPanel.Expand ();
			mainGamePanel.Expand();
			levelsPanel.Expand();
			TimeManager.Get ().ResumeGame();

		}
		
		public override void Exit()
		{
			scoreDisplayPanel.Collapse();
			inGameGUI.Collapse();
			guiPanel.Collapse ();
			mainGamePanel.Collapse();
			levelsPanel.Collapse();
		}

		public void OnDeath()
		{	
			StateMachine.Get ().SetState(new GameOverState());
			GameOverPanel.Get ().ActivateLostScreen();
			TimeManager.Get().PauseGame();
		}

		public void OnWin()
		{
			StateMachine.Get ().SetState(new GameOverState());
			GameOverPanel.Get ().ActivateWonScreen();
			TimeManager.Get().PauseGame();
		}
		
	}

	class GameOverState : State
	{
		GameOverPanel gameOverPanel;
		
		public override void Init()
		{
			gameOverPanel = stateMachineAccess.gameOverPanel;
			gameOverPanel.Expand();
			
		}
		
	}

    //-------------------------------------------------------------

	public MainGamePanel mainGamePanel;
	public MainMenuPanel mainMenuPanel;
	public SettingsPanel settingsPanel;
	public LevelsPanel levelsPanel;
	public GuiPanel guiPanel;
	public InGameGUIPanel inGameGUI;
	public DemoPanel demoPanel;
	public ScoreDisplayPanel scoreDisplayPanel;
	public GameOverPanel gameOverPanel;
	public PlayerConnectorScript playerConnector;

	public GameObject pool;

	static StateMachine instance;

	State currentState;

	static public StateMachine Get() { return instance;}

	void Awake()
	{
		if(StateMachine.instance == null)
			StateMachine.instance = this;
		else
			Debug.LogError("Error with the state machine's singleton");
	}

	void Start()
	{
		Init();
	}

	protected void SetState(State newState)
	{
		if(currentState != null)
			currentState.Exit ();
		currentState = newState;
		newState.Init ();
	}

    private void preWarmManagers() {
        Debug.Log("preWarmManagers...");
        MiscAssetPool.loadAndGet().prewarmPools(4);
        ComponentPool.loadAndGet().prewarmPools(4);
        ComponentShipPool.loadAndGet().prewarmPools(4);

        ComponentShipScript x = ComponentShipPool.get().getScript("TestShip");
        ComponentShipPool.get().returnToPool(x.gameObject);
        
        AsteroidPool.loadAndGet().prewarmPools(10);
        WeaponPool.loadAndGet().prewarmPools(10);
        WeaponPool.get().prewarmPool("EnemyGun", 350);
        ShipPool.loadAndGet().prewarmPools(10);
        ShipPool.get().prewarmPool("EnemyShip", 250);
        ShipPool.get().prewarmPool("EnemyShip2", 100);
        ProjectilePool.loadAndGet().prewarmPools(64);
        ProjectilePool.get().prewarmPool("EnemyProjectile", 512);
		ProjectilePool.get().prewarmPool("HerpDerp", 256);
		//ProjectileManager.get ().verifyPool ("HerpDerp");

		//TimeManager.Get ().timeDependantUpdate_10seconds += checkProjectilePool;
    }

	void checkProjectilePool(){
		ProjectilePool.get ().verifyPool ("HerpDerp");
	}

	public void Init()
	{
        preWarmManagers();
		StateMachine.Get ().SetState(new MainMenuState());
	}

}
