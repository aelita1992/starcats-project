﻿using UnityEngine;
using System.Collections;

public class BonusManager : MonoBehaviour 
{
	private bool isGodModeEnabled;

	public GameObject godModeIndicator;
	GodModeIndicatorScript godModeIndicatorScript;

	float godModeTimer;

	private static BonusManager singleton;
	public static BonusManager Get()
	{
		return singleton;
	}

	void Start()
	{
		godModeIndicatorScript = godModeIndicator.GetComponent<GodModeIndicatorScript> ();
		singleton = this;
	}

	public void EnableGodMode()
	{
		isGodModeEnabled = true;
		godModeIndicator.SetActive (true);
		godModeTimer = 5;
		godModeIndicatorScript.ResetTimer ();
		TimeManager.Get ().timeDependantUpdate += ControlGodModeTimer;
	}

	private void ControlGodModeTimer()
	{
		godModeTimer -= Time.deltaTime;
		godModeIndicatorScript.ReduceTimer (Time.deltaTime);

		if (godModeTimer <= 0) 
		{
			TimeManager.Get ().timeDependantUpdate -= ControlGodModeTimer;
			DisableGodMode();
		}
	}

	public void DisableGodMode()
	{
		godModeIndicator.SetActive (false);
		isGodModeEnabled = false;
	}

	public bool GodModeEnabled()
	{
		return isGodModeEnabled;
	}


}
