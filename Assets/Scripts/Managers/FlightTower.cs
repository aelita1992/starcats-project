using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlightTower 
{
	private static FlightTower singleton;

	public static FlightTower Get()
	{
		if(singleton == null)
			singleton = new FlightTower();

		return singleton;
	}

	public void PrepareFlight(
			FormationsController.FormationEnum formation,
			EnemyMovementManager.MovementType movementType,
			List<GameObject> enemyShips, 
			FormationsController.Direction direction,
			Vector3 centerPosition,
			Vector2 xySpread
			)
	{
		FormationsController.Get().PrepareFormation(formation, enemyShips, direction, centerPosition, xySpread);
		EnemyMovementManager.Get().SetMovementStyle(enemyShips, movementType);
	}
}
