﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Level
{
    public event Scenario.StageStarted nextStageStarted;

    public static string levelPath = "JSON/Levels/";
    public static Level fromFile(string fileName)
    {
        TextAsset levelJSON = Resources.Load(levelPath + fileName) as TextAsset;

        if (levelJSON == null)
        {
            Debug.LogError("Error loading " + fileName);
            return null;
        }

        var levelData = JSON.Parse(levelJSON.text);

        if (levelData == null)
        {
            Debug.LogError("Error parsing " + fileName);
            return null;
        }

        return new Level(levelData.AsObject);
    }
    public Level(JSONClass levelData)
    {
        name = levelData["name"].Value;
        firstStage = levelData["firstStage"].Value;

        JSONArray stageList = levelData["stages"].AsArray;
        if (stageList != null && stageList.Count > 0)
        {

            stages = new List<Stage>();

            foreach (JSONClass s in stageList)
            {
                if (s == null)
                {
                    Debug.Log("err 3");
                    continue;
                }

                Stage sc = new Stage(s);
                stages.Add(sc);
            }
        }
        else
        {
            Debug.Log("err 2");
        }

    }

    Stage findStage(string name)
    {
        foreach (Stage s in stages)
        {
            if (s.Name == name) return s;
        }
        return null;
    }

    public void start(float time)
    {
        startTime = time;
        Debug.LogWarning("Level " + name + ": start @" + time);

        currentStage = findStage(firstStage);
        if (currentStage == null)
        {
            Debug.LogError("Level " + name + ": Stage not found: " + firstStage);
        }
        else {
            Debug.LogWarning("Level " + name + ": Switched to _first_ stage: " + currentStage.Name);
        }

        if (nextStageStarted != null)
        {
            nextStageStarted(currentStage);
        }
        currentStage.start(elapsedTime);
    }


    public void update(float time)
    {
        elapsedTime = time - startTime;

        if (currentStage == null)
        {
            Debug.LogError("Level " + name + ": Stage not initialized");
            isDone = true;
            return;
        }

        if (currentStage.IsDone)
        {
            if (currentStage.NextStage == null || currentStage.NextStage == string.Empty)
            {
                isDone = true;
                Debug.LogWarning("Level " + name + ": Finished final stage: " + currentStage.Name);
                return;
            }

            Stage nextStage = findStage(currentStage.NextStage);
            if (nextStage == null)
            {
                Debug.LogError("Level " + name + ": Stage not found: " + currentStage.NextStage);
                return;
            }

            currentStage = nextStage;
            if (nextStageStarted != null) nextStageStarted(currentStage);
            currentStage.start(elapsedTime);
            Debug.LogWarning("Level " + name + ": Switched to stage: " + currentStage.Name + " @" + elapsedTime);
        }

        currentStage.update(elapsedTime);
    }

    public void forceEnd(bool rewind = true)
    {
        foreach (Stage s in stages)
        {
            s.forceEnd(rewind);
        }

        currentStage = null;

        if (rewind)
        {            
            startTime = 0f;
            elapsedTime = 0f;
            isDone = false;
        }
    }

    float startTime = 0f;
    float elapsedTime = 0f;
    bool isDone = false;

    public bool IsDone
    {
        get { return isDone; }
        set { isDone = value; }
    }


    private string name = null;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    private List<Stage> stages = null;
    private string firstStage = null;
    private Stage currentStage = null;
    /*
    private List<Level> nextLevelList = null;
    private Level nextLevelDefault = null;
    */
}
