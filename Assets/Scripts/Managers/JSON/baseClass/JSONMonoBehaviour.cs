﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public abstract class JSONMonoBehaviour : MonoBehaviour
{

    public GameObjectPool parentPool;
    public bool pooled = false;
    // Use this for initialization
    public abstract JSONMonoBehaviour applyConfig(JSONClass config);

    // set prefab state before putting it back in the game
    public virtual void resetPrefab()
    {
        // implement prefab-specific reset action in the subclass (if needed)
    }

    // cleanup prefab state before returning it to pool
    public virtual void cleanUp()
    {
        // implement prefab-specific cleanUp action in the subclass (if needed)
    }

    public JSONMonoBehaviour getFresh()
    {
        resetPrefab();
        return this;
    }

    // tworzy Vector3 z 3-elementowej tablicy pobranej z JSONa, albo zwraca Vector3.zero jak tablica jest zła
    protected Vector3 vector3FromJSONArray0(JSONArray src)
    {
        if (src != null && src.Count == 3)
        {
            return new Vector3(src[0].AsFloat, src[1].AsFloat, src[2].AsFloat);
        }

        return Vector3.zero;
    }

    // tworzy Vector3 z 3-elementowej tablicy pobranej z JSONa, albo zwraca Vector3.one jak tablica jest zła
    protected Vector3 vector3FromJSONArray1(JSONArray src)
    {
        if (src != null && src.Count == 3)
        {
            return new Vector3(src[0].AsFloat, src[1].AsFloat, src[2].AsFloat);
        }

        return Vector3.one;
    }

    public void listBones(Transform node, string path = null)
    {
        string thisPath = null;

        if (path == null)
        {
            thisPath = "{root} :: [" + node.name + "]";
        }
        else
        {
            thisPath = path + " :: [" + node.name + "]";
        }

        Debug.Log("ListBones:\t" + thisPath, node);
        foreach (Transform child in node)
        {
            listBones(child, thisPath);
        }
    }

    protected GameObject getColliders()
    {
        GameObject o = new GameObject();
        foreach (Component copiedCollider in GetComponents<Collider>())
        {
            System.Type copiedColliderType = copiedCollider.GetType();

            if (copiedColliderType == typeof(BoxCollider))
            {
                BoxCollider specificCopiedCollider = (BoxCollider)copiedCollider;
                BoxCollider specificCollider = o.AddComponent<BoxCollider>();

                specificCollider.center = specificCopiedCollider.center;
                specificCollider.size = specificCopiedCollider.size;
            }

            if (copiedColliderType == typeof(CapsuleCollider))
            {
                CapsuleCollider specificCopiedCollider = (CapsuleCollider)copiedCollider;
                CapsuleCollider specificCollider = o.AddComponent<CapsuleCollider>();

                specificCollider.direction = specificCopiedCollider.direction;
                specificCollider.height = specificCopiedCollider.height;
                specificCollider.radius = specificCopiedCollider.radius;
                specificCollider.center = specificCopiedCollider.center;
            }

            if (copiedColliderType == typeof(SphereCollider))
            {
                SphereCollider specificCopiedCollider = (SphereCollider)copiedCollider;
                SphereCollider specificCollider = o.AddComponent<SphereCollider>();

                specificCollider.radius = specificCopiedCollider.radius;
                specificCollider.center = specificCopiedCollider.center;
            }
        }

        return o;
    }
}
