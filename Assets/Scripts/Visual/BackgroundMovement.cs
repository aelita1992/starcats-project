﻿using UnityEngine;
using System.Collections;

public class BackgroundMovement : MonoBehaviour 
{
	Vector3 startingPosition;
	Vector3 targetPosition;
	
	float distance;
	float speed;

	static bool isInitiated = false;

	void OnEnable () 
	{
		if(!isInitiated)
		{
			startingPosition = transform.localPosition;
			isInitiated = true;
		}

		speed = 128f;
		distance = 0;
		targetPosition = new Vector3(0,-600,0);
		TimeManager.Get().timeDependantUpdate += PerformMovement;
	}

	void OnDisable()
	{
		TimeManager.Get().timeDependantUpdate -= PerformMovement;
	}

	private void PerformMovement()
	{
		float step = speed * Time.deltaTime;
		distance += step;
		transform.position = Vector3.MoveTowards(transform.position, targetPosition, step);

		if(distance >= 1024)
		{
			distance = 0;
			transform.localPosition = startingPosition;

		}
	}
	

}
