﻿using UnityEngine;
using System.Collections;

public class ShieldController : JSONMonoBehaviour
{
    private bool logEverything = false;
    private bool debugImpacts = true;
    GameObject protectedObject = null;
    public bool attachToParent = true;

    public shieldTrigger triggerCollider = null;
    public DeflectorScript deflectorCollider = null;
    public particleEffectWrapper impactEffect = null;

    // bubble mesh
    public GameObject bubble = null;

    private float startHP = 250f;
    private float currentHP = 250f;

    private float minHP = 25f;
    private float regenRate = 30f; // hp per sec
    public float StartHP
    {
        get { return startHP; }
        set { startHP = value; }
    }


    public float CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }

    bool shieldOnline = false;
    private bool isRebuilding = false;
    private bool isFull = true;
    
    private bool isDeflector = false;

    public void setStrength(float strength) {
        startHP = strength;
        currentHP = startHP;
    }

    /*
    public void triggerDeflector()
    {
        isDeflector = true;
        bubble.renderer.material.SetColor("_Color", Color.green);
    }

    public void triggerAbsorber()
    {
        isDeflector = false;
		ChangeShieldColor ();
    }
    */
    protected bool isObjectValidTarget(GameObject collider)
    {
        if (protectedObject == null) {
            Debug.LogWarning("Unattached shield in collision!",this);
            return false;
        }

        if (protectedObject.tag == "Player" && collider.tag == "EnemyProjectile") return true;
        if (protectedObject.tag == "EnemyShip" && collider.tag == "Projectile") return true;
		if (protectedObject.tag == "EnemyShip" && collider.tag == "Rocket") return true;

        if (protectedObject.tag == "EnemyShip" && collider.tag == "Player") return true;
        if (protectedObject.tag == "Player" && collider.tag == "EnemyShip") return true;

        return false;
    }

    private void passThrough(Collider other){
        if (other.gameObject.collider != deflectorCollider.collider)
        {
            if (logEverything) Debug.Log("passing through: " + other.gameObject.name, other.gameObject);
            if (triggerCollider.gameObject.activeSelf && other.gameObject.activeSelf) 
                Physics.IgnoreCollision(triggerCollider.collider, other.gameObject.collider);
            if (deflectorCollider.gameObject.activeSelf && other.gameObject.activeSelf) 
                Physics.IgnoreCollision(deflectorCollider.collider, other.gameObject.collider);
        }
        else
        {
            if (logEverything) Debug.Log("same thing? " + other.gameObject.name, other.gameObject);
        }
    }

    private DamageInfo projectileImpactDamage = new DamageInfo(0f, null, null, false);
    public void handleImpact(DamageInfo damage)
    {
        float damageHP = 1f;

        if (damage != null)
        {
            if (damage.DOT)
                damageHP = damage.damage * Time.deltaTime;
            else
                damageHP = damage.damage;
        }

        currentHP -= damageHP;

        ChangeShieldColor();

        if (currentHP <= 0f)
        {
            currentHP = 0;
            Color color = bubble.renderer.material.GetColor("_Color");
            color.a = 0;
            bubble.renderer.material.SetColor("_Color", color);

            if (logEverything) Debug.LogWarning("Shield down");
            deflectorCollider.rigidbody.detectCollisions = false;
            isRebuilding = true;
            isFull = false;
        }
        else
        {
            if (logEverything) Debug.LogWarning("Shield HP:" + currentHP);

            Color color = bubble.renderer.material.GetColor("_Color");
            color.a = 1;
            bubble.renderer.material.SetColor("_Color", color);
            isFull = false;
        }
    }

    public void handleTriggerImpact(Collider other)
    {
        if (protectedObject == null) return;
		ChangeShieldColor ();

        if (protectedObject.tag == "Player" && BonusManager.Get() != null && BonusManager.Get().GodModeEnabled())
			return;

        if (isObjectValidTarget(other.gameObject))
        {
            if (!isUp())
            {
                currentHP = 0f;
                passThrough(other);
                return;
            }

            projectileScript p = other.gameObject.GetComponent<projectileScript>();
            float projectileDamage = 10f;
            if (p != null)
            {
                projectileDamage = p.projectileDamage;
            }

            projectileImpactDamage.damage = projectileDamage;
            handleImpact(projectileImpactDamage);
        }
        else
        {
            passThrough(other);
        }

        if (protectedObject.gameObject.tag == "Player" && GuiSliderBars.Get() != null)
		{
            GuiSliderBars.Get().SetShieldPoints(currentHP);
		}
    }

	private void ChangeShieldColor()
	{
        if (protectedObject != null && protectedObject.tag == "Player" && BonusManager.Get() != null && BonusManager.Get().GodModeEnabled()) 
		{
			Color color = Color.yellow;
			bubble.renderer.material.SetColor("_Color", color);
		}
		else 
		{
			Color color = bubble.renderer.material.GetColor("_Color");
			color.r = 1f - currentHP/startHP;
			color.g = currentHP/startHP;
            color.b = currentHP / startHP;
            color.a = currentHP / startHP;
			bubble.renderer.material.SetColor("_Color", color);
		}
	}


    float lastImpactTime = 0f;
    float impactFrequency = 8f;
    public void handleColliderImpact(Collision collision)
    {
        bool impacted = false;

        if ((lastImpactTime + (1 / impactFrequency)) > Time.time)
        {
            impacted = true;
        }
        else {
            lastImpactTime = Time.time;
        }

        foreach (ContactPoint contact in collision.contacts)
        {
            if (debugImpacts)
            {
                Debug.DrawRay(contact.point, contact.normal * 15f, Color.red, 2f, true);
                Debug.DrawRay(contact.point, contact.normal * -15f, Color.blue, 2f, true);
            }

            if (isDeflector)
            {
                contact.otherCollider.rigidbody.AddForce(contact.normal * -200f, ForceMode.VelocityChange);
            }
            else
            {
				if (contact.otherCollider.gameObject.tag == "Projectile" || contact.otherCollider.gameObject.tag == "Rocket" || contact.otherCollider.gameObject.tag == "EnemyProjectile")
                {
                    ProjectilePool.get().returnToPool(contact.otherCollider.gameObject);
                }
            }

            // pokazujemy efekt trafienia            
            if (!impacted && currentHP > 1f && impactEffect != null)
            {
                Quaternion impactRotation = Quaternion.identity;
                impactRotation = Quaternion.FromToRotation(transform.position, contact.point);

                particleEffectWrapper ex = Instantiate(impactEffect, contact.point, impactRotation) as particleEffectWrapper;
                if (ex != null)
                {
                    ex.play();
                    impacted = true;                      
                }
            }
        }

    }

    public bool isUp()
    {
        if (!isRebuilding || currentHP >= minHP) return true;

        if (protectedObject != null && protectedObject.tag == "Player" && BonusManager.Get() != null && BonusManager.Get().GodModeEnabled()) return true;

        return false;
    }

    public void setParent(GameObject o)
    {
        protectedObject = o;
    }

    private void activateComponents()
    {
        this.gameObject.SetActive(true);

        triggerCollider.gameObject.SetActive(true);
        triggerCollider.enabled = true;
        deflectorCollider.gameObject.SetActive(true);
        deflectorCollider.enabled = true;

        bubble.SetActive(true);
    }

    private void initializeComponents()
    {
        if (triggerCollider.gameObject.activeSelf && deflectorCollider.gameObject.activeSelf && triggerCollider.gameObject.activeInHierarchy && deflectorCollider.gameObject.activeInHierarchy)
            Physics.IgnoreCollision(triggerCollider.collider, deflectorCollider.collider);

        triggerCollider.rigidbody.detectCollisions = true;
        deflectorCollider.rigidbody.detectCollisions = true;

        triggerCollider.attach(this);
        deflectorCollider.attach(this);
    }

    public void activateShield()
    {
        if (protectedObject != null && protectedObject.collider != null)
        {
            bool protectedObjectActiveState = protectedObject.activeSelf;
            //this.transform.position = protectedObject.transform.position;

            reset();
            online();

            if (attachToParent)
                this.transform.parent = protectedObject.transform;

            if (protectedObjectActiveState == false)
            {
                protectedObject.SetActive(true);
            }

            activateComponents();
            initializeComponents();

            if (triggerCollider.gameObject.activeSelf && deflectorCollider.gameObject.activeSelf && triggerCollider.gameObject.activeInHierarchy && deflectorCollider.gameObject.activeInHierarchy)
            {
                if (protectedObject.activeSelf) Physics.IgnoreCollision(triggerCollider.collider, protectedObject.collider);
                if (protectedObject.activeSelf) Physics.IgnoreCollision(deflectorCollider.collider, protectedObject.collider);
            }

            if (protectedObject.tag == "Player")
            {
                int layerId = LayerMask.NameToLayer("PlayerShip");
                triggerCollider.gameObject.layer = layerId;
                deflectorCollider.gameObject.layer = layerId;
            }

            if (protectedObject.tag == "EnemyShip")
            {
                int layerId = LayerMask.NameToLayer("EnemyShip");

                triggerCollider.gameObject.layer = layerId;
                deflectorCollider.gameObject.layer = layerId;
            }

            if (protectedObjectActiveState == false)
            {
                protectedObject.SetActive(false);
            }

        }
    }

    void Awake(){
            this.gameObject.SetActive(true);

            if (deflectorCollider.collider == null)
            {
                Debug.LogError("deflectorCollider is null");
            }
            else if (triggerCollider.collider == null)
            {
                Debug.LogError("triggerCollider is null");
            }
            else if (bubble == null)
            {
                Debug.LogError("shield mesh is null");
            }
            else
            {
                activateComponents();
            }
    }

    void Start()
    {

        
        this.gameObject.SetActive(true);

        if (deflectorCollider.collider == null)
        {
            Debug.LogError("deflectorCollider is null");
        }
        else if (triggerCollider.collider == null)
        {
            Debug.LogError("triggerCollider is null");
        }
        else if (bubble == null)
        {
            Debug.LogError("shield mesh is null");
        }
        else
        {
            initializeComponents();
        }
        

    }

    // Update is called once per frame
    void Update()
    {
		ChangeShieldColor();

        if (shieldOnline)
        {

            //activateComponents();

            if (!isFull || isRebuilding)
            {
                currentHP += regenRate * Time.deltaTime;
                ChangeShieldColor();
            }

            if (isRebuilding && currentHP > minHP)
            {
                isRebuilding = false;
                // wlaczamy collider
                //deflectorCollider.gameObject.SetActive(true);
                deflectorCollider.rigidbody.detectCollisions = true;
                //triggerAbsorber();
            }

            if (!isFull && currentHP >= startHP)
            {
                isFull = true;
                currentHP = startHP;
            }

            /*
            if (!isRebuilding)
            {
                if (Input.GetKeyDown(KeyCode.LeftShift)) triggerDeflector();
                if (Input.GetKeyUp(KeyCode.LeftShift)) triggerAbsorber();
            }
            */

            bubble.renderer.material.SetFloat("_Offset", (Time.time / 50) % 1f);

            if (protectedObject != null && protectedObject.tag == "Player" && GuiSliderBars.Get() != null)
            {
                GuiSliderBars.Get().SetShieldPoints(currentHP);
            }

        }
    }

    public void reset() { 
        currentHP = startHP;
        minHP = 25f;
        isRebuilding = false;
        isFull = true;
        isDeflector = false;

        ///
        Color color = bubble.renderer.material.GetColor("_Color");
        color.a = 1;
        bubble.renderer.material.SetColor("_Color", color);
        ///

        deflectorCollider.rigidbody.detectCollisions = true;
    }

    public void online()
    {
        gameObject.SetActive(true);

        bubble.gameObject.SetActive(true);
        triggerCollider.gameObject.SetActive(true);
        deflectorCollider.gameObject.SetActive(true);
        ChangeShieldColor();

        shieldOnline = true;
    }

    public void offline()
    {
        gameObject.SetActive(false);
        
        bubble.SetActive(false);
        triggerCollider.gameObject.SetActive(false);
        deflectorCollider.gameObject.SetActive(false);

        shieldOnline = false;
    }

    public override JSONMonoBehaviour applyConfig(SimpleJSON.JSONClass config)
    {
        float configMaxHP = config["maxHP"].AsFloat;
        float configMinHP = config["minHP"].AsFloat;
        float configHPRegen = config["regen"].AsFloat;

        if (configMaxHP > 0f) startHP = configMaxHP;
        if (configMinHP > 0f) minHP = configMinHP;
        if (configHPRegen > 0f) regenRate = configHPRegen;

        return this;
    }

    internal void setRegen(float shieldRegen)
    {
        regenRate = shieldRegen;
    }
}
