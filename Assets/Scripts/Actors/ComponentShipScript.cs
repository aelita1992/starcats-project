﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ComponentShipScript : ShipScript
{
    ShipComponent baseComponent = null;
    Dictionary<string, ShipComponent> hullComponents = null;
    JSONClass currentLoadout = null;

	public delegate void DeathDelegate();
	public static event DeathDelegate OnDeath;

    public void criticalComponentLost()
    {
        GameObject boom = (GameObject)Resources.Load("Prefabs/Explosn");
        GameObject boomBoom = (GameObject)Instantiate(boom, this.transform.position, Quaternion.LookRotation(new Vector3(0f, 0f, -1f)));
        boomBoom.transform.localScale = Vector3.one * 15f;

        ComponentShipPool.get().returnToPool(gameObject);

		OnDeath();
    }

    private int componentsLost;
    public void componentLost()
    {
        componentsLost++;

        if (componentsLost >= 6)
            baseComponent.TurretsAutofire = true;
    }

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        // reset
        baseComponent = null;
        hullComponents = new Dictionary<string, ShipComponent>();

        // load hull
        JSONClass hullComponentInfo = config["hullComponents"].AsObject;

        foreach (string locationName in hullComponentInfo.Keys)
        {
            JSONClass locationData = hullComponentInfo[locationName].AsObject;

            string componentParentName = locationData["parent"].Value;
            string componentMountPointName = locationData["mountPoint"].Value;
            string componentPrefabName = locationData["componentName"].Value;

            float componentHP = locationData["hp"].AsFloat;
            bool isComponentCritical = locationData["isCritical"].AsBool;

            ShipComponent comopnent = ComponentPool.get().getScript(componentPrefabName);
            if (comopnent != null) {
                comopnent.name = locationName;
                comopnent.ParentShip = this;

                if (componentParentName == "root")
                {
                    if (baseComponent != null)
                        Debug.LogWarning("Multiple root components specified!");

                    baseComponent = comopnent;
                }
                else {
                    ShipComponent parentComponent = null;
                    if( hullComponents.TryGetValue(componentParentName, out parentComponent) )
                        parentComponent.mount(componentMountPointName, comopnent, Vector3.one, Vector3.zero);
                }

                if (componentHP > 0.1f) { comopnent.MaxHP = componentHP; }
                if (isComponentCritical) { comopnent.IsCritical = true; }

                hullComponents.Add(locationName, comopnent);
            }
        }

        if (baseComponent != null)
        {
            baseComponent.transform.rotation = transform.rotation;
            baseComponent.transform.position = transform.position;
            baseComponent.transform.parent = transform;

            baseComponent.ParentShip = this;

            baseComponent.alignAsEnemy();
        }
        else {
            Debug.LogError("Failed to instantiate ship's base component!");
        }

        currentLoadout = config["baseLoadout"].AsObject;
        applyLoadout(currentLoadout);

        return this;
    }

    public void applyLoadout( JSONClass loadout ) {
        foreach (string locationName in loadout.Keys)
        {
            JSONClass locationData = loadout[locationName].AsObject;
            ShipComponent locationComponent = null;

            if (hullComponents.TryGetValue(locationName, out locationComponent))
            {
                locationComponent.setupMounts(locationData);
            }
            else {
                Debug.LogWarning("Location not found: " + locationName);
            }

        }    
    }

    public override void cleanUp() {
        if (baseComponent != null)
        {
            ComponentPool.get().returnToPool(baseComponent.gameObject);
            baseComponent = null;
        }
    }

    public override float StartHP
    {
        get { return baseComponent != null ? baseComponent.MaxHP : 0f; }
        set { if (baseComponent != null) baseComponent.MaxHP = value; }
    }

    public override float CurrentHP
    {
        get { return baseComponent != null ? baseComponent.CurrentHP : 0f; }
        set { if (baseComponent != null) baseComponent.CurrentHP = value; }
    }

    public override float ShieldStartHP
    {
        get { return baseComponent != null ? baseComponent.MaxShield : 0f; }
    }

    public override float ShieldCurrentHP
    {
        get { return baseComponent != null ? baseComponent.CurrentShield : 0f; }
    }

    /*
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && baseComponent != null)
        {
            Debug.LogWarning("Fire!");
            baseComponent.fireWeaponsGroup(0, true);
        }

        if (Input.GetKeyDown(KeyCode.Space) && baseComponent != null)
        {
            Debug.LogWarning("Die!");
            kill();
        }
    }
    */

    public override void fireAllWeapons()
    {
        if (baseComponent != null) baseComponent.fireAllWeapons(true);
    }
    public override void fireWeaponsGroup(int group)
    {
        if (baseComponent != null) baseComponent.fireWeaponsGroup(group, true);
    }

    public override void kill()
    {
        if (baseComponent != null) baseComponent.deattachComponent();
    }

    public override bool IsAlive{ get
        {
        return  baseComponent != null ? baseComponent.isAlive() : false;
        }
    }

    protected bool isTargeted = false;
    public override bool IsTargeted
    {
        get { return isTargeted; }
        set { isTargeted = value; } // if (reticle != null) reticle.SetActive(value); 
    }

    public override void IncreaseHP(int value)
    {
        CurrentHP += value;
        if (CurrentHP > StartHP) CurrentHP = StartHP;

        if (gameObject.tag == "Player")
            GuiSliderBars.Get().SetHitPoints(CurrentHP);
    }
}
