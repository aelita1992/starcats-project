﻿using UnityEngine;
using System.Collections;

public class PoolsManager : MonoBehaviour 
{
	private static PoolsManager pseudoSingleton;

	public static PoolsManager Get()
	{
		return pseudoSingleton;
	}

	void Awake()
	{
		pseudoSingleton = this;
	}
}
