﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour 
{
	public enum VolumeEnum {MUSIC, EFFECTS}
	public enum MusicClipName 
	{
		MAIN_MENU_THEME, 
		INGAME_THEME,
		NONE
	}

	public enum EffectClipName
	{
		BUTTON1, 
		BUTTON2,
		ENERGY_WEAPON,
		MISSILE_ALERT,
		MISSILE_LAUNCH,
		NONE
	}

	public AudioClip mainMenuTheme;
	public AudioClip ingameTheme;

	public AudioClip buttonClip1;
	public AudioClip buttonClip2;
	public AudioClip energyWeapon;
	public AudioClip missileAlert;
	public AudioClip missileLaunch;

	Dictionary <MusicClipName, AudioClip> storedMusicClips; 
	Dictionary <EffectClipName, AudioClip> storedEffectClips; 

	MusicClipName currentlyPlayedMusicClip;
	EffectClipName currentlyPlayedEffectClip;
	public AudioSource musicSource;
	public AudioSource effectsSource;

	private static AudioManager pseudoSingleton;

	void Awake()
	{
		pseudoSingleton = this;
		currentlyPlayedEffectClip = EffectClipName.NONE;
	}

	public static AudioManager Get()
	{
		return pseudoSingleton;
	}

	public void Start()
	{
		storedMusicClips = new Dictionary<MusicClipName, AudioClip>();
		storedEffectClips = new Dictionary<EffectClipName, AudioClip>();

		storedMusicClips.Add (MusicClipName.MAIN_MENU_THEME, mainMenuTheme);
		storedMusicClips.Add (MusicClipName.INGAME_THEME, ingameTheme);

		storedEffectClips.Add (EffectClipName.BUTTON1, buttonClip1);
		storedEffectClips.Add (EffectClipName.BUTTON2, buttonClip2);
		storedEffectClips.Add (EffectClipName.ENERGY_WEAPON, energyWeapon);
		storedEffectClips.Add (EffectClipName.MISSILE_ALERT, missileAlert);
		storedEffectClips.Add (EffectClipName.MISSILE_LAUNCH, missileLaunch);
	}

	public void SetVolume(VolumeEnum volumeGroup, float newValue)
	{
		switch(volumeGroup)
		{
		case VolumeEnum.MUSIC:
			musicSource.volume = newValue;
			break;

		case VolumeEnum.EFFECTS:
			effectsSource.volume = newValue;
			break;
		}
	}

	public void PlayMusicClip(MusicClipName musicClipName)
	{
		if(musicClipName == currentlyPlayedMusicClip)
			return;

		musicSource.Stop();
		musicSource.clip = storedMusicClips[musicClipName];
		musicSource.Play();
		currentlyPlayedMusicClip = musicClipName;
	}

	public void PlayEffectClip(EffectClipName effectClipName)
	{
		if(effectClipName == currentlyPlayedEffectClip)
			return;

		effectsSource.Stop();
		effectsSource.clip = storedEffectClips[effectClipName];
		effectsSource.Play();
		currentlyPlayedEffectClip = effectClipName;
		Invoke ("ReleaseEffectsPlayer", energyWeapon.length);
	}

	public void StopEffectClip()
	{
		effectsSource.Stop();
		currentlyPlayedEffectClip = EffectClipName.NONE;
	}

	private void ReleaseEffectsPlayer()
	{
		currentlyPlayedEffectClip = EffectClipName.NONE;
	}
}
