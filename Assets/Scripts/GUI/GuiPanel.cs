﻿using UnityEngine;
using System.Collections;

public class GuiPanel : MonoBehaviour 
{
	public void Expand()
	{
		gameObject.SetActive (true);
	}

	public void Collapse()
	{
		gameObject.SetActive (false);
	}
}
