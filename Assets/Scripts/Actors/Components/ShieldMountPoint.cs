﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ShieldMountPoint : MountPoint{
    ShieldController attachedShield = null;
    string shieldName = "";
    float shieldStrength = 100f;
    float shieldRegen = 15f;

	public ShieldMountPoint(Transform mountLocation,Vector3 mountScale, Vector3 mountRotation) 
		: base(mountLocation,mountScale, mountRotation)
	{
		
	}

    

    public override void populate(Object mountedItem, Vector3 scale, Vector3 rotation)
    {
        if ( mountedItem.GetType() == typeof(ShieldController) || mountedItem.GetType().IsSubclassOf(typeof(ShieldController)))
        {
            if (attachedShield != null)
                cleanUp();

            //Debug.LogWarning("Attaching Shield: " + shieldName);
            attachedShield = (ShieldController)mountedItem;

            setupTransform(attachedShield.transform);

            attachedShield.attachToParent = false;
            attachedShield.setParent(ParentComponent.gameObject);
            attachedShield.activateShield();

            if (scale != Vector3.one) attachedShield.transform.localScale += scale;
            if (rotation != Vector3.zero) attachedShield.transform.Rotate(rotation);

            ParentComponent.registerShield(attachedShield);
        }
        else
        {
            Debug.LogWarning("Invalid object type for mount point! " + mountedItem.GetType());
        }
    }

    public override void cleanUp()
    {
        if (attachedShield != null)
        {
            // Debug.LogWarning("Cleanup Shield: " + shieldName);

            ParentComponent.unregisterShield(attachedShield);

            attachedShield.offline();
            attachedShield.transform.parent = null;
            attachedShield.setParent(null);            

            ShieldPool.get().returnToPool(attachedShield.gameObject);
            attachedShield = null;
        }
    }
    public override void setup(JSONClass mountInfo)
    {
        shieldName = mountInfo["name"].Value;
        shieldStrength = mountInfo["shieldStrength"].AsFloat;
        shieldRegen = mountInfo["shieldRegen"].AsFloat;

        ShieldController newShield = ShieldPool.get().getScript(shieldName);
        if (newShield != null)
        {
            newShield.setStrength(shieldStrength);
            newShield.setRegen(shieldRegen);
            //Debug.LogWarning("Shield: " + shieldStrength + " / " + shieldRegen);

            populate(newShield, Vector3.one, Vector3.zero);
        }
        else
        {
            Debug.LogWarning("Shield not found: " + shieldName);
        }
    }

}
