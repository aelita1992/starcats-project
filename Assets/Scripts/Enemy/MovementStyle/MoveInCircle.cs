﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveInCircle : MovementController
{
	public enum MovementStage {Init = 0, BabyRoundRound = 1, Leaving = 2, Exit = -1}

	private List<Vector3> movementCenters;
	float amplitudeX = 400.0f;
	float amplitudeY = 200.0f;
	float omegaX = 0.1f;
	float omegaY = 0.1f;
	float index;

	public override void AddEnemy(GameObject enemyShip)
	{
		if(!isInitiated)
		{
			InitController ();
			movementCenters = new List<Vector3>();
		}

		EnemyBehaviour behaviourToAssign = enemyShip.GetComponent<EnemyBehaviour>();

		behaviourToAssign.horizontalMovementStage = 0;
		behaviourToAssign.verticalMovementStage = 0;
		
		enemyBehaviours.Add (behaviourToAssign);
		enemyObjects.Add (behaviourToAssign.gameObject);

		Vector3 basePosition = behaviourToAssign.gameObject.transform.position;
		movementCenters.Add(basePosition);
	}

	protected new void RemoveEnemyOnPosition(int position)
	{
		movementCenters.RemoveAt(position);
	}

	protected override void ExecuteHorizontalMovement(int horizontalMovementStage)
	{	
		switch(horizontalMovementStage)
		{
		case (int)MovementStage.Init:
			Init();
			break;
			
		case (int)MovementStage.BabyRoundRound:
			CalculateHorizontalChange();
			break;
			
		case (int)MovementStage.Leaving:
			break;
			
		case (int)MovementStage.Exit:
			Exit ();
			break;
		}
	}

	protected override void ExecuteVerticalMovement(int verticalMovementStage)
	{	
		switch(verticalMovementStage)
		{
		case (int)MovementStage.Init:
			Init();
			break;
			
		case (int)MovementStage.BabyRoundRound:
			CalculateVerticalChange();
			break;
			
		case (int)MovementStage.Leaving:
			break;
			
		case (int)MovementStage.Exit:
			Exit ();
			break;
		}
	}

	private void Init()
	{
		enemyBehaviours[shipIterator].Speed = new Vector3(0,0,0);
		enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.BabyRoundRound;
		enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.BabyRoundRound;
	}

	private void CalculateHorizontalChange()
	{
		index += Time.deltaTime;
		float x = amplitudeX*Mathf.Cos (omegaX*index);

		Vector3 position = enemyObjects[shipIterator].transform.position;
		position.x = movementCenters[shipIterator].x + x;
		enemyObjects[shipIterator].transform.position = position;
	}

	private void CalculateVerticalChange()
	{
		float y = amplitudeY*Mathf.Sin (omegaY*index);
		
		Vector3 position = enemyObjects[shipIterator].transform.position;
		position.y = movementCenters[shipIterator].y + y;
		enemyObjects[shipIterator].transform.position = position;
	}

	private void Exit()
	{

	}

}
