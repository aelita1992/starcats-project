﻿using UnityEngine;
using System.Collections;

public class TriggerDetector : MonoBehaviour 
{
    ShipScript shipScript;

	void Start()
	{
        shipScript = this.gameObject.GetComponent<ShipScript>();
	}

	void OnTriggerEnter (Collider other) 
	{
		if(other.tag == "Bonus")
		{
			BonusScript.BonusType bonusType = other.gameObject.GetComponent<BonusScript>().GetBonusType();

			switch(bonusType)
			{
			case BonusScript.BonusType.GODMODE:
				BonusManager.Get ().EnableGodMode();
				Debug.LogWarning("God mode activated");
				break;

			case BonusScript.BonusType.REPAIR:
				shipScript.IncreaseHP(50);
				Debug.Log ("Life increased");
				break;

			case BonusScript.BonusType.WEAPON:
				TeslaAmmoManager.Get ().IncreaseAmmo();
				Debug.LogWarning("Maximum firepower");
				break;

			case BonusScript.BonusType.DOUBLE_POINTS:
				ScoreHandle.Get ().SetMultipler(5);
				Debug.LogWarning("We need MOAR POINTZ");
				break;
			}

			other.gameObject.SetActive(false);
		}

	}
}
