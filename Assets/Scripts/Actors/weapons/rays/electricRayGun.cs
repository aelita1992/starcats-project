﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class electricRayGun : rayGun
{

    protected float fireTimeRatio = 0.75f;

    protected float beamTime = 0.125f;
    protected float beamWidthBase = 15f;
    protected float beamWidth = 45f;

    protected float arcTime = 0.8f;
    protected const int arcs = 5;
    protected const int arcSegments = 12;

    protected const float arcSpreadDelta = 4f;
    protected const float arcSpread = 8f;
    protected const float arcWidth = 1.5f;

    private DamageInfo beamDamageInfo = null;
    private DamageInfo arcDamageInfo = null;

    protected LineRenderer beam = null;
    protected LineRenderer[] beamArcs = null;

    GameObject sparksProto = null;

    public electricRayGun() {
        beamDamageInfo = new DamageInfo(25000f, this);
        arcDamageInfo = new DamageInfo(250f, this);
    }

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        base.applyConfig(config);

        sparksProto = Resources.Load("Prefabs/Sparks") as GameObject;

        //////// !!!
        detectRadius = 25f;
        //////// !!!

        // glowny promien
        beam = beamBase.gameObject.AddComponent<LineRenderer>();
        beam.material = new Material(Shader.Find("Particles/Additive"));
        beam.SetColors(Color.white, Color.white);
        beam.SetWidth(beamWidthBase, beamWidth);

        beam.SetPosition(0, beamBase.transform.position);
        beam.SetPosition(1, beamBase.transform.position);

        // luki elektryczne
        beamArcs = new LineRenderer[arcs];
        for (int i = 0; i < arcs; i++)
        {

            GameObject arcWrapper = new GameObject();
            arcWrapper.name = "Arc " + i;
            arcWrapper.transform.parent = beamBase.transform;

            LineRenderer arc = arcWrapper.AddComponent<LineRenderer>();
            arc.material = new Material(Shader.Find("Particles/Additive"));
            arc.SetColors(Color.blue, Color.blue);
            arc.SetWidth(arcWidth, arcWidth);
            arc.SetVertexCount(arcSegments);

            for (int j = 0; j < arcSegments; j++)
            {
                arc.SetPosition(j, beamBase.transform.position);
            }

            beamArcs[i] = arc;
        }

        initialized = true;

        return this;
    }

    private void drawArc(LineRenderer arc, Vector3 start, Vector3 end, float arcRatio, float size)
    {

        arc.SetPosition(0, start);
        arc.SetPosition(arcSegments - 1, end);

        for (int j = 1; j < arcSegments - 1; j++)
        {
            Vector3 pos = Vector3.Lerp(start, end, (float)j / (arcSegments - 1));

            float spread = (arcSpread + arcSpreadDelta * size) * arcRatio;

            pos.x += Random.Range(-1f * spread, spread);
            pos.y += Random.Range(-1f * spread / 10f, spread / 10f);
            pos.z += Random.Range(-1f * spread, spread);

            arc.SetPosition(j, pos);
        }  
  
    }

    Vector3 lastTargetPosition = Vector3.zero;
    protected override void updateBeam()
    {
        // gotowe do strzalu -> wylaczamy promien i efekty
        if (canFire())
        {
            if (beam.enabled)
            {
                beam.enabled = false;
                for (int i = 0; i < arcs; i++)
                {
                    LineRenderer arc = beamArcs[i];
                    arc.enabled = false;
                }
                //Debug.Log("disabled beam");
            }
        }
        else if (nextFire > 0f)
        {
            // bron w trakcie strzalu - wlaczamy efekty
            if (!beam.enabled)
            {
                beam.enabled = true;
                for (int i = 0; i < arcs; i++)
                {
                    beamArcs[i].enabled = true;
                }
            }

            float timeUntilNextShot = nextFire - Time.time;

            if (timeUntilNextShot > 0f)
            {
                // kawal matmy: liczymy ile czasu zostalo przed nastepnym strzalem na wyswietlanie promienia i lukow elektrycznych
                //              uwzgledniajac to, ze efekt nie jest widoczny 100% czasu miedzy strzalami
                float timeBetweenShots = 1f / rateOfFire;
                float timeToFire = timeBetweenShots * fireTimeRatio;
                float coolDown = timeBetweenShots - timeToFire;

                float timeLeft = timeUntilNextShot - coolDown;
                //float timeLeftRatio = timeLeft / timeToFire;
                float timeElapsed = timeToFire - timeLeft;

                float beamTimeStart = beamTime * timeToFire;
                float beamTimeLeft = beamTimeStart - timeElapsed;
                float beamRatio = beamTimeLeft / beamTimeStart;



                float arcTimeStart = arcTime * timeToFire;
                float arcTimeLeft = arcTimeStart - timeElapsed;
                float arcRatio = arcTimeLeft / arcTimeStart;

                Vector3 beamEnd = attachBone.position;
                DamageInfo currentDamageType = null;

                if (beamTimeLeft > 0f)
                {
                    beam.SetWidth(beamWidthBase * beamRatio, beamWidth * beamRatio);
                    currentDamageType = beamDamageInfo;
                    trackTarget = false;
                }
                else
                {
                    beam.enabled = false;
                    currentDamageType = arcDamageInfo;
                    trackTarget = true;
                }

                // jezeli mamy bron ktora strzela prosto, sprawdzamy, czy nie pojawil sie jakis cel
                if (lookForMoreTargets && target == null)
                {
                    updateTarget();
                }

                if (target != null)
                {
                    // cel jeszcze zyje, 
                    if (target.activeSelf)
                    {
                        lastTargetPosition = target.transform.position;

                        if (trackTarget)
                        {
                            beamEnd = lastTargetPosition;
                        }
                        else
                        {
                            beamEnd = muzzleBone.position + muzzleBone.up * hit.distance;
                        }

                        if (beamTimeLeft > 0f || arcTimeLeft > 0f)
                            target.SendMessageUpwards("handleImpact", currentDamageType);
                    }

                    if (!target.activeSelf)
                    {
                        // efekt anihilacji (tylko dla glownego promienia)
                        if (beamTimeLeft > 0f)
                        {
                            GameObject boom = Instantiate(sparksProto, lastTargetPosition, Quaternion.identity) as GameObject;
                            boom.transform.localScale = Vector3.one * 5f;
                        }

                        // szukamy nastepnego celu
                        if (lookForMoreTargets)
                            updateTarget();
                    }

                }
                else
                {
                    beamEnd = muzzleBone.position + muzzleBone.up * detectRange;
                }

                beam.SetPosition(0, muzzleBone.position);
                beam.SetPosition(1, beamEnd);


                if (arcTimeLeft > 0f)
                {
                    ///
                    for (int i = 0; i < arcs; i++)
                    {
                        LineRenderer arc = beamArcs[i];
                        arc.SetWidth(arcWidth * 2f, arcWidth);
                        arc.SetColors(Color.white, Color.blue);

                        drawArc(arc, muzzleBone.position, beamEnd, arcRatio, i);
                    }
                    ///

                    // sparks damage nearby targets!
                    if (target != null && target.activeSelf)
                    {
                        Collider[] hitColliders = Physics.OverlapSphere(target.transform.position, 150f, detectionLayers);
                        int i = 0;
                        while (i < hitColliders.Length)
                        {
                            Collider c = hitColliders[i];

                            if (c.gameObject != target && c.transform.parent.gameObject != target && Vector3.Distance( c.transform.position, target.transform.position) >= 5f )
                            {
                                if (arcs > i+1)
                                {
                                    LineRenderer arc = beamArcs[i+1];
                                    arc.SetWidth(arcWidth * 5f, arcWidth * 2f);
                                    arc.SetColors(Color.cyan, Color.blue);

                                    drawArc(arc, target.transform.position, c.transform.position, arcRatio*2f, i*2f);
                                }

                                c.SendMessageUpwards("handleImpact", arcDamageInfo);
                                Debug.Log("arc damage to " + c.gameObject.name, c.gameObject);
                            }

                            i++;
                        }
                    }
                    else if (target != null)
                    {
                        target = null;
                    }
                }
                else
                {
                    for (int i = 0; i < arcs; i++)
                    {
                        beamArcs[i].enabled = false;
                    }
                }
            }

        }
    }
}