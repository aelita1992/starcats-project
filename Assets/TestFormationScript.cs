﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestFormationScript : MonoBehaviour
{
	public EnemyMovementManager.MovementType movementType;
	GameObject prefab;

	List<GameObject> ships;

	void Awake(){
		prefab = Resources.Load("shipMarker") as GameObject;
	}

	void Start()
	{
		ships = new List<GameObject>();

		for(int i = 0; i < 9; i++)
		{
			GameObject instance = UnityEngine.GameObject.Instantiate(prefab) as GameObject;
			instance.transform.position = new Vector3(0f, 200f, -65f);
			shipSpawner shipMarker = instance.GetComponent<shipSpawner>();
			ShipScript shipScript = shipMarker.ship;
			shipScript.gameObject.transform.position = Vector3.zero;
			ships.Add (shipScript.gameObject);
		}


		FormationsController.FormationEnum formation = FormationsController.FormationEnum.TRIFORCE;
		FlightTower.Get().PrepareFlight(formation, movementType, ships, FormationsController.Direction.DOWN, new Vector3(0,200,-65f), new Vector2(64,64));

	}
}
