﻿using UnityEngine;
using System.Collections;

public class BonusSpawner : MonoBehaviour 
{
	public GameObject godModeBonus;
	public GameObject weaponBonus;
	public GameObject repairBonus;
	public GameObject doublePoints;

	GameObject[] bonuses;

	void Start()
	{
		bonuses = new GameObject[4];
		bonuses [0] = godModeBonus;
		bonuses [2] = repairBonus;
		//bonuses [1] = doublePoints;
		bonuses [3] = weaponBonus;
	}

	void OnEnable()
	{
		TimeManager.Get ().timeDependantUpdate_10seconds += SpawnBonus;
	}

	void OnDisable()
	{
		TimeManager.Get ().timeDependantUpdate_10seconds -= SpawnBonus;
	}

	private void SpawnBonus()
	{
		int bonusNumber = Random.Range (0, 100);
		int bonusIndex = 0;

		if(bonusNumber >= 0 && bonusNumber < 10)
		{
			if(BonusManager.Get ().GodModeEnabled())
			{
				SpawnBonus();
				return;
			}

			bonusIndex = 0;
		}

		/*if(bonusNumber >= 10 && bonusNumber < 50)
		{
			//bonusIndex = 1;
		}*/

		if(bonusNumber >= 10 && bonusNumber < 30)
		{
			bonusIndex = 2;
		}

		if(bonusNumber >= 30 && bonusNumber <= 100)
		{
			bonusIndex = 3;
		}

		bonuses [bonusIndex].transform.position = gameObject.transform.position;
		bonuses [bonusIndex].SetActive (true);
	}
}
