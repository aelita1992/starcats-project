﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretController2 : JSONMonoBehaviour
{
    public bool fireOnTarget = false;
    public bool autoFire = false;

    public GameObject looker = null;
    public GameObject turretBase = null;
    public GameObject leftGunArm = null;
    public GameObject rightGunArm = null;

    public Transform gunBoneTopLeft = null;
    public Transform gunBoneBottomLeft = null;
    public Transform gunBoneTopRight = null;
    public Transform gunBoneBottomRight = null;
    private Transform[] guns = null;
    private List<gunScript> weapons = new List<gunScript>();

    public List<gunScript> Weapons
    {
        get { return weapons; }
        set { weapons = value; }
    }

    public string upperGunName = "testGun4";
    public string lowerGunName = "testGun2";

    float leftGunArmBaseConvergenceAngle = 0f;
    float rightGunArmBaseConvergenceAngle = 0f;

    float maxConvergenceAdjustment = 10f;
    float zeroConvergencePoint = 256f;
    float maxConvergencePoint = 8f;

    //float maxGunConvergenceAdjustment = 12.5f;

    public GameObject targetObject = null;
    Vector3 targetPosition = Vector3.zero;

    //private float minAdjustmentTime = 15.0f; // the shortest time an adjustment may take, to ensure smooth movement
    private float adjustmentOffset = 0.05f; // affects how much the target has to move before updating the target coords, also how much off target the turret needs to be to move
    private float targetOffset = 15.0f; // affects how far off-target can we get and still fire

    public float speed = 25f;     // movement speed

    private float startTime = 0;

    private Quaternion lastUpdateRotation = Quaternion.identity;

    private Quaternion horizontalInitialRotation = Quaternion.identity;
    private Quaternion horizontalTargetRotation = Quaternion.identity;
    private float horizontalStart = 0;
    private float horizontalEnd = 0;
    private float horizontalEndTime = 0;

    private Quaternion verticalInitialRotationLeft = Quaternion.identity;
    private Quaternion verticalInitialRotationRight = Quaternion.identity;
    private Quaternion verticalTargetRotationLeft = Quaternion.identity;
    private Quaternion verticalTargetRotationRight = Quaternion.identity;
    private float verticalStart = 0;
    private float verticalEnd = 0;
    private float verticalEndTime = 0;

    private float horizontalLength = 0;
    private float verticalLength = 0;

    void Start()
    {
        leftGunArmBaseConvergenceAngle = leftGunArm.transform.localRotation.eulerAngles.z;
        rightGunArmBaseConvergenceAngle = rightGunArm.transform.localRotation.eulerAngles.z;

        guns = new Transform[] { gunBoneTopLeft, gunBoneBottomLeft, gunBoneTopRight, gunBoneBottomRight };

        foreach (Transform gunMount in guns)
        {
            GameObject gun = null;

            if (gunMount == gunBoneBottomLeft || gunMount == gunBoneBottomRight)
            {
                gun = WeaponPool.get().getPrefab(lowerGunName, gunMount.transform.position, gunMount.transform.rotation);
            }
            else
            {
                gun = WeaponPool.get().getPrefab(upperGunName, gunMount.transform.position, gunMount.transform.rotation);
            }

			gun.transform.parent = this.transform;

            gunScript weapon = gun.GetComponent<gunScript>();
            Transform gunBone = weapon.getAttachBone();

            gunBone.parent = gunMount;
            weapons.Add(weapon);

            if (gunMount == gunBoneBottomLeft || gunMount == gunBoneBottomRight)
            {                
                gunBone.Rotate(0f, 180f, 0f);
            }

            gunBone.localScale = Vector3.one;
        }
    }

    public void setTarget(GameObject target)
    {
        targetObject = target;
        targetPosition = Vector3.zero;
        resetTrackingData();
    }

    public void setPoint(Vector3 target)
    {
        targetPosition = target;
        targetObject = null;
        resetTrackingData();
    }

    float convergenceAdjustment = 0f;
    private void convergeOnTarget(Vector3 target)
    {
        float distance = (looker.transform.position - target).magnitude;
        float convergenceAdjustmentFactor = Mathf.Clamp(distance, maxConvergencePoint, zeroConvergencePoint) / maxConvergencePoint;
        convergenceAdjustment = maxConvergenceAdjustment / convergenceAdjustmentFactor;

        //Debug.Log("Distance: " + distance);
        //Debug.Log("convergenceAdjustmentFactor: " + convergenceAdjustmentFactor);
        //Debug.Log("convergenceAdjustment: " + convergenceAdjustment);
        //if (Input.GetKey(KeyCode.LeftControl))

        leftGunArm.transform.localRotation = Quaternion.Euler(leftGunArm.transform.localRotation.eulerAngles.x, leftGunArm.transform.localRotation.eulerAngles.y, leftGunArmBaseConvergenceAngle + convergenceAdjustment);
        rightGunArm.transform.localRotation = Quaternion.Euler(rightGunArm.transform.localRotation.eulerAngles.x, rightGunArm.transform.localRotation.eulerAngles.y, rightGunArmBaseConvergenceAngle - convergenceAdjustment);

        foreach (gunScript gun in weapons)
        {
            Transform gunBone = gun.getAttachBone();
            gunBone.localRotation = Quaternion.Euler(-convergenceAdjustment, gunBone.localRotation.eulerAngles.y, gunBone.localRotation.eulerAngles.z);
        }
    }


    private void resetTrackingData()
    {
        /////////////////////
        // initPointTrackingData()
        startTime = 0;

        /////////////////////
        // updatePointTrackingStartPosition()
        horizontalInitialRotation = Quaternion.identity;
        verticalInitialRotationLeft = Quaternion.identity;
        verticalInitialRotationRight = Quaternion.identity;

        /////////////////////
        // updatePointTrackingData()
        looker.transform.LookAt(Vector3.zero);
        lastUpdateRotation = Quaternion.identity;

        horizontalStart = 0f;
        horizontalEnd = 0f;

        verticalStart = 0f;
        verticalEnd = 0f;

        horizontalTargetRotation = Quaternion.identity;

        verticalTargetRotationLeft = Quaternion.identity;
        verticalTargetRotationRight = Quaternion.identity;

        horizontalLength = 0f;
        verticalLength = 0f;


        horizontalEndTime = 0f;
        verticalEndTime = 0f;
    }

    private void initPointTrackingData(Vector3 target)
    {
        startTime = Time.time;
        updatePointTrackingStartPosition();
        updatePointTrackingData(target);

        if (Input.GetKey(KeyCode.LeftShift) && this.gameObject.tag == "Player")
        {
            Debug.LogWarning("initPointTrackingData@startTime: " + startTime);

            Debug.Log("horizontalStart: " + horizontalStart);
            Debug.Log("horizontalEnd: " + horizontalEnd);
            Debug.Log("horizontalLength: " + horizontalLength);
            Debug.Log("horizontalEndTime: " + horizontalEndTime);

            Debug.Log("verticalStart: " + verticalStart);
            Debug.Log("verticalEnd: " + verticalEnd);
            Debug.Log("verticalLength: " + verticalLength);
            Debug.Log("verticalEndTime: " + verticalEndTime);
        }
    }

    private void updatePointTrackingStartPosition()
    {
        horizontalInitialRotation = turretBase.transform.localRotation;
        verticalInitialRotationLeft = leftGunArm.transform.localRotation;
        verticalInitialRotationRight = rightGunArm.transform.localRotation;
    }

    private void updatePointTrackingData(Vector3 target)
    {
        looker.transform.LookAt(target);
        Quaternion baseRotation = looker.transform.localRotation;
        lastUpdateRotation = baseRotation;

        horizontalStart = turretBase.transform.localRotation.eulerAngles.y;
        horizontalEnd = baseRotation.eulerAngles.y + 90f;

        verticalStart = rightGunArm.transform.localRotation.eulerAngles.y;
        verticalEnd = baseRotation.eulerAngles.x;


        horizontalTargetRotation = Quaternion.Euler(turretBase.transform.localRotation.eulerAngles.x, horizontalEnd, turretBase.transform.localRotation.eulerAngles.z);

        verticalTargetRotationLeft = Quaternion.Euler(leftGunArm.transform.localRotation.eulerAngles.x, verticalEnd * -1f, leftGunArmBaseConvergenceAngle + convergenceAdjustment);
        verticalTargetRotationRight = Quaternion.Euler(rightGunArm.transform.localRotation.eulerAngles.x, verticalEnd * 1f, rightGunArmBaseConvergenceAngle - convergenceAdjustment);

        horizontalLength = Quaternion.Angle(horizontalInitialRotation, horizontalTargetRotation);

        verticalLength = Quaternion.Angle(verticalInitialRotationRight, verticalTargetRotationRight);

        horizontalEndTime = (horizontalLength / speed);
        verticalEndTime = (verticalLength / speed);

        //horizontalEndTime = Mathf.Max((horizontalLength / speed), 0.01f);
        // verticalEndTime = Mathf.Max((verticalLength / speed), 0.01f);
    }

    bool onTarget = false;
    bool converged = false;

    float easeMovement(float t)
    {
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        return t;
    }

    public void fire()
    {
        foreach (gunScript gun in weapons)
        {
            gun.fireIfReady();
        }
    }

    float timeElapsed = 0;
    float horizontalProgress = 0;
    float verticalProgress = 0;
    // Update is called once per frame
    void FixedUpdate()
    {
		if (autoFire) {
			fire ();
		}

        if (Input.GetKey(KeyCode.Mouse0))
        {
            if (Input.GetKey(KeyCode.LeftControl) )
                fire();
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {

            Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(path, out hit))
            {
                if (hit.collider.gameObject.tag != "Projectile")
                {
                    if (Input.GetKey(KeyCode.LeftControl))
                    {
                        setPoint(hit.point);
                    }
                    else
                    {
                        setTarget(hit.collider.gameObject);
                    }
                }
            }

        }

        Vector3 target = Vector3.zero;
        if (targetObject != null && Input.GetKey(KeyCode.Mouse1) == false)
        {
            target = targetObject.transform.position;
        }
        else if (targetPosition != Vector3.zero)
        {
            target = targetPosition;
        }

        if (target != Vector3.zero)
        {
            // startTime being zero means tracking was reset
            // if we're off target, start a new tracking sequence
            if (startTime == 0f || (onTarget && (horizontalLength > adjustmentOffset || verticalLength > adjustmentOffset)))
            {
                initPointTrackingData(target);
                onTarget = false;
                converged = false;
            }
            else
            {
                looker.transform.LookAt(target);
                if (Quaternion.Angle(lastUpdateRotation, looker.transform.localRotation) > adjustmentOffset || Input.GetKey(KeyCode.LeftShift))
                    updatePointTrackingData(target);
            }

            if (!onTarget)
            {
                timeElapsed = (Time.time - startTime) + 0.0001f;
                horizontalProgress = easeMovement(2 / (1 + horizontalEndTime / timeElapsed));
                verticalProgress = easeMovement(2 / (1 + verticalEndTime / timeElapsed));
                /*
                Debug.Log("timeElapsed: " + timeElapsed);
                Debug.Log("horizontalProgress: " + horizontalProgress + " / " + horizontalEndTime);
                Debug.Log("verticalProgress: " + verticalProgress + " / " + verticalEndTime);
                 */

                if (horizontalLength <= targetOffset && verticalLength <= targetOffset) {
                    if (fireOnTarget)
                        fire();
                }
            }

            if (!onTarget && (horizontalProgress <= 1f || verticalProgress <= 1f))
            {

                if (horizontalEndTime > 0f)
                    turretBase.transform.localRotation =
                        Quaternion.Slerp(
                            horizontalInitialRotation,
                            horizontalTargetRotation,
                            horizontalProgress);

                if (verticalEndTime > 0f)
                    leftGunArm.transform.localRotation =
                        Quaternion.Slerp(
                            verticalInitialRotationLeft,
                            verticalTargetRotationLeft,
                            verticalProgress);

                if (verticalEndTime > 0f)
                    rightGunArm.transform.localRotation =
                        Quaternion.Slerp(
                            verticalInitialRotationRight,
                            verticalTargetRotationRight,
                            verticalProgress);

                if (Input.GetKey(KeyCode.LeftControl) && this.gameObject.tag == "Player")
                {
                    Debug.Log("Time: " + Time.time + " * " + speed);
                    Debug.Log("horizontalProgress: " + horizontalProgress + " / " + horizontalEndTime);
                    Debug.Log("verticalProgress: " + verticalProgress + " / " + verticalEndTime);
                }
            }
            else
            {
                
                if (!onTarget)
                {
                    if (this.gameObject.tag == "Player")
                    {
                        Debug.LogWarning("Reached target@ " + Time.time + " ( " + (Time.time - startTime) + " / " + (Mathf.Max(horizontalEndTime, verticalEndTime)) + " )");
                        Debug.Log("horizontalProgress: " + horizontalProgress + " / " + horizontalEndTime);
                        Debug.Log("verticalProgress: " + verticalProgress + " / " + verticalEndTime);

                        Debug.Log("turretBase: " + turretBase.transform.localRotation.eulerAngles);
                        Debug.Log("horizontalTargetRotation: " + horizontalTargetRotation.eulerAngles);
                    }

                    //turretBase.transform.localRotation = horizontalTargetRotation;
                    //leftGunArm.transform.localRotation = verticalTargetRotationLeft;
                    //rightGunArm.transform.localRotation = verticalTargetRotationRight;

                    onTarget = true;
                }

                // once pointed in the right direction, converge the arms
                if (!converged)
                {
                    convergeOnTarget(target);
                    converged = true;
                }

                if (fireOnTarget)
                {
                    fire();
                }
            }
        }

    }

    public override JSONMonoBehaviour applyConfig(SimpleJSON.JSONClass config)
    {
        if( config["upperGun"].Value.Length > 0 ) upperGunName = config["upperGun"].Value;
        if (config["lowerGun"].Value.Length > 0) lowerGunName = config["lowerGun"].Value;

        return this;
    }
}
