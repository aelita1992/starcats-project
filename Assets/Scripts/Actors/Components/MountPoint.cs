﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public abstract class MountPoint{
    protected ShipComponent parentComponent = null;
    protected ComponentShipScript parentShip = null;

    protected Transform mountLocation = null;
    protected Vector3 mountScale = Vector3.one;
    protected Vector3 mountRotation = Vector3.one;

    protected GameObject mountedItem = null;
	
	public ShipComponent ParentComponent {
		get {
			return parentComponent;
		}
		set {
			parentComponent = value;
		}
	}

    public ComponentShipScript ParentShip
    {
		get {
			return parentShip;
		}
		set {
			parentShip = value;
		}
	}
	
	protected MountPoint(Transform mountLocation,Vector3 mountScale, Vector3 mountRotation){
		this.mountLocation = mountLocation;
		this.mountScale = mountScale;
		this.mountRotation = mountRotation;
	}

    protected void setupTransform(Transform mountedTransform)
    {
        if (mountedTransform == null || mountLocation == null)
        {
            if (mountedTransform == null) Debug.LogError("mountedTransform is null");
            if (mountLocation == null) Debug.LogError("mountLocation is null");
            return;
        }

        
        mountedTransform.parent = mountLocation;
        mountedTransform.localPosition = Vector3.zero;

        mountedTransform.localScale = mountScale;
        mountedTransform.localRotation = Quaternion.Euler(mountRotation);
    }
    protected void setupTransformAligned(Transform mountedTransform, Transform attachTransform)
    {
        if (mountedTransform == null || mountLocation == null)
        {
            if (mountedTransform == null) Debug.LogError("mountedTransform is null");
            if (mountLocation == null) Debug.LogError("mountLocation is null");
            return;
        }

        //mountedTransform.position = mountLocation.position;
        mountedTransform.parent = mountLocation;

        mountedTransform.localPosition = Vector3.zero;
        mountedTransform.localRotation = Quaternion.identity;
        mountedTransform.localScale = Vector3.one;

        attachTransform.position = mountLocation.position;
        //attachTransform.parent = mountLocation;

        attachTransform.localScale = mountScale;
        attachTransform.localRotation = Quaternion.Euler(mountRotation);
    }

    public abstract void populate(Object mountedItem, Vector3 scale, Vector3 rotation);
	public abstract void cleanUp ();

    public abstract void setup(JSONClass mountInfo);
}