﻿using UnityEngine;
using System.Collections;

public class TurretController : MonoBehaviour {

    public GameObject looker = null;
    public GameObject turretBase = null;
    public GameObject gun1 = null;
    public GameObject gun2 = null;

    float gunBaseRotation = 0f;

    void Awake()
    {
        gunBaseRotation = gun1.transform.rotation.x*100f;
    }



	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Mouse1))
        {

            Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(path, out hit))
            {
                looker.transform.LookAt(hit.point);

                turretBase.transform.localRotation = Quaternion.Euler(turretBase.transform.rotation.x, looker.transform.rotation.y * 100f, turretBase.transform.rotation.z);
                gun1.transform.localRotation = Quaternion.Euler(looker.transform.rotation.x * 100f + gunBaseRotation, gun1.transform.rotation.y, gun1.transform.rotation.z);
                gun2.transform.localRotation = Quaternion.Euler(looker.transform.rotation.x * 100f + gunBaseRotation, gun2.transform.rotation.y, gun2.transform.rotation.z);
            }
        }
	}
}
