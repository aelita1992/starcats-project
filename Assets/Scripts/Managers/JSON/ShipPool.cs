﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ShipPool : JSONAssetManager<ShipPool, ShipScript>
{
    public ShipPool()
    {
        fileName = "JSON/shipManagerConfig";
        listName = "ships";
        typeName = "ships";
    }

    protected override GameObject processAsset(GameObject asset, JSONClass config)
    {
        return asset;
    }


    protected override GameObject preparePrefab(GameObject prefab, JSONClass config)
    {
        //shipScript2 ship = prefab.GetComponent<shipScript2>();
        //ship.reset();
        return prefab;
    }

    public override GameObject _getPrefab(string name, Vector3 position, Quaternion rotation)
    {
        GameObject prefab = getPrefab(name);
        if (prefab != null)
        {
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            return prefab;
        }
        else
        {
            if (logWarnings) Debug.LogWarning("Prefab not found:" + name);
        }

        return null;
    }
}