﻿using UnityEngine;
using System.Collections;


//[ExecuteInEditMode]
public class ComponentShipSpawner : MonoBehaviour {

    ComponentShipScript ship = null;

	// Use this for initialization
	void spawnShip () {
        if (ship != null)
        {
            ComponentShipPool.get().returnToPool(ship.gameObject);
            ship = null;
        }
        else
        {
            //ship = ComponentShipPool.get().getScript("TestShip");
            ship = ComponentShipPool.get().getScript("BossShip");
            if (ship != null)
            {
                ship.transform.position = transform.position;
                ship.transform.rotation = transform.rotation;

                ship.transform.parent = transform;
                ship.transform.localScale = transform.localScale;
            }
        }
	}

    void Start() {
        if (ship == null)
            spawnShip();
    }

    /*
    #if UNITY_EDITOR
	    void Update () {
		    if(UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode) {
                if (ship == null)
                    spawnShip();
		    } else {
                if (ship == null)
			        spawnShip();
		    }
	    }
    #endif
    */
    void Update() {
        if ( Input.GetKeyDown(KeyCode.Tab) )
        {
            spawnShip();
        }    
    }
    
}
