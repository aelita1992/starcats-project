﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class projectileScript : JSONMonoBehaviour
{
    private Transform attachBone = null;
    private bool initialized = false;

    private bool logEverything = false;
    private bool logWarnings = true;

    public MovementAI tracker = null;
    protected string trackedTag = "";
    protected Transform target = null;

    public string modelName = null;
    public string attachBoneName = null;
    public string attachBonePath = null;

    public string projectileClass = null;
    public float projectileSpeed = 0;
    public float projectileMass = 0;
    public float projectileDamage = 25f;

	protected static int bulletCounter = 0;
	protected int bulletId = 0;

	/*
    void OnEnable()
	{
		ChangeParentToPrefabCollection();
	}
    */
	public projectileScript (){
		bulletId = bulletCounter++;
	}

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    { 
		name = config["name"].Value + "["+bulletId+"]";
		gameObject.name = config["name"].Value + "["+bulletId+"]";

        modelName = config["modelName"].Value;
        attachBoneName = config["bone"].Value;
        attachBonePath = config["bonePath"].Value;

        projectileClass = config["class"].Value;
        projectileSpeed = config["speed"].AsFloat;
        projectileMass = config["mass"].AsFloat;

        string attachBoneSearch = attachBonePath + "/" + attachBoneName;

        attachBone = transform.Find(attachBoneSearch);

        if (attachBone != null)
        {
            if (logEverything) Debug.Log("fund attachBone: " + attachBoneName + " at bone " + attachBoneSearch, attachBone);
        }
        else
        {
            if (logWarnings) Debug.LogWarning("attachBone missing: " + attachBoneSearch);
        }

        initialized = true;

        return this;
    }
    /*
	static bool keks = false;
	private void ChangeParentToPrefabCollection()
	{

		if(!keks)
		{
			Debug.LogWarning("Czemu to nie dziala 2");
			keks = true;
		}

		if(this.name.StartsWith("Trololo"))
		{
			PrefabCollectionsManager.Get().AddToManager(PrefabCollectionsManager.PrefabType.ENEMY_PROJECTILE, this.gameObject);
		}
		else if(this.name.StartsWith("EnemyBolt"))
		{
			PrefabCollectionsManager.Get ().AddToManager(PrefabCollectionsManager.PrefabType.ENEMY_BOLT, this.gameObject);
		}
	}
    */
    public Transform getAttachBone()
    {
        if (logEverything) Debug.Log("getAttachBone", this);

        if (this.initialized == false)
        {
            Debug.LogError("projectile not initialized @ getAttachBone()");
            return null;
        }
        else
        {
            if (logWarnings && attachBone == null) Debug.LogWarning("projectile initialized, attachBone == null");
        }

        return attachBone;
    }

	void OnCollisionEnter(Collision other)
	{

		if(tag != "Rocket")
			return;

		Debug.LogWarning("boom");

		GameObject boom = (GameObject)Resources.Load("Explosions/RocketExplosion");
		GameObject boomBoom = (GameObject) Instantiate(boom, transform.position, transform.rotation);

        boomBoom.transform.localScale = new Vector3(15f, 15f, 15f);

        TimeManager.Get().timeDependantUpdate -= lookForTargets;
        TimeManager.Get().timeDependantUpdate -= chaseTarget;

        tracker = null;
        target = null;
        trackedTag = "";
	}

    public override void resetPrefab() {
        TimeManager.Get().timeDependantUpdate -= lookForTargets;
        TimeManager.Get().timeDependantUpdate -= chaseTarget;

        tracker = null;
        target = null;
        trackedTag = "";

        this.transform.rotation = Quaternion.identity;
        this.rigidbody.angularVelocity = Vector3.zero;
        this.rigidbody.velocity = Vector3.zero;

        this.rigidbody.Sleep();        
    }

    public void trackTag(string tagName)
    {
        trackedTag = tagName;
        tracker = new MissileAI(new Vector3(5f, 5f, 5f), new Vector3(1.0f, 1.0f, 1.0f), Vector3.one, 0.125f);
        tracker.setSelf(this.transform);

        TimeManager.Get().timeDependantUpdate += lookForTargets;
    }
    public void trackTarget(Transform newTarget)
    {
        trackedTag = newTarget.tag;
        tracker = new MissileAI(new Vector3(5f, 5f, 5f), new Vector3(1.0f, 1.0f, 1.0f), Vector3.one, 0.125f);

        tracker.setSelf(this.transform);
        lockTarget(newTarget);

        TimeManager.Get().timeDependantUpdate += chaseTarget;
    }

    float trackingRadius = 250f;

    private Transform getClosestEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, trackingRadius);
        int i = 0;
        GameObject closestEnemy = null;
        float closestEnemyDistance = float.MaxValue;
        float distance = 0;
        while (i < hitColliders.Length)
        {
            if (hitColliders[i].tag == trackedTag)
            {
                distance = (transform.position - hitColliders[i].transform.position).sqrMagnitude;
                if (distance < closestEnemyDistance)
                {
                    closestEnemy = hitColliders[i].gameObject;
                    closestEnemyDistance = distance;
                }
            }
            i++;
        }

        if (closestEnemy != null)
            return closestEnemy.transform;
        else
            return null;
    }

    protected void lookForTargets() {
        if (tracker != null && trackedTag != "" && target == null)
        {
            target = getClosestEnemy();

            if (target != null)
            {
                lockTarget(target);

                TimeManager.Get().timeDependantUpdate -= lookForTargets;
                TimeManager.Get().timeDependantUpdate += chaseTarget;
            }
        }
    }

    protected void lockTarget(Transform newTarget)
    {
        this.target = newTarget;

        tracker.setTarget(target);
        this.transform.parent = null;
       // target.gameObject.GetComponent<shipScript2>().IsTargeted = false;
        Debug.LogWarning("found target:" + target.gameObject.name);
        Debug.LogWarning("by tag:" + trackedTag);

        this.rigidbody.velocity = Vector3.zero;
        this.rigidbody.angularVelocity = Vector3.zero;  
    }

    void chaseTarget()
    {
        if (tracker != null && target != null && target.gameObject.activeSelf == true)
        {
            tracker.updateAcceleration();
            tracker.updateVelocity();

            this.transform.rotation = Quaternion.Euler(this.transform.position - target.transform.position);

            tracker.applyDeltaToSelf();
        }
        else 
        {
            target = getClosestEnemy();

            if (target != null)
            {
                lockTarget(target);
            }
            else
            {
                TimeManager.Get().timeDependantUpdate += lookForTargets;
                TimeManager.Get().timeDependantUpdate -= chaseTarget;

                Debug.LogWarning("Resuming search for targets");

                this.transform.rotation = Quaternion.identity;
                this.rigidbody.AddForce(new Vector3(0f, 500f, 0f), ForceMode.VelocityChange);
            }
        }
    }

	void OnTriggerEnter (Collider other) 
	{
		if(other.gameObject.tag == "Bonus")
		{
			other.gameObject.SetActive(false);

			GameObject boom = (GameObject)Resources.Load("Explosions/Explosion01b");
			GameObject boomBoom = (GameObject) Instantiate(boom, other.transform.position, other.transform.rotation);		
			boomBoom.transform.localScale = new Vector3(20f, 20f, 20f);
		}
	} 
}
