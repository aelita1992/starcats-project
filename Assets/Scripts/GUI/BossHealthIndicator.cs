﻿using UnityEngine;
using System.Collections;

public class BossHealthIndicator : MonoBehaviour
{
    Rect bar1;
    Rect bar2;
    private ShipScript monitoredShip = null;

    public ShipScript MonitoredShip
    {
        get { return monitoredShip; }
        set { 
            monitoredShip = value;
            if (monitoredShip != null) {
                bar1 = new Rect(Screen.width / 2 - 200, 25, 400, 20);
                bar2 = new Rect(Screen.width / 2 - 200, 25, 400, 20);
            }
        }
    }

    void OnGUI()
    {
        if (monitoredShip != null)
        {
            GUI.backgroundColor = Color.red;
            GUI.HorizontalScrollbar(bar1, 0, monitoredShip.CurrentHP, 0, monitoredShip.StartHP);
            GUI.backgroundColor = Color.yellow;
            GUI.HorizontalScrollbar(bar2, 0, monitoredShip.ShieldCurrentHP, 0, monitoredShip.ShieldStartHP);
        }
    }
}
