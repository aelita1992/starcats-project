﻿using UnityEngine;
using System.Collections;

public class MoveForward : MovementController 
{
	public enum MovementStage {Init = 0, Acceleration = 1, FullSpeed = 2, Exit = -1}
	
	protected override void ExecuteVerticalMovement(int verticalMovementStage)
	{	
		switch(verticalMovementStage)
		{
		case (int)MovementStage.Init:
			Init();
			break;
			
		case (int)MovementStage.Acceleration:
			Acceleration();
			break;
			
		case (int)MovementStage.FullSpeed:
			break;
			
		case (int)MovementStage.Exit:
			Exit ();
			break;
		}
	}

	private void Init()
	{
		enemyBehaviours[shipIterator].MaxSpeed = new Vector3(0,2f,0);
		enemyBehaviours[shipIterator].Speed = new Vector3(0,0f,0);
		
		enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.Acceleration;
		enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.Acceleration;
	}

	private void Acceleration()
	{
		Vector3 speed = enemyBehaviours[shipIterator].Speed;
		Vector3 maxSpeed = enemyBehaviours[shipIterator].MaxSpeed;
		Vector2 xyChangePace = enemyBehaviours[shipIterator].xySpeedChange_Pace;
		
		speed.y = Mathf.SmoothDamp(speed.y, -maxSpeed.y, ref xyChangePace.y, 1f);
		
		if(maxSpeed.y - Mathf.Abs (speed.y) < 0.1)
		{
			enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.FullSpeed;
		}
		
		enemyBehaviours[shipIterator].Speed = speed;
		enemyBehaviours[shipIterator].xySpeedChange_Pace = xyChangePace;
	}

	private void FullSpeed()
	{
		
	}

	private void Exit()
	{
		
	}
}
