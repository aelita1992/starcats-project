﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	public Transform game;

	bool isCreated;
	bool isRecreated;
	float time;
	float time2;

	void Start () {
		time = 1;
		time2 = 5;
		isCreated = false;
		isRecreated = false;
	}
	
	// Update is called once per frame
	void Update () {
		time -= Time.deltaTime;
		time2 -= Time.deltaTime;

		if(time < 0 && !isCreated)
		{
			isCreated = true;
			Instantiate(game);
		}

		if(time2 < 0 && !isRecreated)
		{
			Destroy (game);
			isRecreated = true;
			Instantiate(game);
		}
	}
}
