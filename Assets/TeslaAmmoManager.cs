﻿using UnityEngine;
using System.Collections;

public class TeslaAmmoManager : MonoBehaviour 
{

	public GameObject[] teslaAmmoIcons;
	int currentAmmo;

	private static TeslaAmmoManager pseudoSingleton;
	public static TeslaAmmoManager Get()
	{
		return pseudoSingleton;
	}

	void Awake()
	{
		pseudoSingleton = this;
	}

	void Start ()
	{
		currentAmmo = 1;
		teslaAmmoIcons[0].SetActive(true);
	}

	public bool IsEnoughAmmo()
	{
		if(currentAmmo > 0)
			return true;

		return false;
	}

	public void ReduceAmmo()
	{
		if(currentAmmo <= 0)
			return;

		currentAmmo--;
		teslaAmmoIcons[currentAmmo].SetActive (false);
	}

	public void IncreaseAmmo()
	{
		if(currentAmmo > 2)
			return;

		teslaAmmoIcons[currentAmmo].SetActive (true);
		currentAmmo++;
	}

}
