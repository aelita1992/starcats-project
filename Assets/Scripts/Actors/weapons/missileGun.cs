﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

// chwilowo jak projectileGun!

public class missileGun : gunScript {
	
	protected ProjectilePool ProjectileManagerInstance = null;
	protected GameObject projectilePrefab = null;
	protected string projectilePrefabName = null;
	
	projectileScript loadedProjectile = null;
	
	public override JSONMonoBehaviour applyConfig(JSONClass config)
	{
		base.applyConfig (config);
		
		// Manager pocisków
		
		ProjectileManagerInstance = ProjectilePool.get();
		
		if (ProjectileManagerInstance == null)
		{
			Debug.LogError("ProjectileManager not ready during weapon init!");
			return null;
		}
		
		// prefab pocisku
		
		projectilePrefabName = config["projectile"].Value;
		projectilePrefab = ProjectileManagerInstance.getPrefab(projectilePrefabName, muzzleBone.transform.position, muzzleBone.transform.rotation);
		
		if (projectilePrefab == null)
		{
			Debug.LogWarning("Can't find matching prefab '" + projectilePrefabName + "' for weapon " + name);
		}
		else
		{
			projectilePrefab.transform.parent = muzzleBone;
			projectilePrefab.SetActive(false);
		}
		
		initialized = true;
		
		return this;
	}
	
	public projectileScript prepareToFire( ){
		
		if( loadedProjectile == null )
			loadedProjectile = ProjectileManagerInstance.getScript(projectilePrefabName);
		
		return loadedProjectile;
	}
	
	public override void fire()
	{
		projectileScript projectilePrefabClone = null;
		
		if (loadedProjectile != null) {
			projectilePrefabClone = loadedProjectile;
		} else {
			projectilePrefabClone = prepareToFire();
		}
		
		//Debug.Log (this.transform.parent.name + " fired: " + projectilePrefabClone.name, projectilePrefabClone);
		
		if (projectilePrefabClone != null)
		{
			projectilePrefabClone.transform.position = projectilePrefab.transform.position;
			projectilePrefabClone.transform.rotation = projectilePrefab.transform.rotation;
			projectilePrefabClone.projectileDamage = damage;
			
			Rigidbody RigidbodyHandle = projectilePrefabClone.rigidbody;
			RigidbodyHandle.WakeUp();
			
			// nadajemy skale jak w obiekcie nadrzednym
			projectilePrefabClone.transform.localScale =  attachBone != null ? attachBone.lossyScale : Vector3.one;// this.transform.lossyScale;
			float scaleConstant = projectilePrefabClone.transform.localScale.magnitude;
			
			// skala pocisku         
			if (projectileScale != null && projectileScale.Count == 3)
			{
				projectilePrefabClone.transform.localScale += (new Vector3(projectileScale[0].AsFloat, projectileScale[1].AsFloat, projectileScale[2].AsFloat) );
			}
			
			// masa i predkosc wg definicji pocisku
			RigidbodyHandle.mass = projectilePrefabClone.projectileMass * scaleConstant;
			RigidbodyHandle.AddForce(projectilePrefabClone.projectileSpeed * projectilePrefab.transform.up, ForceMode.VelocityChange);
			
			// rozrzut
			if (projectileSpread != null && projectileSpread.Count == 3)
			{
				RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[0].AsFloat, projectileSpread[0].AsFloat) * projectilePrefabClone.projectileSpeed * projectilePrefab.transform.right, ForceMode.VelocityChange);
				RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[1].AsFloat, projectileSpread[1].AsFloat) * projectilePrefabClone.projectileSpeed * projectilePrefab.transform.up, ForceMode.VelocityChange);
				RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[2].AsFloat, projectileSpread[2].AsFloat) * projectilePrefabClone.projectileSpeed * projectilePrefab.transform.forward, ForceMode.VelocityChange);
			}
			
			
			if (projectilePrefabClone.tag == "Rocket")
			{
				if (parentShip.tag == "Player")
				{
					projectilePrefabClone.trackTag("EnemyShip");
				}
				else {
					projectilePrefabClone.trackTag("Player");
				}
			}
			
			loadedProjectile = null;
			
			// liczymy czas do nast strzalu:
			// jezeli strzelamy seria, liczymy wg szybkostrzelnosci w serii
			// jezeli skonczylismy serie (albo burstRate == 1) to liczymy wg globalnej szybkostrzelnosci
			if (burstSize == 1)
			{
				nextFire = Time.time + 1f / rateOfFire;
			} else {
				
				if (Time.time > nextBurst){
					burstCount = 0;
					nextBurst = Time.time + 1f / rateOfFire;
				}
				
				burstCount++;
				if (burstCount >= burstSize ){
					burstCount = 0;
					nextFire = Time.time + 1f / rateOfFire;
				} else {
					nextFire = Time.time + 1f / burstRate;
				}
			}
			
		}
	}
}
