﻿using UnityEngine;
using System.Collections;

public class ParticleWeapon : MonoBehaviour
{
    private DamageInfo placeholderParticleDamage = new DamageInfo(2.5f, null, null, false);
    private ParticleSystem.CollisionEvent[] collisionEvents = new ParticleSystem.CollisionEvent[16];
    private ParticleSystem impactEffect = null;
    private ParticleSystem penetrationEffect = null;

    void Awake()
    {
        GameObject impactPrefab = Resources.Load("Prefabs/bulletImpact") as GameObject;
        GameObject penetrationPrefab = Resources.Load("Prefabs/bulletPenetration") as GameObject;
        impactEffect = impactPrefab.GetComponent<ParticleSystem>();
        penetrationEffect = penetrationPrefab.GetComponent<ParticleSystem>();
    }

    void OnParticleCollision(GameObject other)
    {
        int safeLength = particleSystem.safeCollisionEventSize;
        if (collisionEvents.Length < safeLength)
            collisionEvents = new ParticleSystem.CollisionEvent[safeLength];

        int numCollisionEvents = particleSystem.GetCollisionEvents(other, collisionEvents);
        int i = 0;
        while (i < numCollisionEvents)
        {
            //Debug.Log("Impacted: " + collisionEvents[i].collider.gameObject.name);
            Instantiate(impactEffect, collisionEvents[i].intersection, Quaternion.LookRotation(collisionEvents[i].normal));

            if (Random.Range(0f, 1f) >= 0.95f)
                Instantiate(penetrationEffect, collisionEvents[i].intersection, Quaternion.LookRotation(collisionEvents[i].normal * -1f));

            collisionEvents[i].collider.gameObject.SendMessageUpwards("handleImpact", placeholderParticleDamage);

            /*
            if (other.rigidbody)
            {
                Vector3 pos = collisionEvents[i].intersection;
                Vector3 force = collisionEvents[i].velocity * 10;
                other.rigidbody.AddForce(force);
            }
            */
            i++;
        }
    }

}
