﻿using UnityEngine;
using System.Collections;

public class BonusScript : MonoBehaviour
{
	public enum BonusType {REPAIR, GODMODE, DOUBLE_POINTS, WEAPON}

	public BonusType bonusType;
	Vector3 movementSpeed = new Vector3(0, -4, 0);

	public BonusType GetBonusType()
	{
		return bonusType;
	}

	void Update()
	{
		Vector3 position = transform.position;
		position += movementSpeed;
		transform.position = position;
	}
}
