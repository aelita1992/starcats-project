﻿using UnityEngine;
using System.Collections;

public class enemyTestScript : MonoBehaviour {

    public string shipAssetName = "EnemyShip";
    
    public Transform from;
    public Transform to;

    protected MovementAI mai = null;
    private ShipScript ship = null;

    // Use this for initialization
    void Awake()
    {
        GameObject shipObject = ShipPool.get().getPrefab(shipAssetName, transform.position, transform.rotation);

        if (shipObject != null)
        {
            ship = shipObject.GetComponent<ShipScript>();

            //shipObject.transform.localScale = from.localScale;
            //shipObject.transform.rotation = from.rotation;
            shipObject.transform.position = from.position;

            mai = new AcceleratedMovementAI2(new Vector3(5f, 5f, 5f), new Vector3(1.0f, 1.0f, 1.0f), Vector3.one, 0.125f);
            mai.setSelf(ship.transform);
            mai.setTarget(to);
        }
    }

    void Update(){
        mai.updateAcceleration();
        mai.updateVelocity();

        Debug.DrawLine(ship.transform.position, to.position, Color.green, Time.deltaTime * 2);
        Debug.DrawLine(ship.transform.position, ship.transform.position + (mai.getVelocity() * 25f), Color.red, Time.deltaTime * 2);
        Debug.DrawLine(ship.transform.position, ship.transform.position+(mai.getAcceleration() * 25f), Color.blue, Time.deltaTime * 2);

        mai.applyDeltaToSelf();
    }

}
