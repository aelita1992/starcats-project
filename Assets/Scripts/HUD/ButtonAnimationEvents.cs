﻿using UnityEngine;
using System.Collections;

public class ButtonAnimationEvents : MonoBehaviour 
{
	Animator currentButtonAnimator;

	void Awake()
	{
		currentButtonAnimator = gameObject.GetComponent<Animator>();
	}

	void OnFadeIn_Finished()
	{
		currentButtonAnimator.SetBool("IsFadingIn", false);
	}

	void OnFadeOut_Finished()
	{
		currentButtonAnimator.SetBool("IsFadingOut", false);
		this.gameObject.SetActive(false);
	}
}
