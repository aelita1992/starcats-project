﻿using UnityEngine;
using System.Collections;

public class testGroundBoundrary : MonoBehaviour {
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Projectile" || other.gameObject.tag == "EnemyProjectile")
            ProjectilePool.get().returnToPool(other.gameObject);
        else if (other.gameObject.tag == "Player")
            Debug.LogWarning("Player Exit", other.gameObject);
        else
        {
            //Debug.LogWarning("Destroyed: " + other.gameObject.name, other.gameObject);
            //Destroy(other.gameObject);
        }
    }
}
