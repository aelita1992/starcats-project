﻿using UnityEngine;
using System.Collections;

public class TargetDummy : MonoBehaviour
{
    private Transform startMarker;
    private Transform endMarker;

    public Transform marker1;
    public Transform marker2;
    public Transform marker3;
    public Transform marker4;
    public Transform marker5;
    public Transform marker6;
    public Transform marker7;
    public Transform marker8;

    private Transform[] markers;
    private int stage = 0;
    public float speed = 1.0F;

    private float startTime;
    private float journeyLength;

    void enterStage() {
        startMarker = markers[stage];
        endMarker = markers[stage+1 == markers.Length ? 0 : stage+1];

        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Projectile")
        {
            ProjectilePool.get().returnToPool(col.gameObject);
        }
    }

    void advanceStage() {
        stage++;
        if (stage == markers.Length) stage = 0;

        enterStage();
    }

    float easeMovement(float t)
    {
        t = t * t * t * (t * (6f * t - 15f) + 10f);
        return t;
    }

    void Start()
    {
        markers = new Transform[] { marker1, marker2, marker3, marker4, marker5, marker6, marker7, marker8};
        enterStage();
    }
    void Update()
    {
        float distCovered = (Time.time - startTime) * speed;
        float fracJourney = distCovered / journeyLength;
        transform.position = Vector3.Lerp(startMarker.position, endMarker.position, easeMovement(fracJourney));
        if (fracJourney > 1f) {
            advanceStage();
        }
    }

    private void swapMarkers()
    {
        Transform tmp = startMarker;
        startMarker = endMarker;
        endMarker = tmp;
        startTime = Time.time;
        journeyLength = Vector3.Distance(startMarker.position, endMarker.position);
    }
}
