﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriforceFormation : Formation 
{
	int numberOfShipsLeft;
	int iterator;
	
	public override void SetFormation(List<GameObject> formationShips, FormationsController.Direction direction, Vector3 centerPosition, Vector2 xySpread)
	{
		numberOfShipsLeft = formationShips.Count;
		iterator = 0;
		this.xySpread = xySpread;
		
		switch(direction)
		{
		case FormationsController.Direction.LEFT:
		case FormationsController.Direction.RIGHT:
			
			Debug.LogError("Triforce formation doesn't support LEFT and RIGHT directions");
			break;

		case FormationsController.Direction.NO_DIRECTION:

			direction = FormationsController.Direction.UP;
			SetCenterAndTopWing(formationShips, centerPosition, direction);
			SetBottomWing(formationShips, centerPosition, direction);
			break;
			
		case FormationsController.Direction.DOWN:
		case FormationsController.Direction.UP:
			
			SetCenterAndTopWing(formationShips, centerPosition, direction);
			SetBottomWing(formationShips, centerPosition, direction);
			break;
		}
		
	}
	
	private void SetCenterAndTopWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		int topWingSize = (numberOfShipsLeft-1)/3;
		int translatedDir = TranslateDirection(direction);
		
		formationShips[iterator++].transform.position = centerPosition;

		for(int i = 0; i < topWingSize; i++)
		{
			centerPosition.y += xySpread.y * translatedDir;
			formationShips[iterator++].transform.position = centerPosition;
		}
		
		numberOfShipsLeft -= (topWingSize + 1);
		
	}
	
	private void SetBottomWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		Vector3 currentCenter = centerPosition;
		for(int i = 0; i < numberOfShipsLeft/2; i++)
		{
			currentCenter.x -= xySpread.x;
			currentCenter.y -= xySpread.y * TranslateDirection(direction);
			formationShips[iterator++].transform.position = currentCenter;
		}

		numberOfShipsLeft /= 2;
		currentCenter = centerPosition;

		for(int i = 0; i < numberOfShipsLeft; i++)
		{
			currentCenter.x += xySpread.x;
			currentCenter.y -= xySpread.y * TranslateDirection(direction);
			formationShips[iterator++].transform.position = currentCenter;
		}
	}

	private int TranslateDirection (FormationsController.Direction direction)
	{
		switch(direction)
		{
		case FormationsController.Direction.UP:
			return 1;
		case FormationsController.Direction.DOWN:
			return -1;
		}
		
		return 0;
		
	}

}
