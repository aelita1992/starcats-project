﻿using UnityEngine;
using System.Collections;

public class GameOverPanel : MonoBehaviour {

	public GameObject gameOverText;
	public GameObject wonText;
	public GameObject lostText;
	public GameObject finalScoreText;
	public GameObject finalScoreValue;

	public int score;

	private static GameOverPanel pseudoSingleton;

	void Awake()
	{
		pseudoSingleton = this;
	}

	public static GameOverPanel Get()
	{
		return pseudoSingleton;
	}

	public void Expand()
	{
		gameObject.SetActive (true);
	}
	
	public void Collapse()
	{
		gameObject.SetActive (false);
	}

	public void ActivateWonScreen()
	{
		gameOverText.SetActive(true);
		wonText.SetActive(true);
		finalScoreText.SetActive(true);
		finalScoreValue.SetActive(true);
		finalScoreValue.GetComponent<TextMesh>().text = score.ToString();
	}

	public void ActivateLostScreen()
	{
		gameOverText.SetActive(true);
		lostText.SetActive(true);
		finalScoreText.SetActive(true);
		finalScoreValue.SetActive(true);
		finalScoreValue.GetComponent<TextMesh>().text = score.ToString();
	}
	
}
