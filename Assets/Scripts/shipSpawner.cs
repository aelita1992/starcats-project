﻿using UnityEngine;
using System.Collections;

public class shipSpawner : MonoBehaviour {

    public string shipAssetName = "LOLShip";
    public string shipAssetType = "ShipScript2";

    public ShipScript ship = null;
    public GameObject shipDestination = null;

    protected static GameObject spawnerContainer = null;

    public bool fireShipGuns = false;
    public bool instantInit = true;
    private bool initialized = false;
    private MovementAI shipMovementAI = null;

	// Use this for initialization
	void Start () {
        if (instantInit) init();
	}

    public void init() {
        if (initialized) return;

        if (spawnerContainer == null)
        {
            spawnerContainer = new GameObject();
            spawnerContainer.name = "Ship spawners";
            spawnerContainer.transform.position = Vector3.zero;
            spawnerContainer.transform.localScale = Vector3.one;
            spawnerContainer.transform.rotation = Quaternion.identity;
        }
        this.transform.parent = spawnerContainer.transform;

        GameObject shipObject = null;
        if ( shipAssetType == "ComponentShip")
            shipObject = ComponentShipPool.get().getPrefab(shipAssetName, transform.position, Quaternion.Euler(270f, 0f, 0f));
        else
            shipObject = ShipPool.get().getPrefab(shipAssetName, transform.position, Quaternion.Euler(90f, 180f, 0f));

        if (shipObject != null)
        {
            ship = shipObject.GetComponent<ShipScript>();

            initialRotation = ship.transform.rotation;

            shipDestination = new GameObject();
            shipDestination.AddComponent<EnemyBehaviour>();

            shipDestination.transform.parent = this.transform;
            shipDestination.transform.position = transform.position;
            shipDestination.transform.rotation = transform.rotation;

            shipObject.transform.parent = this.transform;
            shipObject.transform.localScale = Vector3.one;

            shipMovementAI = new ConstantMovementAI(new Vector3(250.0f, 250.0f, 250.0f), new Vector3(0.25f, 0.25f, 0.25f), Vector3.one, 0.25f);
            ////new AcceleratedMovementAI2 InstantMovementAI
			shipMovementAI.setSelf(ship.transform);
            shipMovementAI.setTarget(shipDestination.transform);

            TimeManager.Get().timeDependantUpdate += managerUpdate;
        }
        else {
            Debug.LogWarning("ShipSpawner::Init() - Failed to instantiate ship \"" + shipAssetName + "\"!");
        }

        initialized = true;
		this.GetComponent<MeshRenderer>().enabled = false;
    }

	// Update is called once per frame
    //void Update() { }

    void managerUpdate()
    {
        if (!initialized) init();

        if (fireShipGuns && ship != null)
        {
			float rand = Random.Range (0, 1000);

            if (ship.transform.position.y <= 300f && ship.transform.position.y > -250f && rand < 8)
            	ship.fireAllWeapons();

            if (ship.transform.position.y < -350f) {
                if (ship.IsAlive) ship.kill();
                GameObject.Destroy(this);
            }
        }
    }

    Quaternion initialRotation = Quaternion.identity;
    void FixedUpdate() {
        if (initialized) {
            shipMovementAI.updateAcceleration();
            shipMovementAI.updateVelocity();

            Vector3 a = shipMovementAI.getVelocity();
            ship.transform.localRotation = Quaternion.Euler(
                initialRotation.eulerAngles.x + 0f,
                initialRotation.eulerAngles.y + 0f,
                initialRotation.eulerAngles.z + (a.x * 10f * Time.deltaTime));

            /*
            Debug.DrawLine(shipMovementAI.Self.transform.position, shipMovementAI.Target.position, Color.green, Time.deltaTime * 2);
            Debug.DrawLine(shipMovementAI.Self.transform.position, shipMovementAI.Self.transform.position + (shipMovementAI.getVelocity() * 25f), Color.red, Time.deltaTime * 2);
            Debug.DrawLine(shipMovementAI.Self.transform.position, shipMovementAI.Self.transform.position + (shipMovementAI.getAcceleration() * 25f), Color.blue, Time.deltaTime * 2);
            */

            shipMovementAI.applyDeltaToSelf();        
        }
    }

    internal void shutDown()
    {
        gameObject.SetActive(false);

        shipMovementAI.Deattach();
        shipMovementAI = null;

        TimeManager.Get().timeDependantUpdate -= managerUpdate;
    }
}
