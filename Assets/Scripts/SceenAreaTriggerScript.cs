﻿using UnityEngine;
using System.Collections;

public class SceenAreaTriggerScript : MonoBehaviour {

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Projectile" || other.gameObject.tag == "EnemyProjectile")
            ProjectilePool.get().returnToPool(other.gameObject);
    }
}
