﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Chapter {
    private List<Level> levels = null;
    private Level currentLevel = null;
    private int levelIndex = 0;

    public event Scenario.LevelStarted nextLevel;
    public event Scenario.StageStarted nextStage;

    float startTime = 0f;
    float elapsedTime = 0f;

    private string name = null;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    bool isDone = false;

    public bool IsDone
    {
        get { return isDone; }
        set { isDone = value; }
    }

    void advanceStage(Stage s)
    {
        nextStage(s);
    }

    public Chapter(JSONClass chapterData)
    {
        name = chapterData["name"].Value;

        JSONArray levelList = chapterData["levels"].AsArray;
        if (levelList != null && levelList.Count > 0)
        {

            levels = new List<Level>();

            foreach (JSONNode l in levelList)
            {
                if (l == null)
                {
                    Debug.LogError("error loading level name");
                    continue;
                }

                Level lv = Level.fromFile(l.Value);
                lv.nextStageStarted += advanceStage;
                levels.Add(lv);
            }
        }
        else
        {
            Debug.LogError("error loading levelList");
        }
    }

    public void start(float time)
    {
        startTime = time;
        elapsedTime = 0f;
        levelIndex = 0;
        
        startLevel(levelIndex);
    }

    private void startLevel(int index){
        currentLevel = levels[index];
        if (currentLevel == null)
        {
            Debug.LogError("Level not found: " + index);
        }

        Debug.LogWarning("Starting level " + name + "::" + levelIndex + " (" + currentLevel .Name+ ")");
        if (nextLevel != null)
        {
            nextLevel(currentLevel); 
        }
        currentLevel.start(elapsedTime);
    }

    public void restartLevel() {
        if (currentLevel != null) {
            currentLevel.forceEnd();
            startLevel(levelIndex);
        }
    }

    public void update(float time)
    {
        elapsedTime = time - startTime;

        if (currentLevel == null || currentLevel.IsDone)
        {
            if (levelIndex >= levels.Count) {
                isDone = true;
                return;
            }
            startLevel(levelIndex);
            levelIndex++;
        }

        currentLevel.update(elapsedTime);
    }

}
