﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineFormation : Formation
{
	int numberOfShipsLeft;
	int iterator;
	
	public override void SetFormation(List<GameObject> formationShips, FormationsController.Direction direction, Vector3 centerPosition, Vector2 xySpread)
	{
		numberOfShipsLeft = formationShips.Count;
		if(numberOfShipsLeft <= 0)
			return;

		iterator = 0;
		this.xySpread = xySpread;

		switch(direction)
		{
		case FormationsController.Direction.LEFT:
		case FormationsController.Direction.RIGHT:

			SetCenterAndTopWing(formationShips, centerPosition);
			SetBottomWing(formationShips, centerPosition);
			break;

		case FormationsController.Direction.DOWN:
		case FormationsController.Direction.UP:
		case FormationsController.Direction.NO_DIRECTION:

			SetCenterAndLeftWing(formationShips, centerPosition);
			SetRightWing(formationShips, centerPosition);
			break;
		}

	}
	
	private void SetCenterAndLeftWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		int leftWingSize = numberOfShipsLeft/2;

		formationShips[iterator++].transform.position = centerPosition;

		for(int i = 0; i < leftWingSize; i++)
		{
			centerPosition.x -= xySpread.x;
			formationShips[iterator++].transform.position = centerPosition;
		}
		
		numberOfShipsLeft -= (leftWingSize + 1);
		
	}
	
	private void SetRightWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		for(int i = 0; i < numberOfShipsLeft; i++)
		{
			centerPosition.x += xySpread.x;
			formationShips[iterator++].transform.position = centerPosition;
		}
	}

	private void SetCenterAndTopWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		int leftWingSize = numberOfShipsLeft/2;

		formationShips[iterator++].transform.position = centerPosition;

		for(int i = 0; i < leftWingSize; i++)
		{
			centerPosition.y += xySpread.y;
			formationShips[iterator++].transform.position = centerPosition;
		}
		
		numberOfShipsLeft -= (leftWingSize + 1);
	}

	private void SetBottomWing(List<GameObject> formationShips, Vector3 centerPosition)
	{
		for(int i = 0; i < numberOfShipsLeft; i++)
		{
			centerPosition.y -= xySpread.y;
			formationShips[iterator++].transform.position = centerPosition;
		}
	}
}
