﻿using UnityEngine;
using System.Collections;

public class ShipTestInitScript : MonoBehaviour {

    ShipScript selectedShip = null;
    GameObject selectedShipGameObject = null;

    void Start () {
        ShipPool demo = ShipPool.get();

        GameObject selectedShipGameObject =
        demo.getPrefab("LOLShip", new Vector3(50, 15, 50), Quaternion.Euler(0, 0, 0));
        demo.getPrefab("ROTFLShip", new Vector3(50, 15, -50), Quaternion.Euler(0, 0, 0));

        demo.getPrefab("LOLShipAlt", new Vector3(-50, 15, 50), Quaternion.Euler(0, 45, 0));
        demo.getPrefab("ROTFLShipAlt", new Vector3(-50, 15, -50), Quaternion.Euler(0, -45, 0));

        demo.getPrefab("PlayerShip", new Vector3(0, 10, -100), Quaternion.Euler(0, 0, 0));

        selectedShip = selectedShipGameObject.GetComponent<ShipScript>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && selectedShip != null)
        {
            if (Input.GetKey(KeyCode.LeftControl))
                selectedShip.fireWeaponsGroup(1);
            else if (Input.GetKey(KeyCode.LeftShift))
                selectedShip.fireWeaponsGroup(2);
            else
                selectedShip.fireAllWeapons();
        }

        if (Input.GetKey(KeyCode.Mouse1))
        {

            Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);                        
            RaycastHit hit;

            if (Physics.Raycast(path, out hit))
            {
                if (hit.transform.gameObject.tag == "Player" || hit.transform.gameObject.tag == "EnemyShip")
                {
                    if (selectedShipGameObject == hit.transform.gameObject) return;

                    ShipScript shipInfo = hit.transform.gameObject.GetComponent<ShipScript>();
                    if (shipInfo != null) { 
                        selectedShip = shipInfo;
                        selectedShipGameObject = hit.transform.gameObject;
                        Debug.Log("Selected", shipInfo);
                    }                    
                }
                else if (selectedShip != null)
                {
                    selectedShip.transform.LookAt(hit.point);
                }
            }
        }
    }

    /*
    Vector3 pos1, pos2, pos3;
    pos1 = Input.mousePosition;
    pos2 = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    pos3 = Camera.main.ViewportToWorldPoint(Input.mousePosition);

    Debug.LogWarning("pos1: " + pos1.x + " / " + pos1.y + " / " + pos1.z);
    Debug.LogWarning("pos2: " + pos2.x + " / " + pos2.y + " / " + pos2.z);
    Debug.LogWarning("pos3: " + pos3.x + " / " + pos3.y + " / " + pos3.z);

    transform.LookAt(pos1);*/
    //transform.LookAt(Camera.main.ViewportToWorldPoint(Input.mousePosition));
    /*
    var objectPos = Camera.main.WorldToScreenPoint(transform.position);
    var dir = Input.mousePosition - objectPos;
    transform.rotation = Quaternion.Euler(new Vector3(0, 0, Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg));
     */
    /*
    Ray path = Camera.main.ScreenPointToRay(Input.mousePosition);
            
    Vector3 pos1, pos2, pos3;
    pos1 = Input.mousePosition;
    pos2 = path.origin;
    pos3 = transform.position - pos2;

    Debug.LogWarning("pos1: " + pos1.x + " / " + pos1.y + " / " + pos1.z);
    Debug.LogWarning("pos2: " + pos2.x + " / " + pos2.y + " / " + pos2.z);
    Debug.LogWarning("pos3: " + pos3.x + " / " + pos3.y + " / " + pos3.z);

    handle.transform.LookAt(pos3);
     */
}
