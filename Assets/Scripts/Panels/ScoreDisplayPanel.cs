﻿using UnityEngine;
using System.Collections;

public class ScoreDisplayPanel : MonoBehaviour 
{
	public void Expand()
	{
		gameObject.SetActive(true);
	}
	
	public void Collapse()
	{
		gameObject.SetActive(false);
	}
}
