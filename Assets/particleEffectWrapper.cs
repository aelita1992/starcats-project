﻿using UnityEngine;
using System.Collections;

public class particleEffectWrapper : MonoBehaviour {

    private ParticleSystem ps;
    public void Awake()
    {
        if (!initd) init();
    }

    private bool initd = false;
    private void init() {
        initd = true;

        Transform firstChild = transform.GetChild(0);
        if (firstChild != null)
        {
            ps = firstChild.gameObject.GetComponent<ParticleSystem>();
        }
    }

    public void play() {
        if (!initd) init();
        ps.Play();
    }

    public void setScale(float scale) {
        if (!initd) init();
        transform.localScale = new Vector3(scale,scale,scale);
        ps.startSize *= scale;
        ps.startSpeed *= scale;
    }

    public void Update()
    {
        if (ps)
        {
            if (!ps.IsAlive())
            {
                Destroy(gameObject);
            }
        }
    }
}
