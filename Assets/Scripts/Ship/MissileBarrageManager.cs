﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MissileBarrageManager : MonoBehaviour 
{
	bool isControlPressed = false;
	bool isMarkingActive = false;
	
	float cooldownLeft;
	float defaultCooldown;

    static int reloadRate = 10;

    Dictionary<int, ShipScript> pooledShips;
	List <int> activeShipIDs;
	List <int> markedShips;

    public PlayerConnectorScript playerConnector = null;

	public EnergyBar energyBar;
	private static MissileBarrageManager pseudoSingleton;
	public static MissileBarrageManager Get()
	{
		return pseudoSingleton;
	}
	
	void Awake()
	{
		pseudoSingleton = this;
		TimeManager.Get ().timeDependantUpdate_1second += ReloadBarrage;


        pooledShips = new Dictionary<int, ShipScript>();
		activeShipIDs = new List<int>();
		markedShips = new List<int>();
		
		defaultCooldown = 0.05f;
		cooldownLeft = 0.05f;
	}
	
	void Start(){}
	
	public void AddPooledShipReference(int shipID, GameObject shipObject)
	{
		if(!pooledShips.ContainsKey(shipID))
            pooledShips.Add(shipID, shipObject.GetComponent<ShipScript>());
	}
	
	public void AddActiveShip(int shipID)
	{
		if(pooledShips.ContainsKey(shipID))
			activeShipIDs.Add (shipID);
	}
	
	public void RemoveActiveShip(int shipID)
	{
		for(int i = 0; i < activeShipIDs.Count; i++)
		{
			if(activeShipIDs[i] == shipID)
			{
				activeShipIDs.RemoveAt (i);
				pooledShips[shipID].IsTargeted = false;
				return;
			}
		}
		
		for(int i = 0; i < markedShips.Count; i++)
		{
			if(markedShips[i] == shipID)
			{
				markedShips.RemoveAt (i);
				pooledShips[shipID].IsTargeted = false;
				return;
			}
		}
		
		Debug.LogError("Ship with id " + shipID + " was not found");
	}
	
	void CustomUpdate()
	{
		if (Input.GetKeyDown(KeyCode.LeftControl) )
		{
			TimeManager.Get ().EnableBulletTime();
			isControlPressed = true;
			isMarkingActive = true;
			AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.MISSILE_ALERT);
		}
		
		if(Input.GetKeyUp(KeyCode.LeftControl))
		{
			isControlPressed = false;
			AudioManager.Get ().StopEffectClip();
		}
		
		if(isControlPressed && markedShips.Count < 8)
		{
			MarkEnemy();
		}

		if(isControlPressed && (activeShipIDs.Count <= 0 || markedShips.Count >= 8))
		{
			isControlPressed = false;
			isMarkingActive = false;
			ExecuteMissileBarrage();
		}

		if(!isControlPressed && isMarkingActive)
		{
			isMarkingActive = false;
			ExecuteMissileBarrage();
		}
		
	}
	
	private void MarkEnemy()
	{
		cooldownLeft -= Time.deltaTime;
		
		if(cooldownLeft <= 0)
		{
			cooldownLeft = defaultCooldown;
			
			if(activeShipIDs.Count == 0)
				return;
			
			int position = Random.Range(0, activeShipIDs.Count);

			if(pooledShips[activeShipIDs[position]].gameObject.tag == "Player")
			{
				activeShipIDs.RemoveAt(position);
				return;
			}

			markedShips.Add (activeShipIDs[position]);
			pooledShips[activeShipIDs[position]].IsTargeted = true;
			activeShipIDs.RemoveAt(position);
		}
		
	}

    private Transform playerShip = null;
	private void ExecuteMissileBarrage()
	{
		AudioManager.Get ().StopEffectClip();
		AudioManager.Get ().PlayEffectClip(AudioManager.EffectClipName.MISSILE_LAUNCH);

        if (playerConnector != null) playerShip = playerConnector.SelectedShipGameObject.transform;
		for(int i = 0; i < markedShips.Count; i++)
		{
			PrepareRocket(pooledShips[markedShips[i]].transform);
			pooledShips[markedShips[i]].IsTargeted = false;
			activeShipIDs.Add (markedShips[i]);
		}
		
		markedShips.Clear();

		energyBar.SetValueCurrent(0);
		TimeManager.Get ().DisableBulletTime();
		TimeManager.Get ().timeDependantUpdate -= CustomUpdate;
		TimeManager.Get().timeDependantUpdate_1second += ReloadBarrage;
	}

	private void PrepareRocket(Transform target)
	{
		projectileScript projectilePrefabClone = null;
		projectilePrefabClone = ProjectilePool.get().getScript("RPG");
		if (projectilePrefabClone != null)
		{
            if (playerShip != null)
                projectilePrefabClone.transform.position = playerShip.position;
            else
                projectilePrefabClone.transform.position = this.transform.position;

			projectilePrefabClone.transform.rotation = this.transform.rotation;
			projectilePrefabClone.projectileDamage = 500;

			
			
			projectilePrefabClone.rigidbody.WakeUp();
            //Vector3 direction = projectilePrefabClone.projectileSpeed * 10f * new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f) * 10f, Random.Range(1f, 10f));
            Vector3 direction = new Vector3(Random.Range(-10f, 10f), Random.Range(-10f, 10f) * 10f, Random.Range(-100f, -25f));
            projectilePrefabClone.rigidbody.AddForce(direction, ForceMode.VelocityChange);
            

            projectilePrefabClone.trackTarget(target);
            projectilePrefabClone.tracker.setVelocity(direction);
		}
	}

	private void ReloadBarrage()
	{
		energyBar.SetValueCurrent(energyBar.valueCurrent + reloadRate);

		if(energyBar.valueCurrent > 100)
		{
			TimeManager.Get ().timeDependantUpdate_1second -= ReloadBarrage;
			TimeManager.Get ().timeDependantUpdate += CustomUpdate;
		}
	}

}
