using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovementController
{
	protected List<GameObject> enemyObjects;
	protected List<EnemyBehaviour> enemyBehaviours;

	protected int shipIterator;

	protected bool isInitiated = false;

	protected void InitController()
	{
		enemyObjects = new List<GameObject>();
		enemyBehaviours = new List<EnemyBehaviour>();

		isInitiated = true;
	}

	public virtual void AddEnemy(GameObject enemyShip)
	{
		if(!isInitiated)
			InitController ();

		EnemyBehaviour behaviourToAssign = enemyShip.GetComponent<EnemyBehaviour>();

		behaviourToAssign.horizontalMovementStage = 0;
		behaviourToAssign.verticalMovementStage = 0;

		enemyBehaviours.Add (behaviourToAssign);
		enemyObjects.Add (behaviourToAssign.gameObject);
	}

	protected void RemoveEnemyOnPosition(int position)
	{
		enemyBehaviours.RemoveAt (position);
		enemyObjects.RemoveAt (position);
	}

	protected virtual void ExecuteHorizontalMovement(int horizontalMovementStage){}
	protected virtual void ExecuteVerticalMovement(int verticalMovementStage){}

	public void PerformMovement()
	{
		if(enemyBehaviours == null)
			return;
		
		for(shipIterator = enemyBehaviours.Count - 1; shipIterator >= 0; shipIterator--)
		{
            if (enemyObjects[shipIterator] == null) {
                RemoveEnemyOnPosition(shipIterator);
                continue;
            }

			if(enemyObjects[shipIterator].activeSelf == false)
			{
				enemyBehaviours.RemoveAt(shipIterator);
				enemyObjects.RemoveAt(shipIterator);
				continue;
			}

			ExecuteHorizontalMovement(enemyBehaviours[shipIterator].horizontalMovementStage);
			ExecuteVerticalMovement(enemyBehaviours[shipIterator].verticalMovementStage);

			enemyObjects[shipIterator].transform.position += enemyBehaviours[shipIterator].Speed;
		}
		
	}

}

