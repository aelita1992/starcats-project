﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ShipComponent : JSONMonoBehaviour
{
    #region fields
    ComponentShipScript parentShip = null;

    ComponentMountPoint parentMountPoint = null;
    ShipComponent parentComponent = null;
    List<ShipComponent> subComponents = null;

    // where the component is attached
    Transform baseBone = null;

    Dictionary<string, MountPoint> mountPoints = null;

    WeaponManager weapons = null;
    TurretManager turrets = null;
    ShieldManager shields = null;

    ParticleSystem deathEffect = null;

    protected GameObject rayCastColliders = null;

    bool isDeattached = false;
    bool isDestroyed = false;

    float maxHP = 200f;
    float currentHP = 200f;

    List<string> damagedByTags = new List<string>();
    #endregion

    public void alignAsEnemy()
    {
        damagedByTags = new List<string>();
        this.gameObject.tag = "EnemyShip";
        acceptDamageFromPlayer();

        if( turrets != null )
            turrets.targetTag = "Player";

        if (subComponents != null)
            foreach (ShipComponent s in subComponents)
                s.alignAsEnemy();
            
    }

    public void alignAsPlayer()
    {
        damagedByTags = new List<string>();
        this.gameObject.tag = "Player";
        acceptDamageFromEnemy();

        if (turrets != null)
            turrets.targetTag = "EnemyShip";

        if (subComponents != null)
            foreach (ShipComponent s in subComponents)
                s.alignAsPlayer();

    }

    public void acceptDamageFromPlayer()
    {        
        damagedByTags.Add("Projectile");
        damagedByTags.Add("Rocket");
    }

    public void acceptDamageFromEnemy()
    {        
        damagedByTags.Add("EnemyProjectile");
        damagedByTags.Add("EnemyRocket");
    }

    private DamageInfo projectileImpactDamage = new DamageInfo(10f, null, null, false);
    
    private bool turretsAutofire = false;
    public bool TurretsAutofire{
        get{ return turretsAutofire; }
        set{ turretsAutofire = value; if( hasTurrets() ) turrets.FireAtWill = turretsAutofire; }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (isDeattached) return;

        if (damagedByTags.Contains(other.gameObject.tag))
        {
            handleImpact(projectileImpactDamage);
            ProjectilePool.get().returnToPool(other.gameObject);
        }
    }

    public void OnCollisionEnter(Collision other)
    {
        if (isDeattached && isDestroyed) return;

        if (damagedByTags.Contains(other.gameObject.tag))
        {
            handleImpact(projectileImpactDamage);
        }
    }

    public void handleImpact(DamageInfo damage)
    {
        float damageHP = 1f;

        if (damage != null)
        {
            if (damage.DOT)
                damageHP = damage.damage * Time.deltaTime;
            else
                damageHP = damage.damage;
        }

        if( !shieldsActive() )
            CurrentHP -= damageHP;

        if (!isDeattached && CurrentHP <= 0f)
            deattachComponent();

        if (isDeattached && !isDestroyed && currentHP <= (maxHP * -1f))
            destroyComponent();

        //Debug.LogWarning("Component [" + name + "] health: " + currentHP + " by " + ( damage.weapon != null ? damage.weapon.name  : "???" ) );
    }

    public void deattachComponent()
    {
        //Debug.LogWarning("Component deattached");

        if (IsCritical) 
            ParentShip.criticalComponentLost();
        else
            ParentShip.componentLost();

        /*
        GameObject dummy = new GameObject();
        dummy.name = "Component Dummy";
        dummy.transform.position = Vector3.zero;
        dummy.transform.rotation = Quaternion.identity;
        dummy.transform.localScale = transform.parent.localScale;
        */
        
        //transform.parent = dummy.transform;

        isDeattached = true;
        rigidbody.mass = 1000f;
        rigidbody.isKinematic = false;

        /*
        foreach (Collider c in GetComponents<Collider>()) {
            c.isTrigger = false;
        }
        */

        Vector3 baseDirection = (baseBone != null ? baseBone.up : transform.up);
        Vector3 randomDirection = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        Vector3 dettachDirection = baseDirection * 10f + randomDirection * 2.5f;

        rigidbody.AddForce(dettachDirection * 10f, ForceMode.VelocityChange);
        rigidbody.AddTorque(randomDirection*2f, ForceMode.VelocityChange);

        deathEffect.Play();

        if (hasShields())
            deactivateShields();

        if (hasTurrets())
            turrets.FireOnTarget = false;

        if (subComponents != null && subComponents.Count > 0) {
            foreach (ShipComponent c in subComponents) {
                c.currentHP = 0f;
                c.deattachComponent();
            }
        }

    }

    
    public void destroyComponent() {
        Debug.LogWarning("Component destroyed");
        isDestroyed = true;
        ParentMountPoint.cleanUp();
    }

    #region properties
    public bool IsCritical { get; set; }
    public float MaxHP
    {
        get { return maxHP; }
        set { maxHP = value; currentHP = maxHP; }
    }
    public float CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }

    public Transform BaseBone
    {
        get { return baseBone; }
        set { baseBone = value; }
    }

    public ShipComponent ParentComponent
    {
        get
        {
            return parentComponent;
        }
        set
        {
            parentComponent = value;
        }
    }

    public ComponentShipScript ParentShip
    {
        get
        {
            return parentShip;
        }
        set
        {
            parentShip = value;

            if (mountPoints != null && mountPoints.Count > 0)
                foreach (KeyValuePair<string, MountPoint> m in mountPoints) m.Value.ParentShip = value;
        }
    }

    public ComponentMountPoint ParentMountPoint { 
        get { return parentMountPoint; }
        set { parentMountPoint = value; } 
    }
    #endregion

    protected void reset(){
        parentShip = null;
        parentComponent = null;
        parentMountPoint = null;
        baseBone = null;

        currentHP = maxHP;

        isDeattached = false;
        isDestroyed = false;

        weapons = null;
        turrets = null;
        shields = null;

        rigidbody.isKinematic = true;
        foreach (Collider c in GetComponents<Collider>())
        {
            c.isTrigger = true;
        }

        if (deathEffect != null)
        {
            deathEffect.Stop();
        }
        else
        {
            GameObject deathEffectPrefab = Resources.Load("Prefabs/boxFire") as GameObject;
            GameObject deathEffectInstance = Instantiate(deathEffectPrefab) as GameObject;
            deathEffect = deathEffectInstance.GetComponent<ParticleSystem>();

            deathEffect.emissionRate *= 6f;
            deathEffect.startSize *= 5.5f;
        }
    }

    #region JSONMonoBehaviour config & cleanup
    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        reset();

        // dont update baseBone once found
        if (baseBone == null)
        {
            string baseBoneName = config["bone"].Value;
            baseBone = transform.Find(baseBoneName);

            if (baseBone == null)
            {
                Debug.LogError("Component failed to find base bone: " + baseBoneName, this);
                return null;
            }
        }

        // only setup mount points once
        if (mountPoints == null || mountPoints.Count > 0) { 

            JSONArray mountPointDefs = config["mountPoints"].AsArray;
            if (mountPointDefs.Count > 0)
            {

                mountPoints = new Dictionary<string, MountPoint>();

                foreach (JSONNode mountDef in mountPointDefs)
                {
                    MountPoint mountPoint = null;
                    string type = mountDef["type"].Value;
                    string name = mountDef["name"].Value;
                    string path = mountDef["path"].Value;
                    string boneName = mountDef["bone"].Value;

                    Transform bone = baseBone.Find(path.Length > 0 ? path + "/" + boneName : boneName);
                    //Debug.Log("Mount point at: " + (path.Length > 0 ? path + "/" + boneName : boneName), bone);

                    Vector3 scale = vector3FromJSONArray1(mountDef["scale"].AsArray);
                    Vector3 rotation = vector3FromJSONArray0(mountDef["rotate"].AsArray);

                    switch (type)
                    {
                        case "Weapon":
                            //Debug.Log(gameObject.name+": create weapon mount point: " + name, bone);
                            mountPoint = new WeaponMountPoint(bone, scale, rotation);
                            break;
                        case "Shield":
                            //Debug.Log(gameObject.name + ": create shield mount point: " + name, bone);
                            mountPoint = new ShieldMountPoint(bone, scale, rotation);
                            break;
                        case "Component":
                            //Debug.Log(gameObject.name + ": create component mount point: " + name, bone);
                            mountPoint = new ComponentMountPoint(bone, scale, rotation);
                            break;
                        case "Turret":
                            //Debug.Log(gameObject.name + ": create turret mount point: " + name, bone);
                            mountPoint = new TurretMountPoint(bone, scale, rotation);
                            break;
                        default:
                            Debug.LogWarning(gameObject.name + ": Unknown component type: " + type);
                            break;
                    }

                    if (mountPoint != null)
                    {
                        mountPoint.ParentComponent = this;
                        mountPoint.ParentShip = ParentShip;
                        //Debug.Log(name);
                        mountPoints.Add(name, mountPoint);
                    }
                }
            }
        }

        deathEffect.transform.parent = baseBone;
        deathEffect.transform.position = Vector3.zero;
        deathEffect.transform.localScale = Vector3.one * 10f;

        rayCastColliders = getColliders();
        rayCastColliders.name = "Raycast Collider";
        rayCastColliders.transform.parent = this.transform;

        if (tag == "Player")
            rayCastColliders.layer = LayerMask.NameToLayer("Player Raycast Collider");
        else
            rayCastColliders.layer = LayerMask.NameToLayer("Enemy Raycast Collider");

        return this;
    }    

    public override void cleanUp()
    {
        if (mountPoints != null && mountPoints.Count > 0) {
            foreach (KeyValuePair<string, MountPoint> mountPoint in mountPoints) {
                mountPoint.Value.cleanUp();
            }
        }
    }
    #endregion

    #region managers

    ///  Weapons

    public void registerWeapon(gunScript weapon, int index)
    {
        if (weapons == null)
            weapons = gameObject.AddComponent<WeaponManager>();

        weapons.addWeaponToGroup(weapon, index);
    }

    public void unregisterWeapon(gunScript weapon)
    {
        if (weapons == null)
        {
            Debug.LogWarning("unregisterWeapon called with no weapons registered!");
            return;
        }

        weapons.removeWeapon(weapon);
    }

    ///  Turrets

    public void registerTurret(TurretController2 turret)
    {
        if (turrets == null)
        {
            turrets = gameObject.AddComponent<TurretManager>();

                    TimeManager.Get().timeDependantUpdate += turrets.updateTarget;

                    if (this.tag == "Player")
                    {
                        turrets.targetTag = "EnemyShip";
                    }
                    else {
                        turrets.targetTag = "Player";
                    }
        }

        turrets.registerTurret(turret);
    }

    public void unregisterTurret(TurretController2 turret)
    {
        if (turrets == null)
        {
            Debug.LogWarning("unregisterTurret called with no turret registered!");
            return;
        }

        turrets.unregisterTurret(turret);
    }

    /// Shields

    public void registerShield(ShieldController Shield)
    {
        if (shields == null)
            shields = gameObject.AddComponent<ShieldManager>();

        shields.registerShield(Shield);
    }

    public void unregisterShield(ShieldController Shield)
    {
        if (shields == null)
        {
            Debug.LogWarning("unregisterShield called with no Shield registered!");
            return;
        }

        shields.unregisterShield(Shield);
    }

    #endregion

    #region subComponent handlers
    public void registerComponent(ShipComponent component)
    {
        if (subComponents == null)
            subComponents = new List<ShipComponent>();

        subComponents.Add(component);
    }
    public void unregisterComponent(ShipComponent component)
    {
        if (subComponents == null)
        {
            Debug.LogWarning("unregisterComponent called with no components registered!");
            return;
        }

        subComponents.Remove(component);
    }
    #endregion

    bool hasWeapons()
    {
        return weapons != null && weapons.weaponsRegistered() > 0;
    }

    bool hasShields()
    {
        return shields != null && shields.shieldsRegistered() > 0;
    }

    bool hasTurrets()
    {
        return turrets != null && turrets.turretsRegistered() > 0;
    }

    // Weapons handling
    public void fireWeaponsGroup(int group, bool recursive = false)
    {
        if (isDeattached || isDestroyed) return;

        if (hasWeapons())
        {
            //Debug.LogWarning("fireWeaponsGroup " + group);
            weapons.fireWeaponsGroup(group);
        }

        if (recursive && subComponents != null)
            foreach (ShipComponent s in subComponents)
                s.fireWeaponsGroup(group, recursive);
    }
    public void fireAllWeapons(bool recursive = false)
    {
        if (isDeattached || isDestroyed) return;

        if (hasWeapons())
        {
            weapons.fireAllWeapons();
        }

        if (recursive && subComponents != null)
            foreach (ShipComponent s in subComponents)
                s.fireAllWeapons(recursive);
    }

    // Shield handling

    public bool shieldsActive() {
        return hasShields() && shields.areShieldsUp();
    }

    public void activateShields()
    {
        if (hasShields() && !isDeattached && !isDestroyed)
            shields.activateShields();
    }

    public void deactivateShields()
    {
        if (hasShields())
            shields.deactivateShields();
    }


    #region setup
    // setup a list of mounted stuff
    internal void setupMounts(JSONClass mounts)
    {
        foreach (string mountPointName in mounts.Keys) {
            JSONClass mountInfo = mounts[mountPointName].AsObject;
            MountPoint mountPoint = mountPoints[mountPointName];

            mountPoint.setup(mountInfo);
        }

        activateShields();
    }

    // setup a single mounted stuff
    public void mount(string mountPointName, Object mountedItem, Vector3 scale, Vector3 rotation) {
        MountPoint mountPoint = null;
        if( mountPoints.TryGetValue(mountPointName, out mountPoint ) ){
            mountPoint.populate(mountedItem, scale, rotation);
        }

        activateShields();
    }
    #endregion


    public float MaxShield
    {
        get
        {
            float maxShieldValue = 0f;

            if (shields != null)
                maxShieldValue += shields.maxShieldPower();

            if (subComponents != null && subComponents.Count > 0)
            {
                foreach (ShipComponent s in subComponents)
                    maxShieldValue += s.MaxShield;
            }

            return maxShieldValue;
        }
    }

    public float CurrentShield {  
        get
        {
            float currentShieldValue = 0f;

            if (shields != null)
                currentShieldValue += shields.currentShieldPower();

            if (subComponents != null && subComponents.Count > 0)
            {
                foreach (ShipComponent s in subComponents)
                    currentShieldValue += s.CurrentShield;
            }

            return currentShieldValue;
        }
    }

    internal bool isAlive()
    {
        if (currentHP > 0f && !isDeattached && !isDestroyed) {
            if (subComponents != null && subComponents.Count > 0)
            {
                foreach (ShipComponent s in subComponents)
                {
                    if (s.IsCritical && s.isAlive() == false) return false;
                }
            }
            return true;
        }
        return false;
    }
}
