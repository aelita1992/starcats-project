﻿using UnityEngine;
using System.Collections;

public class GuiSliderBars : MonoBehaviour 
{
	public EnergyBar shieldBar;
	public EnergyBar healthBar;

	float hitPoints;
	float shieldPoints;

	private static GuiSliderBars pseudoSingleton;	
	public static GuiSliderBars Get()
	{
		return pseudoSingleton;
	}

	void Start()
	{
		pseudoSingleton = this;
		shieldPoints = 200;
		hitPoints = 100;
	}

	public void SetHitPoints(float value)
	{
		hitPoints = value;
		healthBar.SetValueCurrent((int)value);
	}

	public void SetShieldPoints(float value)
	{
		shieldPoints = value;
		shieldBar.SetValueCurrent((int)	value);
	}

}
