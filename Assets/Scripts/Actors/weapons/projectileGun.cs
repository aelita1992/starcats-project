﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class projectileGun : gunScript {

	protected string projectilePrefabName = null;
    protected float projectileSpeed = 0f;

	projectileScript loadedProjectile = null;

	public override JSONMonoBehaviour applyConfig(JSONClass config)
	{
		base.applyConfig (config);

        projectilePrefabName = config["projectile"].Value;
        projectileSpeed = config["projectileSpeed"].AsFloat;

		initialized = true;
		
		return this;
	}

	public projectileScript prepareToFire( ){
		
		if( loadedProjectile == null )
            loadedProjectile = ProjectilePool.get().getScript(projectilePrefabName);

        if (loadedProjectile == null)
            Debug.LogWarning("Failed to get projectile: " + projectilePrefabName);

		return loadedProjectile;
	}
	
	public override void fire()
	{
		projectileScript projectilePrefabClone = null;
		
		if (loadedProjectile != null) {
			projectilePrefabClone = loadedProjectile;
		} else {
			projectilePrefabClone = prepareToFire();
		}
		
		//Debug.Log (this.transform.parent.name + " fired: " + projectilePrefabClone.name, projectilePrefabClone);
		
		if (projectilePrefabClone != null)
		{
			projectilePrefabClone.transform.position = muzzleBone.transform.position;
            projectilePrefabClone.transform.rotation = muzzleBone.transform.rotation;
			projectilePrefabClone.projectileDamage = damage;
			
			Rigidbody RigidbodyHandle = projectilePrefabClone.rigidbody;
			RigidbodyHandle.WakeUp();
			
			// nadajemy skale jak w obiekcie nadrzednym
			projectilePrefabClone.transform.localScale =  attachBone != null ? attachBone.lossyScale : Vector3.one;// this.transform.lossyScale;
			float scaleConstant = projectilePrefabClone.transform.localScale.magnitude;
			
			// skala pocisku         
			if (projectileScale != null && projectileScale.Count == 3)
			{
				projectilePrefabClone.transform.localScale += (new Vector3(projectileScale[0].AsFloat, projectileScale[1].AsFloat, projectileScale[2].AsFloat) );
			}
			
			// masa i predkosc wg definicji pocisku
            float speed = projectileSpeed > 0f ? projectileSpeed : projectilePrefabClone.projectileSpeed;
			RigidbodyHandle.mass = projectilePrefabClone.projectileMass * scaleConstant;
            RigidbodyHandle.AddForce(speed * muzzleBone.transform.up, ForceMode.VelocityChange);
			
			// rozrzut
			if (projectileSpread != null && projectileSpread.Count == 3)
			{
                RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[0].AsFloat, projectileSpread[0].AsFloat) * projectilePrefabClone.projectileSpeed * muzzleBone.transform.right, ForceMode.VelocityChange);
                RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[1].AsFloat, projectileSpread[1].AsFloat) * projectilePrefabClone.projectileSpeed * muzzleBone.transform.up, ForceMode.VelocityChange);
                RigidbodyHandle.AddForce(Random.Range(-1 * projectileSpread[2].AsFloat, projectileSpread[2].AsFloat) * projectilePrefabClone.projectileSpeed * muzzleBone.transform.forward, ForceMode.VelocityChange);
			}
			
			
			if (projectilePrefabClone.tag == "Rocket")
			{
                if (parentShip == null || parentShip.tag == "Player")
				{
					projectilePrefabClone.trackTag("EnemyShip");
				}
				else {
					projectilePrefabClone.trackTag("Player");
				}
			}
			
			loadedProjectile = null;
			
			updateFireTimer();
			
		}
	}
}
