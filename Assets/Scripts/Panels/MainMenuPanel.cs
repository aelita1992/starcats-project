﻿	using UnityEngine;
using System.Collections;

public class MainMenuPanel : MonoBehaviour 
{
	public UIButton start;
	public UIButton settings;
	public UIButton exit;

	private Animator startButtonAnimator; 
	private Animator settingsButtonAnimator;
	private Animator exitButtonAnimator;

	void Awake()
	{
		startButtonAnimator = start.gameObject.GetComponent<Animator>();
		settingsButtonAnimator = settings.gameObject.GetComponent<Animator>();
		exitButtonAnimator = exit.gameObject.GetComponent<Animator>();
	}

	public void Expand()
	{
		gameObject.SetActive(true);

		startButtonAnimator.SetBool ("IsFadingIn", true);
		settingsButtonAnimator.SetBool ("IsFadingIn", true);
		exitButtonAnimator.SetBool ("IsFadingIn", true);
	}

	public void Collapse()
	{
		gameObject.SetActive(false);

		startButtonAnimator.SetBool ("IsFadingOut", true);
		settingsButtonAnimator.SetBool ("IsFadingOut", true);
		exitButtonAnimator.SetBool ("IsFadingOut", true);
	}
}
