﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMovementManager
{
	public enum MovementType {FORWARD = 0, DIAGONAL = 1, SINUSOIDAL = 2, FORWARD_AND_SPLIT = 3, CIRCLE = 4, HORIZONTAL = 5};

	private Dictionary <MovementType, MovementController> movementControllers;
	
	static EnemyMovementManager enemyMovementController;

	public static EnemyMovementManager Get()
	{
		if(enemyMovementController == null)	
			enemyMovementController = new EnemyMovementManager();
		
		return enemyMovementController;
	}
	
	private EnemyMovementManager()
	{
		movementControllers = new Dictionary <MovementType, MovementController>();
		
		movementControllers.Add (MovementType.FORWARD, new MoveForward());
		movementControllers.Add (MovementType.HORIZONTAL,new MoveHorizontally());
		movementControllers.Add (MovementType.DIAGONAL, new MoveDiagonally());
		movementControllers.Add (MovementType.SINUSOIDAL, new MoveSinusoidally());
		movementControllers.Add (MovementType.FORWARD_AND_SPLIT, new MoveForwardAndSplit());
		movementControllers.Add (MovementType.CIRCLE, new MoveInCircle());
		
		TimeManager.Get ().timeDependantUpdate += PerformMovements;
	}
	
	public void SetMovementStyle(List<GameObject> enemyShips, MovementType movementType)
	{
		for(int i = 0; i < enemyShips.Count; i++)
			movementControllers[movementType].AddEnemy(enemyShips[i]);
	}
	
	public void PerformMovements()
	{
		foreach (MovementType movementType in movementControllers.Keys)
		{
			movementControllers[movementType].PerformMovement();
		}
	}

	//	public enum MovementType {FORWARD = 0, DIAGONAL = 1, SINUSOIDAL = 2, FORWARD_AND_SPLIT = 3, CIRCLE = 4, HORIZONTAL = 5};
	public MovementType TranslateMovementTypeString(string movementString)
	{
		movementString = movementString.ToLower();

		switch(movementString)
		{
			case "forward": 			return MovementType.FORWARD;
			case "diagonal": 			return MovementType.DIAGONAL;
			case "sinusoidal": 			return MovementType.SINUSOIDAL;
			case "forward_and_split":	return MovementType.FORWARD_AND_SPLIT;
			case "circle": 				return MovementType.CIRCLE;
			case "horizontal": 			return MovementType.HORIZONTAL;
		}

		return MovementType.FORWARD;
	}
	
}
