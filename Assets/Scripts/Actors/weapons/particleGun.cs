﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class particleGun : gunScript {
    GameObject bulletEmitterBase = null;
    ParticleSystem bulletEmitter = null;
    ParticleSystem fireEffect = null;

    bool isMinigun = false;
    Transform barrels = null;

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        base.applyConfig(config);

        GameObject bulletEmitterPrototype = Resources.Load("Prefabs/spamCannonEmitter") as GameObject;
        bulletEmitterBase = GameObject.Instantiate(bulletEmitterPrototype) as GameObject;

        bulletEmitterBase.transform.position = muzzleBone.position;
        bulletEmitterBase.transform.rotation = muzzleBone.rotation;
        bulletEmitterBase.transform.parent = muzzleBone;

        bulletEmitter = bulletEmitterBase.GetComponent<ParticleSystem>();

        isMinigun = config["isMinigunDemo"].AsBool;
        if (isMinigun) {
            Debug.Log("isMinigun", this);
            barrels = transform.Find("Container/Handle/Handle|AttachBone/Handle|Barrels");
            
            GameObject fireEffectPrototype = Resources.Load("Prefabs/minigunFire") as GameObject;
            GameObject fireEffectObject = GameObject.Instantiate(fireEffectPrototype) as GameObject;
            if (fireEffectObject != null)
            {
                fireEffectObject.transform.position = muzzleBone.position;
                fireEffectObject.transform.rotation = Quaternion.LookRotation(muzzleBone.up);
                fireEffectObject.transform.parent = muzzleBone;

                fireEffect = fireEffectObject.GetComponent<ParticleSystem>();
            }

            if (barrels == null)
            {
                Debug.LogWarning("err");
                listBones(transform);
            }
        }
        /*
        Debug.Log("emitter base:", bulletEmitterBase);
        Debug.Log("emitter:", bulletEmitter);
        */
        initialized = true;

        return this;
    }

    private float spread = 0.02f;
    public override void fire()
    {
        if (bulletEmitter != null)
        {
            Vector3 fireDir = muzzleBone.up;
            Vector3 fireSrc = muzzleBone.position;

            fireDir.x += Random.Range(-1f * spread, spread);
            fireDir.y += Random.Range(-1f * spread, spread);
            fireDir.z += Random.Range(-1f * spread, spread);

            fireSrc.x += Random.Range(-1f * spread * 10f, spread * 10f);
            fireSrc.y += Random.Range(-1f * spread * 10f, spread * 10f);
            //fireSrc.z += Random.Range(-1f * spread, spread);
            fireSrc.z = -70f + Random.Range(-1f, 1f);

            bulletEmitter.Emit(fireSrc, fireDir * 950f, 3f, 0.75f, Color.white);

            if (isMinigun && barrels != null)
            {
                barrels.Rotate(0f, 30f, 0f);

                if (fireEffect != null && fireEffect.isPlaying == false)
                {
                    fireEffect.Play();
                }
            }
        }
    }
}
