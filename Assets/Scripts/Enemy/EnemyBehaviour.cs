﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour
{
	public enum EntryPoint {EAST, NORTH, WEST};
	EntryPoint entryPoint;

	public EnemyManager.EnemyTypes enemyType { get; set; }

	public Vector3 Speed { get; set; }
	public Vector3 MaxSpeed { get; set; }
	public Vector3 CurrentDirection { get; set; }

	public Vector2 xySpeedChange_Pace {get; set;}

	public int horizontalMovementStage {get; set;}
	public int verticalMovementStage {get; set;}

	public void SetEntryPoint(EntryPoint _entryPoint)
	{
		entryPoint = _entryPoint;
	}
}
