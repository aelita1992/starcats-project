﻿using UnityEngine;
using System.Collections;

public class MissileAI : MovementAI
{


    public MissileAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate)
    {
    }


    Vector3 targetVector = Vector3.zero;
//    float distance = 0;
//    float timeToTarget = 0;
//    float timeToStop = 0;
//    float distanceToStop = 0;

    private void updateStats()
    {
        targetVector = targetPos - currentPos;

//        distance = targetVector.magnitude;
//        timeToTarget = (currentVelocity.magnitude == 0) ? float.MaxValue : distance / currentVelocity.magnitude;
//        timeToStop = currentVelocity.magnitude / (MaxAccel.magnitude / 2f);
//        distanceToStop = timeToStop / 2;
    }

    public override void updateAcceleration()
    {
        if (!updateState()) return;

        updateStats();
        Vector3 intendedChange = Vector3.zero;

        intendedChange = targetVector;

        currentAccel = Vector3.ClampMagnitude(intendedChange, MaxAccel.magnitude);
    }

    public override void updateVelocity()
    {
        if (!updateState()) return;
        updateStats();

        Vector3 intendedVelocity = currentVelocity + currentAccel;

        currentVelocity.x = Mathf.Clamp(intendedVelocity.x, MaxVelocity.x * -1f, MaxVelocity.x);
        currentVelocity.y = Mathf.Clamp(intendedVelocity.y, MaxVelocity.y * -1f, MaxVelocity.y);
        currentVelocity.z = Mathf.Clamp(intendedVelocity.z, MaxVelocity.z * -1f, MaxVelocity.z);
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position += currentVelocity;
    }

}
