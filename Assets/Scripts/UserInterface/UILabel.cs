﻿using UnityEngine;
using System.Collections;


public class UILabel : MonoBehaviour {
	
	public Color buttonActiveColor;
	public Color buttonInactiveColor;
	
	TextMesh textMesh;
	
	bool currentToggle = false;
	
	void Awake() 
	{
		textMesh = GetComponent<TextMesh>();
	}

	void OnEnable() 
	{ 
		SetActivenessInfo( false );
	}

	public string GetText()
	{
		return textMesh.text;
	}

	public void SetText(string newText)
	{
		textMesh.text = newText;
	}
	
	public void SetActivenessInfo( bool isButtonActive )
	{
		currentToggle = isButtonActive;
		textMesh.color = (isButtonActive ? buttonActiveColor : buttonInactiveColor);
	}
}
