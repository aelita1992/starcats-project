﻿using UnityEngine;
using System.Collections;

public class shieldTrigger : MonoBehaviour {

    ShieldController shield = null;
    public void attach( ShieldController shield ) {
        this.shield = shield;    
    }

    void OnTriggerEnter(Collider other)
    {
        if (shield != null) shield.handleTriggerImpact(other);
    }
}
