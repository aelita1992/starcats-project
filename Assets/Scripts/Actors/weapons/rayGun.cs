﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public abstract class rayGun : gunScript {

	protected float detectRange = 1000f;
	protected float detectRadius = 10f;

	protected int detectionLayers;
	protected bool aligned = false;
	protected bool isPlayer = false;

    protected bool trackTarget = false;
    protected bool lookForMoreTargets = true;

	protected GameObject beamBase = null;

	public override JSONMonoBehaviour applyConfig(JSONClass config)
	{
		base.applyConfig (config);

        // domyslna warstwa - docelowo ustawiana w alignToLayer()
		detectionLayers = 1 << LayerMask.NameToLayer ("Enemy Raycast Collider");

        // pomocniczy obiekt do ktorego doczepiamy promienie i efekty
		beamBase = new GameObject ();
		beamBase.name = "beam base";

		beamBase.transform.position = muzzleBone.position;
		beamBase.transform.rotation = muzzleBone.rotation;
		beamBase.transform.parent = muzzleBone;

		return this;
	}

    // na bazie nadrzednego obiektu ustala, czy ma trafiac gracza czy wrogów
    protected void alignToLayer()
    {
        if (transform.parent.gameObject.layer == LayerMask.NameToLayer("PlayerShip"))
        {
            detectionLayers = 1 << LayerMask.NameToLayer("Enemy Raycast Collider");
            detectionLayers |= 1 << LayerMask.NameToLayer("EnemyShip");
            isPlayer = true;
        }
        else if (transform.parent.gameObject.layer == LayerMask.NameToLayer("EnemyShip"))
        {
            detectionLayers = 1 << LayerMask.NameToLayer("Player Raycast Collider");
            detectionLayers |= 1 << LayerMask.NameToLayer("PlayerShip");
            isPlayer = false;
        }
    }

	protected RaycastHit hit;
	protected GameObject target = null;

    GameObject getRaycastTarget() {

        bool somethingHit;

        //somethingHit = Physics.Raycast( muzzleBone.position, muzzleBone.up, out hit, detectRange, detectionLayers);
        somethingHit = Physics.SphereCast(muzzleBone.position, detectRadius, muzzleBone.up, out hit, detectRange, detectionLayers);

        if (somethingHit)
        {
            return hit.rigidbody.gameObject;
        }
        else
        {
            return null;
        }    
    }

    protected bool updateTarget(){
        target = getRaycastTarget();
        return target != null;
    }

	public override void fire(){
        if (!aligned)
        {
            alignToLayer();
            aligned = true;
        }

        updateTarget();
		updateFireTimer();
        updateBeam();
	}

	protected abstract void updateBeam ();

	void Update(){
        updateBeam();
	}
}
