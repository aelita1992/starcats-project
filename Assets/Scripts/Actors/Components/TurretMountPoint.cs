﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class TurretMountPoint : MountPoint
{
    public TurretMountPoint(Transform mountLocation, Vector3 mountScale, Vector3 mountRotation)
        : base(mountLocation, mountScale, mountRotation)
    {

    }

    TurretController2 attachedTurret = null;
    string turretName = "";

    public override void populate(Object mountedItem, Vector3 scale, Vector3 rotation)
    {
        if (mountedItem.GetType() == typeof(TurretController2) || mountedItem.GetType().IsSubclassOf(typeof(TurretController2)))
        {
            if (attachedTurret != null)
                cleanUp();

            //Debug.LogWarning("Attaching turret: " + turretName);
            attachedTurret = (TurretController2)mountedItem;

            attachedTurret.transform.parent = mountLocation;
            attachedTurret.transform.position = Vector3.zero;
            attachedTurret.transform.rotation = Quaternion.identity;
            attachedTurret.transform.localScale = Vector3.one * 0.1f;

            setupTransform(attachedTurret.transform);

            if (scale != Vector3.one) attachedTurret.transform.localScale += scale;
            if (rotation != Vector3.zero) attachedTurret.transform.Rotate(rotation);

            ParentComponent.registerTurret(attachedTurret);
        }
        else
        {
            Debug.LogWarning("Invalid object type for mount point! " + mountedItem.GetType());
        }
    }

    public override void cleanUp()
    {
        if (attachedTurret != null)
        {
            ParentComponent.unregisterTurret(attachedTurret);
            MiscAssetPool.get().returnToPool(attachedTurret.gameObject);
            attachedTurret = null;
        }
    }
    public override void setup(JSONClass mountInfo)
    {
        turretName = mountInfo["name"].Value;

        JSONMonoBehaviour turretPrefab = MiscAssetPool.get().getScript(turretName);
        if (turretPrefab != null)
        {
            TurretController2 newTurret = turretPrefab.GetComponent<TurretController2>();
            if (newTurret != null)
            {
                populate(newTurret, Vector3.one, Vector3.zero);
            }
            else
            {
                Debug.LogWarning("Invalid asset: " + turretName);
            }

        }
        else
        {
            Debug.LogWarning("Turret not found: " + turretName);
        }
    }
}
