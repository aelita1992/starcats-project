﻿using UnityEngine;
using System.Collections;

public class AsteroidDeactivationController : MonoBehaviour
{
	void OnTriggerExit(Collider other)
	{
		transform.rotation = Quaternion.Euler(0, 0, 0);
		gameObject.SetActive(false);
	}
}
