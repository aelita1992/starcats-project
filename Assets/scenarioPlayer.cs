﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scenarioPlayer : MonoBehaviour {

    public Canvas levelUI = null;
    public RectTransform nextLevelPanel = null;
    public RectTransform deathPanel = null;

    static scenarioPlayer instance = null;
    public static scenarioPlayer get()
    {
        return instance;
    }

    Scenario s = null;
    public PlayerConnectorScript player = null;
    public string scenarioName = "campaign";

	// Use this for initialization
	void Start () {
        if (instance == null)
        {
            instance = this;

            levelUI.enabled = true;

            s = Scenario.fromFile(scenarioName);

            s.nextLevel += levelStart;
            s.nextStage += stageStart;
            s.nextChapter += chapterStart;

            s.start(Time.time);

            TimeManager.Get().timeDependantUpdate += advanceScenario;
        }
	}

    float levelStartTime = 0f;
    void levelStart(Level l)
    {
        //Debug.LogError("Level start event: " + l.Name);
        levelStartTime = Time.time;

        nextLevelPanel.transform.parent.transform.parent.gameObject.SetActive(true);
        nextLevelPanel.gameObject.SetActive(true);
        Transform levelNameTrnasform = nextLevelPanel.FindChild("LevelName");
        Text levelName = levelNameTrnasform.GetComponent<Text>();
        if (levelName != null) {
            levelName.text = l.Name;
        }
    }
    void stageStart(Stage s)
    {
        //Debug.LogError("stageStart start event: " + s.Name);
    }
    void chapterStart(Chapter c)
    {
        //Debug.LogError("chapterStart start event: " + c.Name);
    }

    public void restartLevel() {
        s.restartLevel();
    }

	public void advanceScenario () {
        if (player.isAlive())
        {
            deathPanel.gameObject.SetActive(false);
        }
        else
        {
            nextLevelPanel.gameObject.SetActive(false);
            deathPanel.gameObject.SetActive(true);
        }

        if (Time.time - levelStartTime >= 2f) {
            nextLevelPanel.gameObject.SetActive(false);
        }

        if (player.isAlive() && !s.IsDone)
            s.update(TimeManager.Get().timeSinceStart);	
	}
}
