﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

//[ExecuteInEditMode]
public abstract class JSONAssetManager<ConcreteManagerType, PooledType>
    where ConcreteManagerType : JSONAssetManager<ConcreteManagerType, PooledType>, new()
    where PooledType : JSONMonoBehaviour
{

    /////////////////////////////////////
    // pamietac ustawic te dwie rzeczy!!!
    /////////////////////////////////////
    protected string typeName = null;   // nazwa typu assetów
    protected string fileName = null;   // scierzka/nazwa pliku JSON
    protected string listName = null;   // nazwa wezla z ktorego pobieramy dane (zakladamy, ze to bedzie tablica, tzn taki plik: { "listName": [ {}, {}, {} ] } )

    protected bool fileLoaded = false;

    protected bool logEverything = false;
    protected bool logWarnings = true;
    protected bool logAssets = false;

    protected Dictionary<string, GameObjectPool<PooledType>> pool = new Dictionary<string, GameObjectPool<PooledType>>();
    public void returnToPool(GameObject prefab)
    {

        JSONMonoBehaviour prefabConfig = prefab.GetComponent<JSONMonoBehaviour>();
        if (prefabConfig != null)
            foreach (var poolEntry in pool)
            {
                GameObjectPool<PooledType> poolType = poolEntry.Value;
                if (prefabConfig.parentPool == poolType)
                {
                    poolType.addToPool(prefab);
                    return;
                }
            }

        Debug.LogError("failed to return prefab to any pool", prefab);

#if UNITY_EDITOR
        if (UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode)
        {
            GameObject.Destroy(prefab);
        }
        else
        {
            GameObject.DestroyImmediate(prefab);
        }
#else    
        GameObject.Destroy(prefab);
#endif

    }

    public void prewarmPools(int count)
    {
        foreach (var poolEntry in pool)
        {
            GameObjectPool<PooledType> poolType = poolEntry.Value;
            poolType.preWarm(count);
        }
    }

    public void verifyPool(string name)
    {
        GameObjectPool<PooledType> poolType = null;
        pool.TryGetValue(name, out poolType);
        if (poolType != null) poolType.verify();
    }

    public void prewarmPool(string name, int count)
    {
        GameObjectPool<PooledType> poolType = null;
        pool.TryGetValue(name, out poolType);
        if (poolType != null) poolType.preWarm(count);
    }

    private static ConcreteManagerType instance = new ConcreteManagerType();
    public static ConcreteManagerType get()
    {
        if (!instance.fileLoaded) instance.loadJSON();
        return instance;
    }

    public static ConcreteManagerType loadAndGet()
    {
        if (!instance.fileLoaded) instance.loadJSON();
        return instance;
    }


    public JSONAssetManager() { }

    protected void loadJSON()
    {

        TextAsset configJSON = Resources.Load(fileName) as TextAsset;

        if (configJSON == null)
        {
            Debug.LogError("Error loading " + fileName);
            return;
        }

        var configData = JSON.Parse(configJSON.text);

        if (configData == null)
        {
            Debug.LogError("Error parsing " + fileName);
            return;
        }

        string configPath = null;
        string prefabPath = null;
        JSONClass configPaths = configData["config"].AsObject;

        if (configPaths == null || configPaths.Count == 0)
        {
            Debug.LogError("Error loading path config from " + fileName);
            return;
        }

        configPath = configPaths["configDir"].Value;
        prefabPath = configPaths["prefabDir"].Value;

        JSONArray prefabList = configData[listName].AsArray;

        if (prefabList == null || prefabList.Count == 0)
        {
            Debug.LogError("Error loading list \"" + listName + "\" from " + fileName);
            return;
        }

        if (processList(prefabList, configPath, prefabPath))
            fileLoaded = true;
        else
            Debug.LogError("Error processing list \"" + listName + "\" from " + fileName);
    }

    protected bool processList(JSONArray list, string configPath, string prefabPath)
    {

        foreach (JSONNode assetDef in list)
        {
            // info o tym, co wczytujemy: nazwa assetu, nazwa pliku JSON i nazwa prefaba w Unity
            string assetName = assetDef["name"].Value;
            string scriptName = assetDef["scriptName"].Value;
            string prefabName = assetDef["prefabName"].Value;

            if (logEverything) Debug.Log("Loading asset " + assetName + " from prefab: " + prefabName);

            // czytamy ustawienia assetu z jego pliku JSON
            TextAsset assetJSON = Resources.Load(configPath + "/" + scriptName) as TextAsset;

            if (assetJSON == null)
            {
                Debug.LogError("Error loading " + scriptName);
                continue;
            }

            JSONNode configNode = JSON.Parse(assetJSON.text);

            if (configNode == null)
            {
                Debug.LogError("Error parsing " + fileName);
                continue;
            }

            // próbujemy odczytać prefaba
            Object instanceObject = Resources.Load(prefabPath + "/" + prefabName, typeof(GameObject));
            if (instanceObject == null)
            {
                Debug.LogError("Error loading " + prefabPath + "/" + prefabName);
                continue;
            }

            GameObject instance = (GameObject)instanceObject;
            if (instance != null)
            {
                // miejsce na poprawki unikalne dla typu zasobu - processAsset()
                instance = processAsset(instance, configNode.AsObject);

                GameObjectPool<PooledType> newPool = new GameObjectPool<PooledType>(configNode.AsObject, instance, assetName, typeName);
                pool.Add(assetName, newPool);

                if (logAssets) Debug.Log("Loaded asset " + assetName + " from prefab: " + prefabName, instance);
            }
            else
            {
                if (logWarnings) Debug.LogWarning("Failed to load prefab: " + prefabName);
            }

        }

        return true;
    }

    private GameObjectPool<PooledType>.pooledData getFromPool(string name)
    {
        // wczytujemy config dopiero jak chcą od nas prefaba
        if (!fileLoaded) loadJSON();

        GameObjectPool<PooledType> prefabPool = null;


        if (pool.TryGetValue(name, out prefabPool))
        {
            GameObjectPool<PooledType>.pooledData data = prefabPool.NextItem;

            if (data == null) {
                Debug.LogError("prefabPool.NextItem is null!");
                return null; 
            }

            GameObject prefab = data.item;
            if (prefab == null) prefab = prefabPool.getNewPrefab();

            JSONClass config = prefabPool.ConfigObject;

            JSONMonoBehaviour prefabConfigHandle = data.config;
            if (prefabConfigHandle != null)
            {
                if (logEverything) Debug.Log("applying config: " + prefabConfigHandle);
                prefabConfigHandle.applyConfig(config);
            }
            else
            {
                Debug.LogError("Can't access prefabConfigHandle: " + prefab);
            }

            prefab.SetActive(true);

            // miejsce na poprawki unikalne dla typu zasobu - preparePrefab()
            preparePrefab(prefab, config);

            return data;
        }

        return null;
    }

    public GameObject getPrefab(string name)
    {
        var d = getFromPool(name);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.item;
        }
    }
    public PooledType getScript(string name)
    {
        var d = getFromPool(name);
        if (d == null)
        {
            return null;
        }
        else
        {
            return d.script;
        }
    }
    protected abstract GameObject processAsset(GameObject asset, JSONClass config);

    protected abstract GameObject preparePrefab(GameObject prefab, JSONClass config);

    public GameObject getPrefab(string name, Vector3 position, Quaternion rotation)
    {
        if (!fileLoaded) loadJSON();
        return _getPrefab(name, position, rotation);
    }
    public abstract GameObject _getPrefab(string name, Vector3 position, Quaternion rotation);
}