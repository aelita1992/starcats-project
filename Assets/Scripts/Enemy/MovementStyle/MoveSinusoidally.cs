﻿using UnityEngine;
using System.Collections;

public class MoveSinusoidally : MovementController
{

	public enum MovementStage 
	{
		Init = 0,
		NormalMovement = 1,
		BreakActive = 2,
		ReverseAcceleration = 3,
		AwaitingScreenCenter = 4,
		Exit = -1
	}

	protected override void ExecuteHorizontalMovement(int horizontalMovementStage)
	{	
		switch(horizontalMovementStage)
		{
		case (int)MovementStage.Init:
			Init ();
			break;

		case (int)MovementStage.NormalMovement:
			ControlHorizontalMovement();
			break;

		case (int)MovementStage.BreakActive:
			UseHorizontalBreak();
			break;

		case (int)MovementStage.ReverseAcceleration:
			ReverseHorizontalAcceleration();
			break;

		case (int)MovementStage.Exit:
			RemoveEnemyOnPosition(shipIterator);
			break;
		}
	}

	protected override void ExecuteVerticalMovement(int verticalMovementStage)
	{	
		switch(verticalMovementStage)
		{
		case (int)MovementStage.Init:
			Init ();
			break;

		case (int)MovementStage.NormalMovement:
			ControlVerticalMovement();
			break;
			
		case (int)MovementStage.BreakActive:
			UseVerticalBreak();
			break;
			
		case (int)MovementStage.ReverseAcceleration:
			ReverseVerticalAcceleration();
			break;

		case (int)MovementStage.AwaitingScreenCenter:
			CheckForScreenMiddle();
			break;

		case (int)MovementStage.Exit:
			RemoveEnemyOnPosition(shipIterator);
			break;
		}
	}
	
	private void Init()
	{
		enemyBehaviours[shipIterator].CurrentDirection = new Vector3(-1,-1,0);
		enemyBehaviours[shipIterator].MaxSpeed = new Vector3(4,0.3f,0);
		enemyBehaviours[shipIterator].Speed = new Vector3(-4,-0.3f,0);

		enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.NormalMovement;
		enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.NormalMovement;
	}
	
	private void ControlHorizontalMovement()
	{

		if(enemyObjects[shipIterator].transform.position.x < -200 && enemyBehaviours[shipIterator].CurrentDirection.x == -1)
		{
			enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.BreakActive;
		}
		else if (enemyObjects[shipIterator].transform.position.x > 200 && enemyBehaviours[shipIterator].CurrentDirection.x == 1)
		{
			enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.BreakActive;
		}
	}
	
	private void ControlVerticalMovement()
	{
		if(enemyObjects[shipIterator].transform.position.y < -200 && enemyBehaviours[shipIterator].CurrentDirection.y == -1)
		{
			//enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.BreakActive;
		}
		else if (enemyObjects[shipIterator].transform.position.y > 200 && enemyBehaviours[shipIterator].CurrentDirection.y == 1)
		{
			//enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.BreakActive;
		}
	}
	
	private void UseHorizontalBreak()
	{
	
		Vector3 speed = enemyBehaviours[shipIterator].Speed;
		Vector3 direction = enemyBehaviours[shipIterator].CurrentDirection;
		Vector2 xyChangePace = enemyBehaviours[shipIterator].xySpeedChange_Pace;

		speed.x = Mathf.SmoothDamp(speed.x, 0f, ref xyChangePace.x, 0.6f);

		if(Mathf.Abs(speed.x) < 0.5)
		{	
			direction.x = -direction.x;
			enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.ReverseAcceleration;
		}

		enemyBehaviours[shipIterator].Speed = speed;
		enemyBehaviours[shipIterator].xySpeedChange_Pace = xyChangePace;
		enemyBehaviours[shipIterator].CurrentDirection = direction;
	}
	
	
	private void UseVerticalBreak()
	{
		Vector3 speed = enemyBehaviours[shipIterator].Speed;
		Vector3 direction = enemyBehaviours[shipIterator].CurrentDirection;
		Vector2 xyChangePace = enemyBehaviours[shipIterator].xySpeedChange_Pace;

		speed.y = Mathf.SmoothDamp(speed.y, 0f, ref xyChangePace.y, 0.6f);
		
		if(Mathf.Abs(speed.y) < 0.2)
		{
			speed.y = 0;
			direction.y = -direction.y;
			enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.AwaitingScreenCenter;
		}

		enemyBehaviours[shipIterator].Speed = speed;
		enemyBehaviours[shipIterator].xySpeedChange_Pace = xyChangePace;
		enemyBehaviours[shipIterator].CurrentDirection = direction;
	}
	
	private void CheckForScreenMiddle()
	{
		if(Mathf.Abs(enemyObjects[shipIterator].transform.position.x) <= 5)
		{
			enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.ReverseAcceleration;
		}
	}
	
	private void ReverseHorizontalAcceleration()
	{
		Vector3 speed = enemyBehaviours[shipIterator].Speed;
		Vector3 maxSpeed = enemyBehaviours[shipIterator].MaxSpeed;
		Vector3 direction = enemyBehaviours[shipIterator].CurrentDirection;
		Vector2 xyChangePace = enemyBehaviours[shipIterator].xySpeedChange_Pace;

		speed.x = Mathf.SmoothDamp(speed.x, maxSpeed.x * direction.x, ref xyChangePace.x, 1f);
		
		if(maxSpeed.x - Mathf.Abs (speed.x) < 0.1)
		{
			enemyBehaviours[shipIterator].horizontalMovementStage = (int)MovementStage.NormalMovement;
		}

		enemyBehaviours[shipIterator].Speed = speed;
		enemyBehaviours[shipIterator].xySpeedChange_Pace = xyChangePace;
	}
	
	private void ReverseVerticalAcceleration()
	{
		Vector3 speed = enemyBehaviours[shipIterator].Speed;
		Vector3 maxSpeed = enemyBehaviours[shipIterator].MaxSpeed;
		Vector3 direction = enemyBehaviours[shipIterator].CurrentDirection;
		Vector2 xyChangePace = enemyBehaviours[shipIterator].xySpeedChange_Pace;

		speed.y = Mathf.SmoothDamp(speed.y, maxSpeed.y * direction.y, ref xyChangePace.y, 1f);
		
		if(maxSpeed.y - Mathf.Abs (speed.y) < 0.1)
		{
			enemyBehaviours[shipIterator].verticalMovementStage = (int)MovementStage.NormalMovement;
		}

		enemyBehaviours[shipIterator].Speed = speed;
		enemyBehaviours[shipIterator].xySpeedChange_Pace = xyChangePace;
	}
}
