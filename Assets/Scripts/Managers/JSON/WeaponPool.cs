﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class WeaponPool : JSONAssetManager<WeaponPool, gunScript>
{
    public WeaponPool()
    {
        fileName = "JSON/weaponManagerConfig";
        listName = "weapons";
        typeName = "weapons";
    }

    protected override GameObject processAsset(GameObject asset, JSONClass config)
    {
        return asset;
    }


    protected override GameObject preparePrefab(GameObject prefab, JSONClass config)
    {
        return prefab;
    }

    public override GameObject _getPrefab(string name, Vector3 position, Quaternion rotation)
    {
        GameObject prefab = getPrefab(name);
        if (prefab != null)
        {
            // jezeli bron ma wlasne kosci ustalajace pkt zaczepienia, ustawiamy kosc a nie caly prefab
            gunScript gunScriptComponent = prefab.GetComponent<gunScript>();
            if (gunScriptComponent != null)
            {

                if (gunScriptComponent.getAttachBone() != null)
                {
                    if (logEverything) Debug.Log("Attaching gun at bone", gunScriptComponent.getAttachBone());

                    gunScriptComponent = prefab.GetComponent<gunScript>();
                    gunScriptComponent.getAttachBone().transform.position = position;
                    gunScriptComponent.getAttachBone().transform.rotation = rotation;

                    return prefab;
                }
                else
                {
                    if (logWarnings) Debug.LogWarning("Attaching gun at prefab");
                    if (logWarnings) Debug.LogWarning("Cause: gunScriptComponent.AttachBone == null");
                    prefab.transform.position = position;
                    prefab.transform.rotation = rotation;
                    return prefab;
                }

            }
            else
            {
                if (logWarnings) Debug.LogWarning("Attaching gun at prefab");
                if (logWarnings) Debug.LogWarning("Cause: gunScriptComponent == null");
                prefab.transform.position = position;
                prefab.transform.rotation = rotation;
                return prefab;
            }

        }
        else
        {
            if (logWarnings) Debug.LogWarning("Prefab not found:" + name);
        }

        return null;
    }
}
