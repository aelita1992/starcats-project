﻿using UnityEngine;
using System.Collections;

public class DemoPanel : MonoBehaviour 
{

	public void Expand()
	{
		gameObject.SetActive(true);
	}
	
	public void Collapse()
	{
		gameObject.SetActive(false);
	}
}
