﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ComponentShipPool : JSONAssetManager<ComponentShipPool, ComponentShipScript>
{
    public ComponentShipPool()
    {
        fileName = "JSON/componentShipManagerConfig";
        listName = "componentShips";
        typeName = "componentShips";
    }

    protected override GameObject processAsset(GameObject asset, JSONClass config)
    {
        return asset;
    }


    protected override GameObject preparePrefab(GameObject prefab, JSONClass config)
    {
        return prefab;
    }

    public override GameObject _getPrefab(string name, Vector3 position, Quaternion rotation)
    {
        GameObject prefab = getPrefab(name);
        if (prefab != null)
        {
            prefab.transform.position = position;
            prefab.transform.rotation = rotation;
            return prefab;
        }
        else
        {
            if (logWarnings) Debug.LogWarning("Prefab not found:" + name);
        }

        return null;
    }
}