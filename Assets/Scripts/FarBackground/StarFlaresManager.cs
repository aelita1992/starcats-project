﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StarFlaresManager : MonoBehaviour
{
	public Transform starFlarePrefab;
	public Transform sunFlarePrefab;
	
	List <GameObject> starFlares;
	GameObject sunFlare;
	
	void Start()
	{
		if(starFlares == null)
			starFlares = new List<GameObject>();
		
		for (int i = 0; i < 5; i++) 
		{
			starFlares.Add (((Transform)GameObject.Instantiate(starFlarePrefab)).gameObject);
		}

		for (int i = 0; i < 5; i++) 
		{
			starFlares[i].transform.parent = gameObject.transform;
		}
		
		sunFlare = ((Transform)GameObject.Instantiate(sunFlarePrefab)).gameObject;

		DisableSunFlare();
		DisableStarFlares();

		PrepareFlares ();

	}

	void OnEnable()
	{
		starFlares = new List<GameObject>();
		TimeManager.Get ().timeDependantUpdate += UpdateFlarePositions;
	}

	void OnDisable()
	{
		TimeManager.Get ().timeDependantUpdate -= UpdateFlarePositions;
	}

	void UpdateFlarePositions()
	{
		foreach(GameObject starFlare in starFlares)
		{
			Vector3 position = starFlare.transform.position;
			position.y -= Time.deltaTime * 120;
			starFlare.transform.position = position;

			if(position.y < -400)
				starFlare.transform.position = GenerateRandomPosition();
		}
	}

	public void DisableSunFlare()
	{
		sunFlare.SetActive(false);
	}

	public void DisableStarFlares()
	{
		foreach (GameObject starFlareObject in starFlares) 
		{
			starFlareObject.gameObject.SetActive(false);
		}
			
	}

	public void PrepareFlares()
	{

		for(int i = 0; i < 5; i++)
		{
			Vector3 randomPosition = new Vector3(Random.Range(-480, 480),Random.Range(400 + 200* i, 600 + 200 * i),-300);

			starFlares[i].SetActive(true);
			starFlares[i].transform.position = randomPosition;
		}

	}

	public Vector3 GenerateRandomPosition()
	{
		return new Vector3(Random.Range(-480, 480),Random.Range(400, 600),-300);
	}
}
