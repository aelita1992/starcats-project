﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class shipScript2 : ShipScript{

	static int IdProvider = 0;
	int id = 0;


	bool initialized = false;

    private bool logEverything = false;
    private bool logWarnings = true;
    private bool logDamage = false;

    private float startHP = 200f;

	protected GameObject rayCastColliders = null;

    public override float StartHP
    {
        get { return startHP; }
        set { startHP = value; }
    }
    private float currentHP = 200f;

    public override float CurrentHP
    {
        get { return currentHP; }
        set { currentHP = value; }
    }

    private bool isAlive = true;

    public override bool IsAlive
    {
        get { return isAlive; }
    }

    GameObject reticle = null;
    private bool isTargeted = false;
    public override bool IsTargeted
    {
        get { return isTargeted; }
        set { if (reticle != null ) reticle.SetActive(value); isTargeted = value; }
    }


    protected WeaponManager weapons = null;
    protected TurretManager turrets = null;
    public ShieldController shield = null;

	protected bool hasShield = false;

    protected float scoreScale = 1f;

    public override float ShieldStartHP
    {
        get { return (shield == null) ? 0f : shield.StartHP; }
    }

    public override float ShieldCurrentHP
    {
        get { return (shield == null) ? 0f : shield.CurrentHP; }
    }

    string modelName = null;
    Transform handle = null;



    public override void fireAllWeapons()
    {
        if (weapons != null && this.gameObject.activeSelf)
        {
            weapons.fireAllWeapons();
        }
    }

    public override void fireWeaponsGroup(int index)
    {
        if (weapons != null && this.gameObject.activeSelf)
        {
            weapons.fireWeaponsGroup(index);
        }
    }

    public override JSONMonoBehaviour applyConfig(JSONClass config)
    {
        if (initialized)
        {
            if (logEverything) 
                Debug.Log("Shipscript reconfig...");

            return this.getFresh();
        }

        if (logEverything) listBones(transform);

		rayCastColliders = getColliders ();
		rayCastColliders.name = "Raycast Collider";
		rayCastColliders.transform.parent = this.transform;

		if( tag == "Player" )
			rayCastColliders.layer = LayerMask.NameToLayer ("Player Raycast Collider");
		else
			rayCastColliders.layer = LayerMask.NameToLayer ("Enemy Raycast Collider");

        // parametry statku

        float maxHpValue = config["maxHP"].AsFloat;
        if (maxHpValue > 0.001f)
        {
            startHP = maxHpValue;
        }
        currentHP = startHP;

        float scoreScaleValue = config["scoreScale"].AsFloat;
        if (scoreScaleValue > 0.001f)
        {
            scoreScale = scoreScaleValue;
        }


        // info potrzebne do doczepiania tarczy/broni/wiezyczek

        modelName = config["modelName"].Value;
        handle = transform.Find(modelName);
        if (logEverything) Debug.Log("modelName: " + modelName, handle);

        // tarcza

        if (loadShield(config) == false) return null;

        // doczepianie broni

        if (loadWeapons(config) == false) return null;

        // doczepianie wiezyczek

        if (loadTurrets(config) == false) return null;

        // sukces?

        ///

        GameObject reticleProto = Resources.Load<GameObject>("Prefabs/reticle") as GameObject;
        reticle = Instantiate(reticleProto) as GameObject;
        reticle.transform.parent = this.transform;
        reticle.transform.localPosition = new Vector3(0f, 15f, 6f);
        reticle.transform.localRotation = Quaternion.Euler(90f, 0f, 0f);
        reticle.transform.localScale = new Vector3(2f, 2f, 2f);
        reticle.SetActive(isTargeted);
        ///

        initialized = true;	
        // DAFUQ !?
		//this.gameObject.transform.localScale = new Vector3(3,3,3);

        return this;
    }

    private bool loadShield(JSONClass config)
    {
        string shieldPrefabName = config["shieldType"].Value;
        if (shieldPrefabName != null && shieldPrefabName != "")
        {
            if (logEverything) Debug.Log("Found shield: " + shieldPrefabName);
			hasShield = true;

            ShieldController shieldPrefab = Resources.Load<ShieldController>("Prefabs/" + shieldPrefabName) as ShieldController;
            if (shieldPrefab != null)
            {
                ShieldController shieldInstance = Instantiate(shieldPrefab, transform.position, transform.rotation) as ShieldController;
                if (shieldInstance != null)
                {
                    float shieldScale = config["shieldScale"].AsFloat;
                    Vector3 shieldScaleVector = vector3FromJSONArray0(config["shieldScale"].AsArray);

                    float shieldStrength = config["shieldStrength"].AsFloat;
                    JSONArray shieldOffset = config["shieldOffset"].AsArray;


                    shieldInstance.setParent(this.gameObject);
                    if (shieldScaleVector == Vector3.zero)
                        shieldInstance.transform.localScale = new Vector3(shieldScale, shieldScale, shieldScale);
                    else
                        shieldInstance.transform.localScale = shieldScaleVector;

                    shieldInstance.transform.localPosition += new Vector3(shieldOffset[0].AsFloat, shieldOffset[1].AsFloat, shieldOffset[2].AsFloat);
                    shieldInstance.activateShield();

                    if (shieldStrength > 0.001f)
                    {
                        shieldInstance.setStrength(shieldStrength);
                    }

                    this.shield = shieldInstance;
                }
            }
            else
            {
                if (logWarnings) Debug.LogWarning("Cant instantiate shield: " + shieldPrefabName);
            }
        }

        return true;
    }

    private bool loadWeapons(JSONClass config)
    {
        WeaponPool GunManagerInstance = WeaponPool.get();
        weapons = gameObject.AddComponent("WeaponManager") as WeaponManager;

        if (GunManagerInstance == null)
        {
            Debug.LogError("GunManager not ready during ship init!");
            return false;
        }

        foreach (JSONNode gunDef in config["gunMounts"].AsArray)
        {
            string gunName = gunDef["name"].Value;
            string bonePath = gunDef["path"].Value;
            string boneName = gunDef["bone"].Value;
            string defaultGun = gunDef["defaultGun"].Value;

            JSONArray gunRotation = gunDef["rotate"].AsArray;
            JSONArray gunScale = gunDef["scale"].AsArray;
            JSONArray groups = gunDef["groups"].AsArray;

            string searchPath = "";
            if (modelName.Length > 0) searchPath += modelName + "/";
            if (bonePath.Length > 0) searchPath += bonePath + "/";
            searchPath += boneName;

            Transform gunBone = transform.Find(searchPath);
            if (gunBone != null)
            {

                if (logEverything) Debug.Log("fund gun: " + gunName + " at bone " + searchPath, gunBone);

                if (defaultGun != null)
                {
                    GameObject gunPrefab = GunManagerInstance.getPrefab(defaultGun, gunBone.position, gunBone.rotation);

                    if (gunPrefab == null)
                    {
                        Debug.LogError("Can't find matching prefab '" + defaultGun + "' for gun " + gunName);
                        continue;
                    }

                    gunScript gunData = gunPrefab.GetComponent<gunScript>();
                    gunData.parentShip = this;

                    Transform gunAttachBone = gunData.getAttachBone();

                    if (gunRotation != null && gunRotation.Count == 3)
                    {
                        if (logEverything) Debug.Log("Rotating: " + gunRotation[0].AsFloat + " / " + gunRotation[1].AsFloat + " / " + gunRotation[2].AsFloat);
                        gunAttachBone.rotation = Quaternion.Euler(gunRotation[0].AsFloat, gunRotation[1].AsFloat, gunRotation[2].AsFloat);
                    }

                    if (gunScale != null && gunScale.Count == 3)
                    {
                        if (logEverything) Debug.Log("Scaling: " + gunScale[0].AsFloat + " / " + gunScale[1].AsFloat + " / " + gunScale[2].AsFloat);
                        gunAttachBone.localScale = new Vector3(gunScale[0].AsFloat, gunScale[1].AsFloat, gunScale[2].AsFloat);
                    }

                    if (groups != null && groups.Count > 0)
                    {
                        if (logEverything) Debug.Log("Adding weapon to groups...");
                        foreach (JSONNode gid in groups)
                        {
                            weapons.addWeaponToGroup(gunData, gid.AsInt);
                            if (logEverything) Debug.Log("groupId: " + gid.Value);
                        }
                    }

                    gunPrefab.transform.parent = gunBone;
                    
                }
                else
                {
                    if (logWarnings) Debug.LogWarning("No default gun defined for " + gunName);
                }


            }
            else
            {
                if (logWarnings) Debug.LogWarning("Gun mount missing: " + searchPath);
            }
        }

        return true;
    }

    private bool loadTurrets(JSONClass config)
    {
        //turretMounts

        foreach (JSONNode turretDef in config["turretMounts"].AsArray)
        {
            string gunName = turretDef["name"].Value;
            string bonePath = turretDef["path"].Value;
            string boneName = turretDef["bone"].Value;
            string gunsTop = turretDef["gunsTop"].Value;
            string gunsBottom = turretDef["gunsBottom"].Value;


            string searchPath = "";
            if (modelName.Length > 0) searchPath += modelName + "/";
            if (bonePath.Length > 0) searchPath += bonePath + "/";
            searchPath += boneName;

            Transform turretBone = transform.Find(searchPath);
            if (turretBone != null)
            {
                //////////////////////////// INIT TURRET CONTROLLER HERE!!!
                if (turrets == null)
                {
                    turrets = gameObject.AddComponent<TurretManager>();
                    TimeManager.Get().timeDependantUpdate += turrets.updateTarget;

                    if (this.tag == "Player")
                    {
                        turrets.targetTag = "EnemyShip";
                    }
                }
                //////////////////////////// INIT TURRET CONTROLLER HERE!!!

                GameObject prefab = Resources.Load("Prefabs/Turret1") as GameObject;
                GameObject prefabInstance = Instantiate(prefab, turretBone.transform.position, turretBone.transform.rotation) as GameObject;
                prefabInstance.transform.parent = turretBone;

                // setupTurret
                TurretController2 turret = prefabInstance.GetComponent<TurretController2>();

                turret.upperGunName = gunsTop;
                turret.lowerGunName = gunsBottom;
                //if (this.tag == "Player") turret.speed = 50f;

                turrets.registerTurret(turret);

                JSONArray gunRotation = turretDef["rotate"].AsArray;
                JSONArray gunScale = turretDef["scale"].AsArray;
                JSONArray groups = turretDef["groups"].AsArray;

                if (gunRotation != null && gunRotation.Count == 3)
                {
                    if (logEverything) Debug.Log("Rotating: " + gunRotation[0].AsFloat + " / " + gunRotation[1].AsFloat + " / " + gunRotation[2].AsFloat);
                    turretBone.localRotation = Quaternion.Euler(gunRotation[0].AsFloat, gunRotation[1].AsFloat, gunRotation[2].AsFloat);
                }

                if (gunScale != null && gunScale.Count == 3)
                {
                    if (logEverything) Debug.Log("Scaling: " + gunScale[0].AsFloat + " / " + gunScale[1].AsFloat + " / " + gunScale[2].AsFloat);
                    turretBone.localScale = new Vector3(gunScale[0].AsFloat, gunScale[1].AsFloat, gunScale[2].AsFloat);
                }

                if (groups != null && groups.Count > 0)
                {
                    if (logEverything) Debug.Log("Adding turret to groups...");
                    foreach (JSONNode gid in groups)
                    {
                        foreach (gunScript gun in turret.Weapons)
                        {
                            weapons.addWeaponToGroup(gun, gid.AsInt);
                            if (logEverything) Debug.Log("groupId: " + gid.Value);
                        }
                    }
                }
            }
            else
            {
                if (logWarnings) Debug.LogWarning("Can't find bone for turret " + gunName + "@" + searchPath);
            }
        }

        return true;
    }

    private DamageInfo projectileImpactDamage = new DamageInfo(0f, null, null, false);
	public void handleImpact( DamageInfo damage ){
        float damageHP = 1f;

        if (damage != null) {
            if (damage.DOT)
                damageHP = damage.damage * Time.deltaTime;
            else
                damageHP = damage.damage;
        }

        if (this.shield != null && this.shield.isUp())
        {
            if (logDamage) Debug.LogWarning("Ship protected by shield");
        }
        else
        {

            currentHP -= damageHP;

            if (currentHP <= 0f)
            {
                Debug.LogWarning("Ship died");
                isAlive = false;
                scuttleShip();

                if (ScoreHandle.Get() != null && gameObject.tag != "Player")
                    ScoreHandle.Get().AddScore((int)(1000f * scoreScale));
            }
            else
            {
                if (logDamage) Debug.LogWarning("Ship HP:" + currentHP);
            }

        }
        if (gameObject.tag == "Player" && GuiSliderBars.Get() != null)
            GuiSliderBars.Get().SetHitPoints(currentHP);
	}

    void OnTriggerEnter(Collider other)
    {
        if (
                (
                    (this.gameObject.tag == "Player" && (other.gameObject.tag == "EnemyProjectile" || other.gameObject.tag == "EnemyRocket"))
                || (this.gameObject.tag == "EnemyShip" && (other.gameObject.tag == "Projectile" || other.gameObject.tag == "Rocket"))
                ) && currentHP > 0f
            )
        {
            projectileScript p = other.gameObject.GetComponent<projectileScript>();
            float projectileDamage = 10f;
            if (p != null) {
                projectileDamage = p.projectileDamage;
            }

            projectileImpactDamage.damage = projectileDamage;
            handleImpact(projectileImpactDamage);

            ProjectilePool.get().returnToPool(other.gameObject);
        }


    }

    public override void kill() {
        currentHP = 0f;
        isAlive = false;
        scuttleShip();
    }

    void scuttleShip() {
        //GameObject boom = (GameObject)Resources.Load("Explosions/Explosion01b");
        GameObject boom = (GameObject)Resources.Load("Prefabs/Explosn");
        //GameObject boomBoom = (GameObject)Instantiate(boom, this.transform.position, this.transform.rotation);
        GameObject boomBoom = (GameObject)Instantiate(boom, this.transform.position, Quaternion.LookRotation(new Vector3(0f,0f,-1f)));

        boomBoom.transform.localScale = collider.bounds.size * 1.2f;

        if (shield != null)
        {
            shield.offline();
        }

        ShipPool.get().returnToPool(this.gameObject);
    }

    public void prefreshShip() {
        resetPrefab();
    }

    public override void resetPrefab()
    {
        if (logEverything) 
            Debug.Log("ShipScript2::resetPrefab()",this);

        if (shield != null)
        {
            shield.reset();
            shield.online();
            shield.activateShield();
        }
        else if (hasShield)
        {
            Debug.LogWarning("lost shield?");
        }

        currentHP = startHP;
        isAlive = true;
        IsTargeted = false;
    }

    void Awake()
	{
        //if (!initialized && logEverything) Debug.Log("Ship not ready on Awake()", this);
		//this.gameObject.transform.eulerAngles = new Vector3(270,0,0);
		//this.gameObject.transform.localScale = new Vector3(3.5f, 3.5f, 3.5f);

		if (MissileBarrageManager.Get () != null) {
			if (id != 0)
					return;

			id = ++IdProvider;
			MissileBarrageManager.Get ().AddPooledShipReference (id, this.gameObject);
		}
	}
 
	void OnEnable()
	{
		if (MissileBarrageManager.Get () != null) {
			MissileBarrageManager.Get ().AddActiveShip (id);

            if (PrefabCollectionsManager.Get() != null)
            {
                PrefabCollectionsManager.Get().AddToManager(PrefabCollectionsManager.PrefabType.ENEMY_SHIP, this.gameObject);
            }
		}
	}
	
	void OnDisable()
	{
		isTargeted = false;
		if (MissileBarrageManager.Get () != null) 
			MissileBarrageManager.Get ().RemoveActiveShip(id);
	}

	void Start () {
        //if (!initialized) Debug.LogError("Ship not ready on Start()", this);
        currentHP = startHP;
	}

	public override void IncreaseHP(int value)
	{
		currentHP += value;
        if (currentHP > startHP) currentHP = startHP;

		if(gameObject.tag == "Player")
			GuiSliderBars.Get ().SetHitPoints (currentHP);
	}

}