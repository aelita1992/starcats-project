﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager 
{
	public enum EnemyTypes{WEAK, MEDIUM, BIG};

	Stack <EnemyBehaviour> weakShips;
	Stack <EnemyBehaviour> mediumShips;
	Stack <EnemyBehaviour> bigShips;

	void Awake()
	{
		weakShips = new Stack <EnemyBehaviour>();
		mediumShips = new Stack <EnemyBehaviour>();
		bigShips = new Stack <EnemyBehaviour>();
	}

	public EnemyBehaviour GetShipScript(EnemyTypes enemyType)
	{
		EnemyBehaviour activatedShip = null;

		switch(enemyType)
		{
		case EnemyTypes.WEAK:
			activatedShip = GetAvailableShip(ref weakShips);
			break;

		case EnemyTypes.MEDIUM:
			activatedShip = GetAvailableShip(ref mediumShips);
			break;

		case EnemyTypes.BIG:
			activatedShip = GetAvailableShip(ref bigShips);
			break;

		default:
			activatedShip.enemyType = enemyType;
			break;
		}

		return activatedShip;
	}
	
	private EnemyBehaviour GetAvailableShip(ref Stack <EnemyBehaviour> shipsStorage)
	{
		if(shipsStorage.Count > 0)
			return shipsStorage.Pop();
		
		Debug.LogError("No free ships available");
		return null;
	}

	public void PutObject(EnemyBehaviour enemyBehaviour)
	{
		EnemyTypes enemyType = enemyBehaviour.enemyType;

		switch(enemyType)
		{
		case EnemyTypes.WEAK:
			weakShips.Push(enemyBehaviour);
			break;
			
		case EnemyTypes.MEDIUM:
			mediumShips.Push(enemyBehaviour);
			break;
			
		case EnemyTypes.BIG:
			bigShips.Push(enemyBehaviour);
			break;
		}
	}

}
