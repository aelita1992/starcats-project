﻿using UnityEngine;
using System.Collections;

public class ConstantMovementAI : MovementAI
{
    public ConstantMovementAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate){}

    public override void updateAcceleration()
    {
        if (!updateState()) return;
    }
    public override void updateVelocity()
    {
        if (!updateState()) return;
        Vector3 targetVector = targetPos - currentPos;

        currentVelocity.x = Mathf.Clamp(targetVector.x, MaxVelocity.x * -1f, MaxVelocity.x);
        currentVelocity.y = Mathf.Clamp(targetVector.y, MaxVelocity.y * -1f, MaxVelocity.y);
        currentVelocity.z = Mathf.Clamp(targetVector.z, MaxVelocity.z * -1f, MaxVelocity.z);
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position += currentVelocity * Time.deltaTime;
    }
}
