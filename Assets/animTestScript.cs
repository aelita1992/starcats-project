﻿using UnityEngine;
using System.Collections;

public class animTestScript : MonoBehaviour {

    Animator a;

    void Awake() {
        a = GetComponent<Animator>();
    }

	void Update () {
        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            a.Play("up");
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            a.Play("down");
        }
    }
}
