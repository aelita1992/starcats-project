using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ArrowFormation : Formation 
{

	int numberOfShipsLeft;
	int iterator;
	
	public override void SetFormation(List<GameObject> formationShips, FormationsController.Direction direction, Vector3 centerPosition, Vector2 xySpread)
	{
		numberOfShipsLeft = formationShips.Count;
		iterator = 0;
		this.xySpread = xySpread;

		switch(direction)
		{

		case FormationsController.Direction.NO_DIRECTION:

			direction = FormationsController.Direction.DOWN;
			SetCenterAndLeftWing(formationShips, centerPosition, direction);
			SetRightWing(formationShips, centerPosition, direction);
			break;

		case FormationsController.Direction.UP:
		case FormationsController.Direction.DOWN:

			SetCenterAndLeftWing(formationShips, centerPosition, direction);
			SetRightWing(formationShips, centerPosition, direction);
			break;

		case FormationsController.Direction.LEFT:
		case FormationsController.Direction.RIGHT:

			SetCenterAndTopWing(formationShips, centerPosition, direction);
			SetBottomWing(formationShips, centerPosition, direction);
			break;
		}
		

	}
	
	private void SetCenterAndLeftWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		int leftWingSize = numberOfShipsLeft/2;
		int translatedDir = TranslateDirection(direction);
		Vector3 currentCenter = centerPosition;

		formationShips[iterator++].transform.position = currentCenter;

		for(; iterator <= leftWingSize; iterator++)
		{
			currentCenter.x -= xySpread.x;
			currentCenter.y += xySpread.y * translatedDir;
			formationShips[iterator].transform.position = currentCenter;
		}
		
		numberOfShipsLeft -= (leftWingSize + 1);
		
	}
	
	private void SetRightWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		int translatedDir = TranslateDirection(direction);
		Vector3 currentCenter = centerPosition;

		for(int i = 0; i < numberOfShipsLeft; i++)
		{
			currentCenter.x += xySpread.x;
			currentCenter.y += xySpread.y * translatedDir;
			formationShips[iterator].transform.position = currentCenter;

			iterator++;
		}
	}

	private void SetCenterAndTopWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		int topWingSize = numberOfShipsLeft/2;
		int translatedDir = TranslateDirection(direction);
		Vector3 currentCenter = centerPosition;

		formationShips[iterator++].transform.position = currentCenter;
		
		for(; iterator <= topWingSize; iterator++)
		{
			currentCenter.x += xySpread.x * translatedDir;
			currentCenter.y += xySpread.y;
			formationShips[iterator].transform.position = currentCenter;
		}
		
		numberOfShipsLeft -= (topWingSize + 1);
	}

	private void SetBottomWing(List<GameObject> formationShips, Vector3 centerPosition, FormationsController.Direction direction)
	{
		int translatedDir = TranslateDirection(direction);
		Vector3 currentCenter = centerPosition;
		
		for(int i = 0; i < numberOfShipsLeft; i++)
		{
			currentCenter.x += xySpread.x * translatedDir;
			currentCenter.y -= xySpread.y;
			formationShips[iterator].transform.position = currentCenter;
			
			iterator++;
		}
	}

	private int TranslateDirection (FormationsController.Direction direction)
	{
		switch(direction)
		{
			case FormationsController.Direction.UP:
			case FormationsController.Direction.RIGHT:
				return -1;

			case FormationsController.Direction.DOWN:
			case FormationsController.Direction.LEFT:
				return 1;
		}

		return 0;

	}
}
