﻿using UnityEngine;
using System.Collections;

public class AcceleratedMovementAI2 : MovementAI
{

    float slowDistance = 0f;
    float stopDistance = 0f;
    float reactDistance = 0f;
    public AcceleratedMovementAI2(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate)
    {

        slowDistance = (maxVelocity.magnitude / maxAccel.magnitude)*20;
        stopDistance = slowDistance / 5f;
        reactDistance = slowDistance / 2f;
    }


    Vector3 targetVector = Vector3.zero;
    float distance = 0;
//    float timeToTarget = 0;
//    float timeToStop = 0;
//    float distanceToStop = 0;

    private void updateStats()
    {
        targetVector = targetPos - currentPos;

        distance = targetVector.magnitude;
//        timeToTarget = (currentVelocity.magnitude == 0) ? float.MaxValue : distance / currentVelocity.magnitude;
//        timeToStop = currentVelocity.magnitude / (MaxAccel.magnitude / 2f);
//        distanceToStop = timeToStop / 2;
    }

    public override void updateAcceleration()
    {
        if (!updateState()) return;

        updateStats();
        Vector3 intendedChange = Vector3.zero;
        if (distance <= reactDistance && currentVelocity.magnitude <= 0.1f) return;


        if (distance > slowDistance)
        {
            intendedChange = Vector3.ClampMagnitude(targetVector, distance);
        }
        else if (distance > stopDistance)
        {
            //Debug.Log("Slowwing down...");
            float distanceRatio = distance / slowDistance;
            intendedChange = (currentVelocity * (-1f)) + (distanceRatio * targetVector.normalized);

        }
        else if (currentVelocity.magnitude >= 0.1f)
        {
            //Debug.Log("Stopping...");
            intendedChange = currentVelocity * (-1f);
        }

        //Vector3 actualChange = intendedChange; 
        Vector3 actualChange = Vector3.ClampMagnitude(intendedChange, AccelRate);

        currentAccel = Vector3.ClampMagnitude(currentAccel + actualChange, MaxAccel.magnitude);
    }

    public override void updateVelocity()
    {
        if (!updateState()) return;
        updateStats();

        Vector3 intendedVelocity = currentVelocity + currentAccel;

        if (distance <= stopDistance && currentVelocity.magnitude <= 0.1f)
        {
            //Debug.Log("Stopped");
            intendedVelocity = Vector3.zero;
        }
        else if (distance <= slowDistance) {
            intendedVelocity *= 0.9f;
        }
        
        currentVelocity.x = Mathf.Clamp(intendedVelocity.x, MaxVelocity.x * -1f, MaxVelocity.x);
        currentVelocity.y = Mathf.Clamp(intendedVelocity.y, MaxVelocity.y * -1f, MaxVelocity.y);
        currentVelocity.z = Mathf.Clamp(intendedVelocity.z, MaxVelocity.z * -1f, MaxVelocity.z);
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position += currentVelocity;
    }

}