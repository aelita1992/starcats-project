﻿using UnityEngine;
using System.Collections;

public class DamageInfo{

    public gunScript weapon;
    public ShipScript source;
    public float damage;
    public bool DOT = true;
    public float hullDamageRatio;
    public float shieldDamageRatio;

    public float getDamage() {
        return damage;
    }

    public DamageInfo(float damage, gunScript gunScript = null, ShipScript shipScript = null, bool isDOT = true, float hullDamageRatio = 1f, float shieldDamageRatio = 1f)
    {
        this.weapon = gunScript;
        this.source = shipScript;
        this.damage = damage;
        this.DOT = isDOT;
        this.hullDamageRatio = hullDamageRatio;
        this.shieldDamageRatio = shieldDamageRatio;
    }
}
