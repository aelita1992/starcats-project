﻿using UnityEngine;
using System.Collections;

public abstract class MovementAI
{
    protected Vector3 maxVelocity = Vector3.one;

    protected Vector3 MaxVelocity
    {
        get { return maxVelocity; }
        set { maxVelocity = value; }
    }
    protected Vector3 maxAccel = Vector3.one;

    protected Vector3 MaxAccel
    {
        get { return maxAccel; }
        set { maxAccel = value; }
    }
    protected Vector3 maxDecel = Vector3.one;

    protected Vector3 MaxDecel
    {
        get { return maxDecel; }
        set { maxDecel = value; }
    }
    protected float accelRate = 1f;

    protected float AccelRate
    {
        get { return accelRate; }
        set { accelRate = value; }
    }

    protected MovementAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
    {
        this.accelRate = accelRate;
        this.maxVelocity = maxVelocity;
        this.maxAccel = maxAccel;
        this.maxDecel = maxDecel;
    }

    public MovementAI()
        : this(Vector3.one, Vector3.one, Vector3.one, 0.1f)
    {

    }

    protected Vector3 currentVelocity = Vector3.zero;
    protected Vector3 currentAccel = Vector3.zero;

    protected Transform self = null;

    public Transform Self
    {
        get { return self; }
    }

    protected Transform target = null;

    public Transform Target
    {
        get { return target; }
    }

    protected Vector3 currentPos = Vector3.zero;
    protected Vector3 targetPos = Vector3.one;

    public void Deattach() {
        self = null;
        target = null;

        currentPos = Vector3.zero;
        currentAccel = Vector3.zero;

        targetPos = Vector3.one;        
    }

    public void setSelf(Transform s)
    {
        if (s == null)
        {
            Debug.LogError("self is null [ to disconnect AI from objects use Deattach() ]");
            return;
        }

        self = s;
        currentPos = self.position;
    }

    public void setTarget(Transform t)
    {
        if (t == null)
        {
            Debug.LogError("target is null [ to disconnect AI from objects use Deattach() ]");
            return;
        }

        target = t;
        targetPos = target.position;
    }

    public Vector3 getAcceleration()
    {
        return currentAccel;
    }
    public Vector3 getVelocity()
    {
        return currentVelocity;
    }
    public void setVelocity(Vector3 v)
    {
        currentVelocity = v;
    }

    protected bool updateState()
    {
        if (self == null || target == null) return false;
        currentPos = self.position;
        targetPos = target.position;
        return true;
    }

    public abstract void updateAcceleration();
    public abstract void updateVelocity();
    public abstract void applyDeltaToSelf();
}
