﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class ComponentMountPoint : MountPoint{
    string componentName = "";
    ShipComponent attachedComponent = null;
	public ComponentMountPoint(Transform mountLocation,Vector3 mountScale, Vector3 mountRotation) 
		: base(mountLocation,mountScale, mountRotation)
	{
		
	}

    public override void populate(Object mountedItem, Vector3 scale, Vector3 rotation)
    {
        if (mountedItem.GetType() == typeof(ShipComponent))
        {
            if (attachedComponent != null)
                cleanUp();

            attachedComponent = (ShipComponent)mountedItem;

            attachedComponent.ParentShip = ParentComponent.ParentShip;
            attachedComponent.ParentComponent = ParentComponent;
            attachedComponent.ParentMountPoint = this;

            setupTransformAligned(attachedComponent.transform, attachedComponent.BaseBone);

            ParentComponent.registerComponent(attachedComponent);

            if (scale != Vector3.one && scale != Vector3.zero) attachedComponent.transform.localScale += scale;
            if (rotation != Vector3.zero) attachedComponent.transform.Rotate(rotation);
        }
    }
	public override void cleanUp (){
        if (attachedComponent != null)
        {
            ParentComponent.unregisterComponent(attachedComponent);
            ComponentPool.get().returnToPool(attachedComponent.gameObject);
            attachedComponent = null;
        }
    }

    public override void setup(JSONClass mountInfo) {
        componentName = mountInfo["name"].Value;

        ShipComponent newComponent = ComponentPool.get().getScript(componentName);
        if (newComponent != null)
        {
            populate(newComponent,Vector3.one, Vector3.zero);

            JSONClass subComponents = mountInfo["mounts"].AsObject;
            if (subComponents != null && subComponents.Count > 0)
                attachedComponent.setupMounts(subComponents);
        }
    }
}