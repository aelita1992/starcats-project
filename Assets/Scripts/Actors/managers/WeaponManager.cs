﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour {
    List<gunScript> weapons = new List<gunScript>();
    List<gunScript>[] weaponGroups = { new List<gunScript>(), new List<gunScript>(), new List<gunScript>() };

    //private bool logEverything = false;
    //private bool logWarnings = true;

    public int weaponsRegistered() {
        return weapons.Count;
    }

    public void addWeaponToGroup(gunScript weapon, int index) {
        if (weaponGroups.Length <= index || index < 0)
        {
            Debug.LogError("Invalid weapons group requested (" + index + ")", this);
            return;
        }

        weapons.Add(weapon);
        weaponGroups[index].Add(weapon);
    }

    public void removeWeapon(gunScript weapon) {
        weapons.Remove(weapon);
        foreach (List<gunScript> weaponGroup in weaponGroups)
        {
            weaponGroup.Remove(weapon);
        }
    }

    public void fireAllWeapons()
    {
        fireWeaponsList(weapons);
    }

    public void fireWeaponsGroup(int index)
    {
        if (weaponGroups.Length <= index || index < 0) {
            Debug.LogError("Invalid weapons group requested ("+index+")",this);
            return; 
        }

        fireWeaponsList(weaponGroups[index]);
    }

    protected void fireWeaponsList(List<gunScript> weaponsList)
    {
		foreach (gunScript weapon in weaponsList)
		{
			if( weapon.canFire() ){
            	weapon.fireIfReady();
			}
		}
    }
}
