﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Scene
{

    private List<TimelineEvent> events = null;
    private int startedEventIndex = 0;

    private float startTime = 0f;
    private float timeSinceStart = 0f;
    private string name;

    private bool logEverything = false;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }


    private bool startedLastEvent = false;

    public bool StartedLastEvent
    {
        get { return startedLastEvent; }
        set { startedLastEvent = value; }
    }

    private bool finishedLastEvent = false;

    public bool FinishedLastEvent
    {
        get { return finishedLastEvent; }
        set { finishedLastEvent = value; }
    }

    private float minTime = 0f;
    private float maxTime = 0f;
    private bool killShips = true;

    public Scene(JSONClass sceneData)
    {
        name = sceneData["name"].Value;
        minTime = sceneData["minTime"].AsFloat;
        maxTime = sceneData["maxTime"].AsFloat;
        killShips = sceneData["killShipsOnMaxTime"].AsBool;

        if (logEverything)
            Debug.Log("Created scene " + name +
                        " duration " + minTime + " - " + maxTime +
                        " kill on end " + killShips);

        JSONArray eventsList = sceneData["events"].AsArray;
        if (eventsList != null && eventsList.Count > 0)
        {

            events = new List<TimelineEvent>();

            foreach (JSONClass e in eventsList)
            {
                if (e == null)
                {
                    Debug.LogWarning("Scene " + name + " - error parsing event data");
                    continue;
                }

                TimelineEvent ev = TimelineEvent.parseEventJSON(e);
                events.Add(ev);
            }
        }
        else
        {
            Debug.LogWarning("Scene " + name + " - error parsing event list or event list empty");
        }
    }

    public void start(float time)
    {
        startTime = time;
        timeSinceStart = 0f;
        if (logEverything)
            Debug.Log("Scene " + name + " - started @ " + timeSinceStart);
    }

    public void advanceTime(float currentTime)
    {
        timeSinceStart = currentTime - startTime;

        startEvents();
        updateEvents();

        if (startedLastEvent && finishedLastEvent)
        {
            if (logEverything) Debug.Log("Scene " + name + " - finished @ " + timeSinceStart);
        }
        else if (timeSinceStart >= maxTime)
        {
            if (logEverything) Debug.Log("Forcing scene end: " + name + " @ " + timeSinceStart);
            foreach (TimelineEvent e in events)
            {
                if (e.Started && e.Running)
                {
                    e.forceEnd();
                }
            }
        }
    }

    public void forceEnd(bool rewind = true)
    {
        for (int i = 0; i <= startedEventIndex; i++)
        {
            TimelineEvent e = events[i];
            if (e.Started)
            {
                if (e.Running) e.forceEnd();
                e.reset();
            }
        }

        if (rewind)
        {
            StartedLastEvent = false;
            FinishedLastEvent = false;
            startedEventIndex = 0;
            //startTime = 0f;
            //timeSinceStart = 0f;

            //Debug.LogWarning("Rewind scene "+name);
        }
        else
        {
            StartedLastEvent = true;
            FinishedLastEvent = true;
        }
    }

    private void startEvents()
    {
        for (int i = startedEventIndex; i < events.Count; i++)
        {
            if (events[i].StartTime > timeSinceStart)
            {
                //Debug.LogWarning("Scene " + name + " - waiting for next event to start (" + i + "@" + events[i].StartTime + ", timeSinceStart=" + timeSinceStart + " )");
                return;
            }
            else if (events[i].Started == true)
            {
                //Debug.LogWarning("Scene " + name + " - next event already running...");
            }
            else if (events[i].Started == false)
            {
                startedEventIndex = i;
                events[i].runEvent(timeSinceStart);
                if (logEverything) Debug.Log("Scene " + name + " -  events started: " + startedEventIndex + " / " + events.Count + " (" + name + " @ " + timeSinceStart + ")");
            }
        }

        startedLastEvent = true;
    }


    private void updateEvents()
    {
        int finishedEventsCount = 0;

        for (int i = 0; i <= startedEventIndex; i++)
        {
            if (events[i].Started == true && events[i].Running == false)
            {
                finishedEventsCount++;

                if (finishedEventsCount == events.Count)
                {
                    finishedLastEvent = true;
                }

            }
            else if (events[i].Running == true)
            {
                events[i].updateEvent(timeSinceStart);
            }
        }
    }

    public bool isDone()
    {
        return StartedLastEvent && FinishedLastEvent;
    }
}
