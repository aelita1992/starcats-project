﻿using UnityEngine;
using System.Collections;

public abstract class ShipScript : JSONMonoBehaviour {

//    public abstract JSONMonoBehaviour applyConfig(SimpleJSON.JSONClass config);

    public abstract void fireAllWeapons();
    public abstract void fireWeaponsGroup(int index);
    public abstract void kill();

    public abstract bool IsAlive { get; }
    public abstract float CurrentHP { get; set; }
    public abstract float StartHP { get; set; }
    public abstract float ShieldCurrentHP { get; }
    public abstract float ShieldStartHP { get; }
    public abstract bool IsTargeted { get; set; }

    public abstract void IncreaseHP(int p);
}
