﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Scenario {
    public delegate void ChapterStarted(Chapter c);
    public delegate void LevelStarted(Level l);
    public delegate void StageStarted(Stage s);

    public event ChapterStarted nextChapter;
    public event LevelStarted nextLevel;
    public event StageStarted nextStage;

    private List<Chapter> chapters = null;
    private Chapter currentChapter = null;
    private int chapterIndex = 0;

    float startTime = 0f;
    float elapsedTime = 0f;

    private string name = null;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    bool isDone = false;

    public bool IsDone
    {
        get { return isDone; }
        set { isDone = value; }
    }

    public static string scenarioPath = "JSON/";
    public static Scenario fromFile(string fileName)
    {
        TextAsset scenatioJSON = Resources.Load(scenarioPath + fileName) as TextAsset;

        if (scenatioJSON == null)
        {
            Debug.LogError("Error loading " + fileName);
            return null;
        }

        var scenatioData = JSON.Parse(scenatioJSON.text);

        if (scenatioData == null)
        {
            Debug.LogError("Error parsing " + fileName);
            return null;
        }

        return new Scenario(scenatioData.AsObject);
    }

    void advanceLevel(Level l)
    {
        nextLevel(l);
    }

    void advanceStage(Stage s)
    {
        nextStage(s);
    }

    public Scenario(JSONClass scenatioData)
    {
        name = scenatioData["name"].Value;

        JSONArray chapterList = scenatioData["chapters"].AsArray;
        if (chapterList != null && chapterList.Count > 0)
        {

            chapters = new List<Chapter>();

            foreach (JSONClass c in chapterList)
            {
                if (c == null)
                {
                    Debug.LogError("error loading chapter");
                    continue;
                }

                Chapter ch = new Chapter(c);
                ch.nextLevel += advanceLevel;
                ch.nextStage += advanceStage;
                chapters.Add(ch);
            }
        }
        else
        {
            Debug.LogError("error loading chapterList");
        }

    }

    public void start(float time)
    {
        startTime = time;
        elapsedTime = 0f;
        chapterIndex = 0;

        startChapter(chapterIndex);
    }

    private void startChapter(int index)
    {
        currentChapter = chapters[index];
        if (currentChapter == null)
        {
            Debug.LogError("Chapter not found: " + index);
        }

        Debug.LogWarning("Starting Chapter " + name + "::" + chapterIndex + " (" + currentChapter.Name + ")");
        if (nextChapter != null)
        {
            nextChapter(currentChapter);
        }
        currentChapter.start(elapsedTime);
    }

    public void restartLevel()
    {
        if (currentChapter != null)
        {
            currentChapter.restartLevel();
        }
    }

    public void update(float time)
    {
        elapsedTime = time - startTime;

        //Debug.Log("Scenario update @" + time + " [elapsedTime = " + elapsedTime + "] ");

        if (currentChapter == null || currentChapter.IsDone)
        {
            if (chapterIndex >= chapters.Count)
            {
                isDone = true;
                return;
            }
            startChapter(chapterIndex);
            chapterIndex++;
        }

        currentChapter.update(elapsedTime);
    }

}
