﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

//[ExecuteInEditMode]
public abstract class GameObjectPool
{
    protected static GameObject GameObjectPoolContainer = null;
    protected static bool debugAssetCount = true;
    protected static bool logEverything = false;

    protected static void initGameObjectPoolContainer(){
        if (GameObjectPoolContainer == null)
        {
            GameObjectPoolContainer = new GameObject();
            GameObjectPoolContainer.name = "GameObjectPools";
            GameObjectPoolContainer.transform.position = Vector3.zero;
            GameObjectPoolContainer.transform.localScale = Vector3.one;
            GameObjectPoolContainer.transform.rotation = Quaternion.identity;
            GameObjectPoolContainer.SetActive(true);
        }
    }
}

//[ExecuteInEditMode]
public class GameObjectPool<PooledType> : GameObjectPool
    where PooledType : JSONMonoBehaviour
{
    //[ExecuteInEditMode]
    public class pooledData {
        public PooledType script;
        public JSONMonoBehaviour config;
        public GameObject item;

        public pooledData(PooledType script, JSONMonoBehaviour prefabConfig, GameObject prefab)
        {
            this.script = script;
            this.config = prefabConfig;
            this.item = prefab;
        }
    }

	private readonly Object poolLock = new Object();

    private JSONClass configObject = null;
    private GameObject prefabPrototype = null;
    private Stack<pooledData> usedPrefabs = new Stack<pooledData>();

    protected static GameObject poolContainer = null;
    protected static GameObject activePoolContainer = null;

    private string assetName;
    private string typeName;
    public GameObjectPool(JSONClass _configObject, GameObject _prefabPrototype, string assetName, string typeName)
    {
        configObject = _configObject;
        prefabPrototype = _prefabPrototype;

        this.assetName = assetName;
        this.typeName = typeName;

        if (GameObjectPoolContainer == null) initGameObjectPoolContainer();

        if (poolContainer == null)
        {
            poolContainer = new GameObject();
            poolContainer.name = typeName + "-inactive";
            poolContainer.transform.position = Vector3.zero;
            poolContainer.transform.localScale = Vector3.one;
            poolContainer.transform.rotation = Quaternion.identity;
            poolContainer.transform.parent = GameObjectPoolContainer.transform;
            poolContainer.SetActive(false);
        }

        if (activePoolContainer == null)
        {
            activePoolContainer = new GameObject();
            activePoolContainer.name = typeName+"-active";
            activePoolContainer.transform.position = Vector3.zero;
            activePoolContainer.transform.localScale = Vector3.one;
            activePoolContainer.transform.rotation = Quaternion.identity;
            activePoolContainer.transform.parent = GameObjectPoolContainer.transform;
            activePoolContainer.SetActive(true);
        }
    }

    public GameObject getNewPrefab()
    {
        GameObject prefab = (GameObject)UnityEngine.Object.Instantiate(PrefabPrototype);
        JSONMonoBehaviour prefabConfig = prefab.GetComponent<JSONMonoBehaviour>();
        if (prefabConfig != null) prefabConfig.parentPool = this;
        if (prefab == null || prefabConfig == null)
        {
            Debug.LogError("Error creating prefab", PrefabPrototype);
        }
        return prefab;
    }

    public void preWarm(int count) {
        if (logEverything) Debug.Log("Pool prewarm (" + count + ")");

        for (int i = 0; i < count; i++)
            addToPool( getNewPrefab() );

        if (logEverything) Debug.Log("Pool prewarm finished!");
    }

	public void verify(){
		int c = usedPrefabs.Count - 1;
        if (logEverything) Debug.Log("Pool verify (" + c + ")");

		Stack<pooledData> dump = new  Stack<pooledData> (c);

		for(int n = 0; n < c; n++) {

			pooledData d = usedPrefabs.Pop();

			pooledData h1 = (usedPrefabs.Count > 0) ? usedPrefabs.Peek() : null;
			pooledData h2 = (dump.Count > 0) ? dump.Peek() : null;

			if( (h1==null || h2==null || (h1.item != h2.item)) 
			   	&& 	(h1==null || (h1.item != d.item) )
			   	&& 	(h2==null || (h2.item != d.item) ) )
			{
				dump.Push(d);
			} else {
				Debug.LogWarning("Duplicate: " + h1.item.name);
			}
		}

		c = dump.Count;

		for (int n = 0; n < c; n++) {
			usedPrefabs.Push(dump.Pop());
		}

        if (logEverything) Debug.Log("Pool verified (" + c + ")");
	}

    public void addToPool(GameObject prefab)
    {
        JSONMonoBehaviour prefabConfig = prefab.GetComponent<JSONMonoBehaviour>();
        PooledType script = prefab.GetComponent<PooledType>();
        if (prefabConfig != null && script != null)
        {
            prefab.SetActive(false);

            if (prefab.transform != null)
            {
                
                if (debugAssetCount) {
                    if (poolContainer != null)
                    {
                        prefab.transform.parent = poolContainer.transform;
                        updateAssetCount();
                    }
                }

                prefab.transform.position = Vector3.zero;
                prefab.transform.rotation = Quaternion.identity;
                prefab.transform.localScale = Vector3.one;
            }

            if (prefab.rigidbody != null && !prefab.rigidbody.isKinematic)
            {
                prefab.rigidbody.velocity = Vector3.zero;
                prefab.rigidbody.angularVelocity = Vector3.zero;
                prefab.rigidbody.Sleep();
            }

			if( prefabConfig.pooled == false ){
				prefabConfig.pooled = true;
                prefabConfig.cleanUp();
				usedPrefabs.Push(new pooledData(script, prefabConfig, prefab));
			}
        }
        else {
            UnityEngine.GameObject.Destroy(prefab);
        }
    }

    public JSONClass ConfigObject
    {
        get { return configObject; }
        set { configObject = value; }
    }

    public GameObject PrefabPrototype
    {
        get { return prefabPrototype; }
        set { prefabPrototype = value; }
    }

    private void updateAssetCount()
    {
        poolContainer.name = typeName + "-inactive [" + poolContainer.gameObject.transform.childCount + "]";
        activePoolContainer.name = typeName + "-active [" + activePoolContainer.gameObject.transform.childCount + "]";    
    }

    private void activatePrefab(pooledData d) {
        JSONMonoBehaviour prefabInstance = d.config;
        
        if (debugAssetCount) {
            if (poolContainer != null)
            {
                prefabInstance.transform.parent = activePoolContainer.transform;
                updateAssetCount();
            }
        }

        if (prefabInstance.rigidbody != null) prefabInstance.rigidbody.WakeUp();
        prefabInstance.getFresh();
		prefabInstance.pooled = false;
    }

    private pooledData getNext() {
		lock (poolLock) {
			if (usedPrefabs.Count == 0)
				preWarm (5);

			if (usedPrefabs.Count > 0) {
					//Debug.Log("Pool size: "+usedPrefabs.Count);

					pooledData d = usedPrefabs.Pop ();
                    if (d == null || d.item == null || d.config == null || d.script == null)
                    {
                        Debug.LogError("Null prefab in pool!");
                        return null;
                    }

					//Debug.Log ("Popped off pool: " + d.item.name);
					//Debug.Log ("Pool size: " + usedPrefabs.Count);

					activatePrefab (d);
					return d;
			} else {
					Debug.LogError ("Failed to prewarm pool: " + typeName + " / " + assetName);
                    return null;
			}    
		}
    }

    public GameObject NextPrefab
    {
        get
        {
            pooledData n = getNext();
            if (n != null)
            {
                return n.item;
            }
            else
            {
                return null;
            }
        }
    }
    public PooledType NextScript
    {
        get
        {
            pooledData n = getNext();
            if (n != null)
            {
                return n.script;
            }
            else {
                return null;
            }
        }
    }

    public pooledData NextItem
    {
        get
        {
            pooledData n = getNext();
            return n;
        }
    }


}