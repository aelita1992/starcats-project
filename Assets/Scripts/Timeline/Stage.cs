﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class Stage
{


    public Stage(JSONClass stageData)
    {
        name = stageData["name"].Value;
        nextStage = stageData["nextStage"].Value;

        JSONArray sceneList = stageData["scenes"].AsArray;
        if (sceneList != null && sceneList.Count > 0)
        {

            scenes = new List<Scene>();

            foreach (JSONClass s in sceneList)
            {
                if (s == null)
                {
                    Debug.LogWarning("Error reading scene in stage " + name);
                    continue;
                }

                Scene sc = new Scene(s);
                scenes.Add(sc);
            }
        }
        else
        {
            Debug.LogWarning("Scene list null or empty in stage " + name);
        }

    }

    public void start(float time)
    {
        startTime = time;
        sceneIndex = 0;
        Debug.LogWarning("Stage " + name + " start @" + time);
        
        if (currentScene != null) {
            currentScene.forceEnd(true);
            currentScene = null;
        }
    }

    public void update(float time)
    {
        elapsedTime = time - startTime;

        if (currentScene == null || currentScene.isDone())
        {
            if (sceneIndex >= scenes.Count)
            {
                isDone = true;
                return;
            }

            currentScene = scenes[sceneIndex];
            currentScene.start(elapsedTime);
            Debug.LogWarning("Stage " + name + " switched to scene: " + currentScene.Name + " @" + elapsedTime);
            sceneIndex++;
        }

        currentScene.advanceTime(elapsedTime);
    }

    public void forceEnd( bool rewind = true )
    {
        foreach (Scene s in scenes) {
            s.forceEnd(rewind);
        }

        if (rewind) { 
            startTime = 0f;
            elapsedTime = 0f;
            sceneIndex = 0;
            isDone = false;
            currentScene = null;
        }
    }

    float startTime = 0f;
    float elapsedTime = 0f;
    private bool isDone = false;

    public bool IsDone
    {
        get { return isDone; }
        set { isDone = value; }
    }

    private string name = null;

    public string Name
    {
        get { return name; }
        set { name = value; }
    }
    private List<Scene> scenes = null;
    private Scene currentScene = null;
    private int sceneIndex = 0;

    //private List<Stage> nextStageList = null;
    private string nextStage = null;

    public string NextStage
    {
        get { return nextStage; }
        set { nextStage = value; }
    }

}
