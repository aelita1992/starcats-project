﻿using UnityEngine;
using System.Collections;

public class AcceleratedMovementAI : MovementAI
{

    float slowDistance = 0f;
    float stopDistance = 0f;
    public AcceleratedMovementAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate)
    {

        slowDistance = maxVelocity.magnitude * 5f;
        stopDistance = slowDistance / 10f;
    }


    Vector3 targetVector = Vector3.zero;
    float distance = 0;
    //    float timeToTarget = 0;
    //    float timeToStop = 0;
    //    float distanceToStop = 0;

    private void updateStats()
    {
        targetVector = targetPos - currentPos;

        distance = targetVector.magnitude;
        //        timeToTarget = (currentVelocity.magnitude == 0) ? float.MaxValue : distance / currentVelocity.magnitude;
        //        timeToStop = currentVelocity.magnitude / (MaxAccel.magnitude / 2f);
        //        distanceToStop = timeToStop / 2;    
    }

    public override void updateAcceleration()
    {
        if (!updateState()) return;

        updateStats();
        Vector3 intendedChange = Vector3.zero;

        if (distance > slowDistance)
        {
            intendedChange = targetVector;
        }
        else
        {
            float distanceRatio = distance / slowDistance;
            if (currentVelocity.magnitude >= maxVelocity.magnitude / 5f)
            {
                intendedChange = (currentVelocity * (-1f * distanceRatio)) + (distanceRatio * targetVector.normalized);
            }
            else
            {
                intendedChange = Vector3.zero;
            }
        }

        currentAccel = Vector3.ClampMagnitude(intendedChange, MaxAccel.magnitude);
    }

    public override void updateVelocity()
    {
        if (!updateState()) return;
        updateStats();

        Vector3 intendedVelocity = currentVelocity + currentAccel;

        if (distance <= stopDistance)
        {
            Debug.Log("Stopping...");
            intendedVelocity = Vector3.zero;
        }

        currentVelocity.x = Mathf.Clamp(intendedVelocity.x, MaxVelocity.x * -1f, MaxVelocity.x);
        currentVelocity.y = Mathf.Clamp(intendedVelocity.y, MaxVelocity.y * -1f, MaxVelocity.y);
        currentVelocity.z = Mathf.Clamp(intendedVelocity.z, MaxVelocity.z * -1f, MaxVelocity.z);
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position += currentVelocity;
    }

}
