﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FormationsController 
{
	public enum FormationEnum {LINE, ARROW, X, TRIFORCE}
	public enum Direction{NO_DIRECTION, UP, DOWN, LEFT, RIGHT};
	Dictionary <FormationEnum, Formation> formations;

	private static FormationsController singleton;

	public static FormationsController Get()
	{
		if(singleton == null)
			singleton = new FormationsController();

		return singleton;
	}

	public FormationsController()
	{
		formations = new Dictionary <FormationEnum, Formation>();
		formations.Add (FormationEnum.TRIFORCE, new TriforceFormation());
		formations.Add (FormationEnum.LINE, new LineFormation());
		formations.Add (FormationEnum.X, new XFormation());
		formations.Add (FormationEnum.ARROW, new ArrowFormation());
	}

	public void PrepareFormation(FormationEnum formation, List<GameObject> formationShips, Direction direction, Vector3 centerPosition, Vector2 xySpread)
	{
		formations[formation].SetFormation(formationShips, direction, centerPosition, xySpread);
	}

	public FormationEnum TranslateFormationString(string formationString)
	{
		formationString = formationString.ToLower();

		switch(formationString)
		{
		case "line":		return FormationEnum.LINE;
		case "arrow":		return FormationEnum.ARROW;
		case "x":			return FormationEnum.X;
		case "triforce":	return FormationEnum.TRIFORCE;
		}

		return FormationEnum.LINE;
	}

	public Direction TranslateDirectionString(string directionString)
	{
		directionString = directionString.ToLower();
		
		switch(directionString)
		{
		case "no_direction":	return Direction.NO_DIRECTION;
		case "up":				return Direction.UP;
		case "down":			return Direction.DOWN;
		case "left":			return Direction.LEFT;
		case "right":			return Direction.RIGHT;
		}

		return Direction.DOWN;
	}
	
}
