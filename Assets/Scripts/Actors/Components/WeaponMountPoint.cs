﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class WeaponMountPoint : MountPoint{
	public WeaponMountPoint(Transform mountLocation,Vector3 mountScale, Vector3 mountRotation) 
	: base(mountLocation,mountScale, mountRotation)
	{

	}

    gunScript attachedGun = null;
    string gunName = "";

    public override void populate(Object mountedItem, Vector3 scale, Vector3 rotation)
    {
        if ( mountedItem.GetType().IsSubclassOf( typeof(gunScript) ) )
        {
            if (attachedGun != null)
                cleanUp();

            // Debug.LogWarning("Attaching gun: " + gunName);
            attachedGun = (gunScript)mountedItem;

            Transform gunAttachPoint = attachedGun.getAttachBone();
            if (gunAttachPoint == null) gunAttachPoint = attachedGun.transform;
            setupTransformAligned(attachedGun.transform, gunAttachPoint);

            if (scale != Vector3.one) attachedGun.transform.localScale += scale;
            if (rotation != Vector3.zero) attachedGun.transform.Rotate(rotation);

            ParentComponent.registerWeapon(attachedGun, 0);
        }
        else {
            Debug.LogWarning("Invalid object type for mount point! " + mountedItem.GetType());
        }
    }

	public override void cleanUp (){
        if (attachedGun != null)
        {
            ParentComponent.unregisterWeapon(attachedGun);
            WeaponPool.get().returnToPool(attachedGun.gameObject);
            attachedGun = null;
        }
    }
    public override void setup(JSONClass mountInfo)
    {
        gunName = mountInfo["name"].Value;

        gunScript newGun = WeaponPool.get().getScript(gunName);
        if (newGun != null)
        {
            populate(newGun, Vector3.one, Vector3.zero);
        }
        else {
            Debug.LogWarning("Gun not found: " + gunName);
        }
    }
}
