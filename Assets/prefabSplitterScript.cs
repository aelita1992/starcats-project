﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[ExecuteInEditMode]
public class prefabSplitterScript : MonoBehaviour {

    public bool alreadyRun = false;

    class mountedComponent {
        public string location;
        public string parent;
        public string mountPoint;
        public string prefabName;

        public mountedComponent(string location, string parent, string mountPoint, string prefabName)
        {
            this.location = location;
            this.parent = parent;
            this.mountPoint = mountPoint;
            this.prefabName = prefabName;
        }
    }

    Dictionary<string, mountedComponent> mountPoints = null;
	// Update is called once per frame
	void Update () {
        if (!alreadyRun) {
            mountPoints = new Dictionary<string, mountedComponent>();
            prefabs = new List<GameObject>();

            splitGameobjects(transform);

            string hull = "";
            string mounts = "";
            foreach (KeyValuePair<string, mountedComponent> mount in mountPoints)
            {
                /*
		            "innerLeftWing":{
				            "parent":"centerHull",
				            "mountPoint":"hullLeft",
				            "componentName":"sideHull"
		            },
                 */
                string mountPoint = mount.Key;
                mountedComponent mounted = mount.Value;

                if (mounted != null) {

                    if (mounted.prefabName != null)
                        hull +=
                           ("\t\"" + mounted.location + "\" : {\n\t\t\"parent\":\"" + mounted.parent + "\", \n\t\t\"mountPoint\":\"" + mounted.mountPoint + "\", \n\t\t\"componentName\":\"" + mounted.prefabName + "\" },\n");
                    //else
                   // {
                        //string parentSection = mounted.parent.Replace
                   //     mounts +=
                   //         ("\t\"" + mounted.location + "\" : { \"" + mounted.mountPoint + "\" : [] }\n");
                   // }
                }
            }

            string managerConfig = "";
            prefabs.Add(gameObject);
            foreach (GameObject o in prefabs) {
                string prefabName = o.name;
                string baseName = prefabName.Replace("Prefab", "");
                string configName = prefabName.Replace("Prefab", "Config");

                Transform attach = o.transform.GetChild(0);
                Transform mesh = attach.GetChild(0);

                List<string> prefabMountPoints = new List<string>();
                foreach (Transform m in mesh) {
                    prefabMountPoints.Add(m.name);
                }

                // config
                string mountsString = "";
                foreach (string mountPointName in prefabMountPoints) {
                    mountsString += "\t{\n\t\ttype:\"Component\",\n\t\t\"nane\":\"" + mountPointName + "\",\n\t\t\"bone\":\"" + mountPointName + "\",\n\t\t\"path\":\"" + mesh.name + "\"\n\t},\n";
                }
                string configString = "{\n\t\"name\":\"" + baseName + "\",\n\t\"bone\":\"" + attach.name + "\",\n\t\"mountPoints\":[\n" + mountsString + "\n\t]\n}";
                System.IO.File.WriteAllText("O:\\!\\" + configName + ".json", configString);

                // manager
                managerConfig += "\t{\n\t\t\"name\":\"" + baseName + "\",\n\t\t\"scriptName\":\"" + configName + "\",\n\t\t\"prefabName\":\"" + prefabName + "\"\n\t},\n";

                // mounts
                string locationName = baseName + "Section";
                mounts += "\t\"" + locationName + "\" : { ";
                foreach (string mountPointName in prefabMountPoints) {
                    mounts += "\t\t\"" + mountPointName + "\" : { \"name\":\"placeholder\" },\n";
                }
                mounts += "},\n";

            }

            // Example #2: Write one string to a text file. 
            

            System.IO.File.WriteAllText(@"O:\!\_BossShip.json", "{ \"hull\" : {\n"+hull+"\n}\n, \n \"mounts\": {\n"+mounts+"\n}\n}");
            System.IO.File.WriteAllText("O:\\!\\_Manager.json", managerConfig);

            alreadyRun = true;
        }
	}

    public string getMountedComponent(Transform node) {
        string nodeBaseName = node.name.Replace("Mount", "") + "Attach";

        Transform child = node.FindChild( nodeBaseName );
        if (child != null)
            return child.name;
        else
            return null;
    }

    public void splitGameobjects(Transform node) {
        foreach (Transform child in node)
        {
            if (child.name.Contains("Attach"))
            {
                Debug.LogWarning(node.name + ": deattaching at " + child.name);
                deattachPrefab(child);
            }
            else
            {
                if (child.name.Contains("Mount"))
                {
                    Debug.LogWarning(node.name + ": mount point at " + child.name);
                    string mountedComponent = getMountedComponent(child);

                    if (mountedComponent != null)
                    {
                        string location = child.name.Replace("Mount", "Section");
                        string parent = node.name;
                        string mountPoint = child.name;
                        string componentName = (mountedComponent != null) ? mountedComponent.Replace("Attach", "Prefab") : null;

                        mountedComponent m = new mountedComponent(location, parent, mountPoint, componentName);
                        mountPoints.Add(child.name, m);
                    }
                }
                else
                {
                    Debug.Log(node.name + ": skipping " + child.name);
                }
            }

            splitGameobjects(child);
        }
    }


    List<GameObject> prefabs = new List<GameObject>();
    public void deattachPrefab(Transform attachPoint) {
        GameObject prefab = new GameObject();
        prefab.name = attachPoint.name.Replace("Attach", "Prefab");
        prefab.transform.position = attachPoint.position;

        attachPoint.parent = prefab.transform;
        prefab.AddComponent<ShipComponent>();

        prefabs.Add(prefab);
    }

    public void listBones(Transform node, string path = null)
    {
        string thisPath = null;

        if (path == null)
        {
            thisPath = "{root} :: [" + node.name + "]";
        }
        else
        {
            thisPath = path + " :: [" + node.name + "]";
        }

        Debug.Log("ListBones:\t" + thisPath, node);
        foreach (Transform child in node)
        {
            listBones(child, thisPath);
        }
    }
}
