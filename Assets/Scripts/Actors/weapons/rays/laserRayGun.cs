﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class laserRayGun : rayGun {
	
	protected float beamWidthBase = 2f;
	protected float beamWidth = 15f;
	protected float beamDuration = 0.5f;

	protected LineRenderer beam = null;
	
	public override JSONMonoBehaviour applyConfig(JSONClass config)
	{
		base.applyConfig (config);

		beam = beamBase.gameObject.AddComponent<LineRenderer>();
		beam.material = new Material(Shader.Find("Particles/Additive"));
		beam.SetColors(Color.red, Color.yellow);
		beam.SetWidth (1.5f, 10f);
		//beam.useWorldSpace = false;
		
		beam.SetPosition (0, beamBase.transform.position);
		beam.SetPosition (1, beamBase.transform.position);
		
		initialized = true;
		
		return this;
	}

	protected override void updateBeam(){
		if ( canFire() ) {

			beam.enabled = false;

		} else if( nextFire > 0f ) {

			float timeUntilNextShot = nextFire - Time.time;
			float timeBetweenShots = 1f/rateOfFire;
			float beamTime = timeBetweenShots * beamDuration;

			if( beamTime <= timeUntilNextShot ){
				beam.enabled = true;
				
				beam.SetWidth (beamWidthBase, beamWidth * (beamTime / timeBetweenShots) );
				beam.SetPosition (0, muzzleBone.position);
				
				if( target != null ){
					//beam.SetPosition (1, muzzleBone.position + muzzleBone.up * hit.distance	);				
					if( target.activeSelf )
						beam.SetPosition (1, target.transform.position);
					else
						beam.SetPosition (1, muzzleBone.position + muzzleBone.up * detectRange);

					target.SendMessageUpwards("handleImpact");
				} else {				
					beam.SetPosition (1, muzzleBone.position + muzzleBone.up * detectRange);
				}
			} else {
				beam.enabled = false;
			}
		}
		
	}
}
