﻿using UnityEngine;
using System.Collections;

public class AsteroidsManager : MonoBehaviour 
{
    public static Transform staticAsteroidPrefab = null;
    public Transform asteroidPrefab = null;
	AsteroidBehaviour[] asteroidScripts;

	int asteroidIterator;
	public bool isActive;

	public void OnEnable()
	{
		Init ();
	}

	public void Init()
	{
		if(asteroidScripts == null)
			asteroidScripts = new AsteroidBehaviour[25];

        if (asteroidPrefab == null)
            asteroidPrefab = staticAsteroidPrefab;

		for(int i = 0; i < asteroidScripts.Length; i++)
		{
			Transform asteroid = (Transform)Instantiate(asteroidPrefab, new Vector3(0, 0, 0), Quaternion.identity);
			asteroidScripts[i] = asteroid.GetComponent<AsteroidBehaviour>();
			asteroidScripts[i].gameObject.SetActive(false);
            asteroidScripts[i].manager = this;
		}

		asteroidIterator = 0;
	}

    public bool IsNextAsteroidAvailable()
	{
		if(asteroidScripts[asteroidIterator].gameObject.activeSelf == false)
			return true;

		return false;	
	}

    public AsteroidBehaviour findNextavailableAsteroid() {
        for (int i = 0; i < asteroidScripts.Length; i++)
        {
            if (asteroidScripts[asteroidIterator].gameObject.activeSelf == false)
                return asteroidScripts[asteroidIterator];

            if (asteroidIterator + 1 < asteroidScripts.Length)
                asteroidIterator++;
            else
                asteroidIterator = 0;
        }
        return null;
    }

    public AsteroidBehaviour getNextAsteroid()
    {
        AsteroidBehaviour nextAsteroid = null;

        // czy nastepny jest wolny? Jak nie to trzeba szukac.
        if ( IsNextAsteroidAvailable() )
            nextAsteroid = asteroidScripts[asteroidIterator];
        else if ((nextAsteroid = findNextavailableAsteroid()) == null)
            return null;

        // przesuwamy licznik
        if (asteroidIterator + 1 < asteroidScripts.Length)
            asteroidIterator++;
        else
            asteroidIterator = 0;

        // przygotowujemy asteroid do użycia
        nextAsteroid.transform.rotation = Quaternion.identity;

        return nextAsteroid;
    }

	public void ActivateAsteroid_RandomPosition()
	{
		if(!isActive)
			return;

        AsteroidBehaviour asteroid;
        if ((asteroid = getNextAsteroid()) != null)
        {
            asteroid.GenerateRandomParameters();
            asteroid.gameObject.SetActive(true);
        }
	}

	public void ActivateAsteroid_ChosenParameters(Vector3 position, Vector3 _movementVector)
	{
		if(!isActive)
			return;

        AsteroidBehaviour asteroid;
        if ((asteroid = getNextAsteroid()) != null)
        {
            asteroid.SetCustomParameters(position, _movementVector);
            asteroid.gameObject.SetActive(true);
        }
	}
}
