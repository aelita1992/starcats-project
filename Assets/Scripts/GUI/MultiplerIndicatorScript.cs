﻿using UnityEngine;
using System.Collections;

public class MultiplerIndicatorScript : MonoBehaviour
{
	float timer;

	void Start()
	{
		timer = 5;
	}

	public void ResetTimer()
	{
		timer = 5;
	}

	public void ReduceTimer(float value)
	{
		timer -= value;
	}

	void OnGUI()
	{
		GUI.backgroundColor = Color.red;
		GUI.HorizontalScrollbar (new Rect (875, 65, 60, 20), 0, timer, 0, 5);
	}
}
