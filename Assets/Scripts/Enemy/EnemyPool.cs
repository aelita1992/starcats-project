﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyPool 
{
	private static EnemyPool singleton;
	private Stack<GameObject> markerObjects;

	GameObject prefab;

	public static EnemyPool Get()
	{
		if(singleton == null)
			singleton = new EnemyPool();

		return singleton;
	}

	private EnemyPool()
	{
		markerObjects = new Stack<GameObject>();
		prefab = Resources.Load("shipMarker") as GameObject;

		for(int i = 0; i < 300; i++)
		{
            GameObject instance = GameObject.Instantiate(prefab) as GameObject;
            shipSpawner marker = instance.GetComponent<shipSpawner>();
            marker.shipAssetName = "EnemyShip";
            marker.init();

            ShipScript shipScript = marker.ship;
            if (shipScript != null)
            {
                markerObjects.Push(shipScript.gameObject);
                shipScript.gameObject.SetActive(false);
            }
		}
	}

	public GameObject GetShip()
	{
		if(markerObjects.Count <= 0)
			AddSomeMoreShips();

		return markerObjects.Pop ();
	}

	private void AddSomeMoreShips()
	{
		Debug.LogError("Generating more ships");

		for(int i = 0; i < 5; i++)
		{
			GameObject instance = GameObject.Instantiate(prefab) as GameObject;
			ShipScript shipScript = instance.GetComponent<shipSpawner>().ship;
			markerObjects.Push (shipScript.gameObject);
			shipScript.gameObject.SetActive(false);
		}
	}

	public void ReleaseShip(GameObject markerObject)
	{
		//markerObjects.Push (markerObject);
	}

	void Start ()
	{
	
	}

}
