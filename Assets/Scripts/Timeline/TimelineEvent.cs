﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public abstract class TimelineEvent
{
    private static float zPlaneHeight = -65f;
    private static float xOrigin = 0f;
    private static float yOrigin = 200f;
    private static float ySpawn = 600f;
    private static bool logEverything = false;
    private static bool logWarnings = true;

    protected class EnemySpawnEvent : TimelineEvent
    {
        protected float startX = 0f;
        protected float startY = 0f;
        protected string assetName = null;
        protected string assetClass = null;
        protected bool healthBar = false;
		int enemyCount;
		FormationsController.FormationEnum formation;
		FormationsController.Direction direction;
		EnemyMovementManager.MovementType movement;
        Vector2 formationSize = Vector2.zero;

        protected static BossHealthIndicator bossHealth = null;
        protected static shipSpawner markerPrefab = null;

        JSONClass eventData = null;

        protected List<shipSpawner> spawnerObjects = null;
        protected List<GameObject> markerObjects = null;
        protected List<ShipScript> ships = null;

        public EnemySpawnEvent(JSONClass eventConfig)
        {
            if (logEverything) Debug.Log("Create EnemySpawnEvent...");

            if (markerPrefab == null)
            {
                GameObject markerObject = Resources.Load("shipMarker") as GameObject;
                markerPrefab = markerObject.GetComponent<shipSpawner>();
            }

            if (bossHealth == null)
            {
                GameObject bar = new GameObject();
                bar.AddComponent<BossHealthIndicator>();
                bossHealth = bar.GetComponent<BossHealthIndicator>();
                bossHealth.gameObject.SetActive(true);
            }

            eventData = eventConfig;
            initEvent();
        }

        protected void initEvent(){
            spawnerObjects = new List<shipSpawner>();
            markerObjects = new List<GameObject>();
            ships = new List<ShipScript>();

            startX = eventData["startPositionH"].AsFloat;
            startY = eventData["startPositionV"].AsFloat;
            assetName = eventData["enemyType"].Value;
            assetClass = eventData["enemyClass"].Value;
            enemyCount = eventData["enemyCount"].AsInt;

            formation = FormationsController.Get().TranslateFormationString(eventData["formationType"].Value);
            direction = FormationsController.Get().TranslateDirectionString(eventData["formationDirection"].Value);
            movement = EnemyMovementManager.Get().TranslateMovementTypeString(eventData["movementType"].Value);

            JSONArray formationSizeInfo = eventData["formationSize"].AsArray;
            if (formationSizeInfo != null && formationSizeInfo.Count == 2)
            {
                formationSize.x = formationSizeInfo[0].AsFloat;
                formationSize.y = formationSizeInfo[1].AsFloat;
            }
            else
            {
                formationSize = new Vector2(64f, 64f);
            }

            healthBar = eventData["lifeBar"].AsBool;        
        }
		
        public override void runEventSpecificTask()
        {
            if (logEverything) Debug.Log("Run EnemySpawnEvent...");

			for(int i = 0; i < enemyCount; i++)
			{
                //GameObject enemyMarker = EnemyPool.Get ().GetShip();

                shipSpawner marker = GameObject.Instantiate(markerPrefab, new Vector3(Random.Range(-400f, 400f), ySpawn, zPlaneHeight), Quaternion.identity) as shipSpawner;
                marker.instantInit = false;
                marker.shipAssetName = assetName;
                marker.shipAssetType = assetClass;
                marker.init();

                ShipScript ship = marker.ship;
                ship.resetPrefab();                

                if (healthBar && i == 0) {
                    bossHealth.MonitoredShip = ship;
                    bossHealth.gameObject.SetActive(true);
                }

                GameObject enemyDestination = marker.shipDestination;
				enemyDestination.SetActive(true);

                spawnerObjects.Add(marker);
				markerObjects.Add (enemyDestination);
                ships.Add(ship);
			}

			FlightTower.Get().PrepareFlight
			(
					formation,
					movement,
					markerObjects,
					direction,
                    new Vector3(xOrigin + startX, yOrigin + startY, zPlaneHeight),
                    new Vector2(formationSize.x, formationSize.y)
			);

        }

        protected void cleanLists(bool killShips = false){
            for (int i = 0; i < markerObjects.Count; i++)
            {
                if (killShips || ships[i] == null || ships[i].IsAlive == false)
                {
                    // cleanup
                    if (ships[i] != null && ships[i].IsAlive) ships[i].kill();
                    if (spawnerObjects[i] != null) spawnerObjects[i].shutDown();

                    // free
                    GameObject.Destroy(spawnerObjects[i]);
                    GameObject.Destroy(markerObjects[i]);

                    // mark for removal
                    spawnerObjects[i] = null;
                    markerObjects[i] = null;
                    ships[i] = null;
                }
            }

            // remove nulls
            spawnerObjects.RemoveAll(item => item == null);
            markerObjects.RemoveAll(item => item == null);
            ships.RemoveAll(item => item == null);
        }

        public override void updateEventSpecificTask()
        {
            cleanLists(false);

			if(markerObjects.Count <= 0)
				running = false;

            if (timeSinceStart >= maxTime)
            {
                if (killShips)
                {
                    cleanLists(true);

                    if (logWarnings) Debug.LogWarning("Enemies auto-killed (timeout) - event over");
                }

                if (healthBar)
                {
                    bossHealth.MonitoredShip = null;
                }
                running = false;
            }
        }

        public override void forceEnd()
        {
            cleanLists(true);

            if (logWarnings) Debug.LogWarning("Enemies auto-killed (force end) - event over");

            if (healthBar)
            {
                bossHealth.MonitoredShip = null;
            }

            running = false;
        }

        protected override void eventSpecificReset()
        {
            initEvent();
        }
    }
    protected class RoidSpawnEvent : TimelineEvent
    {
        bool isRandom = true;
        int randomCnt = 1;

        Vector3 spawnPos = Vector3.zero;
        Vector3 spawnDir = Vector3.zero;

        string roidConfig = "MidRoid";

        public RoidSpawnEvent(JSONClass eventData)
        {
            if (logEverything) Debug.LogWarning("Roids 2 - event init");

            // event init
            string configName = eventData["config"].Value;
            if (configName != "") roidConfig = configName;

            string spawnType = eventData["type"].Value;

            if (spawnType == "placed")
            {
                float spawnX = eventData["X"].AsFloat;
                float spawnY = eventData["Y"].AsFloat;
                spawnPos = new Vector3(spawnX, 200f + spawnY, zPlaneHeight);

                JSONArray spawnDirParam = eventData["dir"].AsArray;
                if (spawnDirParam != null && spawnDirParam.Count == 3)
                {
                    spawnDir = new Vector3(spawnDirParam[0].AsFloat, spawnDirParam[1].AsFloat, spawnDirParam[2].AsFloat);
                }

                isRandom = false;
            }
            else
            {
                int randomCntParam = eventData["count"].AsInt;
                if (randomCntParam > 0) randomCnt = randomCntParam;
            }
        }

        public override void runEventSpecificTask()
        {
            if (logEverything) Debug.LogWarning("Roids - event start");
            
            if (isRandom)
            {
                for (int i = 1; i <= randomCnt; i++)
                {
                    AsteroidScript roid = AsteroidPool.get().getScript(roidConfig);
                    roid.transform.position = new Vector3(xOrigin, yOrigin, zPlaneHeight);
                    AsteroidScript.moveRoid(roid, Vector3.one * 20f, new Vector3(0f, 5f, 0f) * 20f);
                }
            }
            else
            {
                AsteroidScript roid = AsteroidPool.get().getScript(roidConfig);
                roid.transform.position = spawnPos;
                AsteroidScript.moveRoid(roid, Vector3.one * 20f, spawnDir * 20f);                
            }

        }

        public override void updateEventSpecificTask()
        {
            if (timeSinceStart >= maxTime)
            {
                if (logEverything) Debug.LogWarning("Roids - event over");
                running = false;
            }
        }

        public override void forceEnd()
        {
            running = false;
        }

        protected override void eventSpecificReset()
        {
            throw new System.NotImplementedException();
        }
    }

    protected class RoidSpawnEventOld : TimelineEvent
    {
        protected static AsteroidsManager manager = null;

        bool isRandom = true;
        Vector3 spawnPos = Vector3.zero;
        Vector3 spawnDir = Vector3.zero;

        int randomCnt = 1;

        public RoidSpawnEventOld(JSONClass eventData)
        {
            if (logEverything) Debug.LogWarning("Roids - event init");

            // roid manager init
            if (manager == null)
            {
                string roidPrefabPath = "Prefabs/Asteroid/Roid";
                GameObject roidPrefab = Resources.Load(roidPrefabPath) as GameObject;

                if (logEverything) Debug.LogWarning(roidPrefabPath, roidPrefab);

                GameObject o = new GameObject("ManagerContainer");

                AsteroidsManager.staticAsteroidPrefab = roidPrefab.transform;

                o.AddComponent<AsteroidsManager>();
                manager = o.GetComponent<AsteroidsManager>();
                manager.asteroidPrefab = roidPrefab.transform;

                if (logEverything) Debug.LogWarning("set", manager.asteroidPrefab);

                manager.isActive = true;
                manager.OnEnable();
            }

            // event init
            string spawnType = eventData["type"].Value;

            if (spawnType == "placed")
            {
                float spawnX = eventData["X"].AsFloat;
                float spawnY = eventData["Y"].AsFloat;
                spawnPos = new Vector3(spawnX, 200f + spawnY, zPlaneHeight);

                JSONArray spawnDirParam = eventData["dir"].AsArray;
                if (spawnDirParam != null && spawnDirParam.Count == 3)
                {
                    spawnDir = new Vector3(spawnDirParam[0].AsFloat, spawnDirParam[1].AsFloat, spawnDirParam[2].AsFloat);
                }

                isRandom = false;
            }
            else
            {
                int randomCntParam = eventData["count"].AsInt;
                if (randomCntParam > 0) randomCnt = randomCntParam;
            }
        }

        public override void runEventSpecificTask()
        {
            if (logEverything) Debug.LogWarning("Roids - event start");
            if (isRandom)
            {
                for (int i = 1; i <= randomCnt; i++)
                    manager.ActivateAsteroid_RandomPosition();
            }
            else
            {
                manager.ActivateAsteroid_ChosenParameters(spawnPos, spawnDir);
            }

        }

        public override void updateEventSpecificTask()
        {
            if (timeSinceStart >= maxTime)
            {
                if (logEverything) Debug.LogWarning("Roids - event over");
                running = false;
            }
        }

        public override void forceEnd()
        {
            running = false;
        }

        protected override void eventSpecificReset()
        {
            throw new System.NotImplementedException();
        }
    }

    protected class BonusSpawnEvent : TimelineEvent
    {
        private static string bonusPrefabDir = "Prefabs/Bonus/";
        Vector3 spawnPos = Vector3.zero;
        string prefabName = null;
        GameObject bonusInstance = null;

        private string JSONNameToPrefabName(string name)
        {
            switch (name)
            {
                case "Points":
                    return "Double2";//"DoublePoints";
                case "GodMode":
                    return "GodMode2";//"GodModeBonus";
                case "Repair":
                    return "Repair2";//"RepairBonus";
                case "Weapon":
                    return "WeaponBonus";
            }
            return "RepairBonus";
        }

        public BonusSpawnEvent(JSONClass eventData)
        {
            float spawnX = eventData["X"].AsFloat;
            float spawnY = eventData["Y"].AsFloat;
            string spawnName = eventData["name"].Value;
            
            spawnPos = new Vector3(spawnX,200f+spawnY,zPlaneHeight);
            prefabName = JSONNameToPrefabName(spawnName);
        }

        public override void runEventSpecificTask()
        {
            if (logEverything) Debug.LogWarning("Bonus: " + bonusPrefabDir + prefabName);
            GameObject prefab = Resources.Load(bonusPrefabDir + prefabName) as GameObject;
            if (prefab != null)
            {
                bonusInstance = UnityEngine.GameObject.Instantiate(prefab) as GameObject;
                bonusInstance.transform.position = spawnPos;
            }
            else {
                if (logEverything) Debug.LogError("Error spawning Bonus: " + bonusPrefabDir + prefabName);
            }
        }

        public override void updateEventSpecificTask() {
            if (bonusInstance == null) 
            {
                if (logEverything) Debug.LogWarning("Bonus picekd up - event over");
                running = false; 
            }
            else if (timeSinceStart >= 15f)
            {
                if (logEverything) Debug.LogWarning("Bonus timed out - event over");
                UnityEngine.GameObject.DestroyObject(bonusInstance);
                running = false;
            }
        }

        public override void forceEnd()
        {
            running = false;
        }

        protected override void eventSpecificReset()
        {
            throw new System.NotImplementedException();
        }
    }

    public enum eventType { SPAWN_ENEMY, SPAWN_ROID, SPAWN_BONUS };
    private eventType type;

    public eventType Type
    {
        get { return type; }
        set { type = value; }
    }

    private float startTime = 0f;

    public float StartTime
    {
        get { return startTime; }
        set { startTime = value; }
    }

    private bool started = false;

    public bool Started
    {
        get { return started; }
        set { started = value; }
    }
    private bool running = false;

    public bool Running
    {
        get { return running; }
        set { running = value; }
    }

    public void reset() {
        running = false;
        started = false;
        eventStartTime = 0f;
        eventSpecificReset();
    }
    protected abstract void eventSpecificReset();

    protected float eventStartTime = 0f;
    public void runEvent(float runTime)
    {
        if (logEverything) Debug.Log("Run event@" + runTime);
        started = true;
        running = true;
        eventStartTime = runTime;
        runEventSpecificTask();
    }

    public abstract void forceEnd();

    public abstract void runEventSpecificTask();

    protected float timeSinceStart = 0f;
    protected float minTime;
    protected float maxTime;
    protected bool killShips;
    public void updateEvent(float currentTime)
    {
        timeSinceStart = currentTime - eventStartTime;
        updateEventSpecificTask();
    }

    public abstract void updateEventSpecificTask();

    public static eventType queryEventType(JSONClass eventData)
    {
        string typeName = eventData["type"].Value;
        switch (typeName) { 
            case "spawnEnemy":
                return eventType.SPAWN_ENEMY;
            case "spawnBonus":
                return eventType.SPAWN_BONUS;
            case "spawnRoid":
                return eventType.SPAWN_ROID;
        }

        return eventType.SPAWN_ENEMY;
    }
    public static TimelineEvent parseEventJSON(JSONClass eventData)
    {
        TimelineEvent e = null;
        eventType eType = queryEventType(eventData);
        JSONClass eventOptions = eventData["typeOptions"].AsObject;

        switch (eType) {
            case eventType.SPAWN_ENEMY:
                e = new EnemySpawnEvent(eventOptions);
                break;
            case eventType.SPAWN_ROID:
                e = new RoidSpawnEvent(eventOptions);
                break;
            case eventType.SPAWN_BONUS:
                e = new BonusSpawnEvent(eventOptions);
                break;
        }

        e.StartTime = eventData["startTime"].AsFloat;

        e.minTime = eventData["minTime"].AsFloat;
        e.maxTime = eventData["maxTime"].Value != "" ? eventData["maxTime"].AsFloat : 60f;
        e.killShips = eventData["killShipsOnMaxTime"].Value != "" ? eventData["killShipsOnMaxTime"].AsBool : true;

        e.type = eType;

        if (logEverything) 
            Debug.Log(  "Created event " + eType + 
                        " starting at " + e.StartTime +
                        " duration " + e.minTime + " - " + e.maxTime +
                        " kill on end " + e.killShips);

        return e;
    }

}
