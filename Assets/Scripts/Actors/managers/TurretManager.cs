﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TurretManager : MonoBehaviour {
    List<TurretController2> turrets = new List<TurretController2>();
    GameObject target = null;
    public string targetTag = "Player";

    bool logEverything = false;

    float trackingRadius = 1000f;

    private bool fireOnTarget = true;
    public bool FireOnTarget
    {
        get { return fireOnTarget; }
        set { fireOnTarget = value; UpdateTurretOrders(); }
    }

    private bool fireAtWill = false;
    public bool FireAtWill
    {
        get { return fireAtWill; }
        set { fireAtWill = value; UpdateTurretOrders(); }
    }

    private void UpdateTurretOrders(){
        foreach (TurretController2 turret in turrets)
        {
            turret.fireOnTarget = fireOnTarget;
            turret.autoFire = fireAtWill;
        }
    }

    public int turretsRegistered()
    {
        return turrets.Count;
    }

    public void registerTurret(TurretController2 turret)
    {
        turret.fireOnTarget = true;

        turrets.Add(turret);
        if (logEverything)
            Debug.Log("registerTurret: " + turret.name);

        if (target != null && target.activeSelf)
        {
            turret.setTarget(target);
        }
    }
    public void unregisterTurret(TurretController2 turret)
    {
        turret.fireOnTarget = false;

        turrets.Remove(turret);
        if (logEverything)
            Debug.Log("unregisterTurret: " + turret.name);
    }

    void targetClosestEnemy()
    { 
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, trackingRadius);
            int i = 0;
            GameObject closestEnemy = null;
            float closestEnemyDistance = float.MaxValue;
            float distance = 0;
            while (i < hitColliders.Length)
            {
                if( hitColliders[i].tag == targetTag ){
                    distance = (transform.position - hitColliders[i].transform.position).sqrMagnitude;
                    if (distance < closestEnemyDistance) {
                        closestEnemy = hitColliders[i].gameObject;
                        closestEnemyDistance = distance; 
                    }
                }
                i++;
            }

            if (closestEnemy != null)
            {
                target = closestEnemy;

                if (logEverything)
                    Debug.Log("Targeting: " + closestEnemy.name, closestEnemy);

                foreach (TurretController2 turret in turrets)
                {
                    turret.setTarget(target);
                }
            }
            else
            {
                if (logEverything) Debug.Log("No enemy found...");
            }
    }

    public void updateTarget() {
        if (target == null || target.activeSelf == false || (target.transform.parent != null && target.transform.parent.position.y >= 500))
        {
            targetClosestEnemy();
        }
        /*
        else {
            float distance = (transform.position - target.transform.position).sqrMagnitude;
            if (distance >= 500f) {
                targetClosestEnemy();
            }
        }
        */
           
    }

}
