﻿using UnityEngine;
using System.Collections;

public class SettingsPanel : MonoBehaviour 
{
	public UIButton returnButton;

	private Animator returnButtonAnimator;

	void Awake()
	{
		returnButtonAnimator = returnButton.gameObject.GetComponent<Animator>();
	}

	public void Expand()
	{
		gameObject.SetActive(true);
		returnButtonAnimator.SetBool ("IsFadingIn", true);
	}
	
	public void Collapse()
	{
		returnButtonAnimator.SetBool ("IsFadingOut", true);
		gameObject.SetActive(false);
	}
}
