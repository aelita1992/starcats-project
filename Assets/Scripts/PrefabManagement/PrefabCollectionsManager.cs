﻿using UnityEngine;
using System.Collections;

public class PrefabCollectionsManager : MonoBehaviour 
{
	public enum PrefabType{ASTEROID, ENEMY_SHIP, ENEMY_PROJECTILE, ENEMY_BOLT}
	public GameObject asteroidPrefabsParent;
	public GameObject enemyShips;
	public GameObject enemyProjectilePrefabsParent;
	public GameObject enemyBoltPrefabsParent;

	private static PrefabCollectionsManager collectionsManager;
	public static PrefabCollectionsManager Get()
	{
		return collectionsManager;
	}

	void Awake()
	{
		collectionsManager = this;
	}

	public void AddToManager(PrefabType prefabType, GameObject prefab)
	{
		switch(prefabType)
		{
		case PrefabType.ASTEROID:
			prefab.transform.parent = asteroidPrefabsParent.transform;
			break;

		case PrefabType.ENEMY_PROJECTILE:
			prefab.transform.parent = enemyProjectilePrefabsParent.transform;
			break;

		case PrefabType.ENEMY_BOLT:
			prefab.transform.parent = enemyBoltPrefabsParent.transform;
			break;

		case PrefabType.ENEMY_SHIP:
			prefab.transform.parent = enemyShips.transform;
			break;
		}
	}

}
