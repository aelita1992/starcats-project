﻿using UnityEngine;
using System.Collections;

public class InstantMovementAI : MovementAI
{
    public InstantMovementAI(Vector3 maxVelocity, Vector3 maxAccel, Vector3 maxDecel, float accelRate)
        : base(maxVelocity, maxAccel, maxDecel, accelRate){}

    public override void updateAcceleration()
    {
        if (!updateState()) return;
    }
    public override void updateVelocity()
    {
        if (!updateState()) return;
    }

    public override void applyDeltaToSelf()
    {
        if (!updateState()) return;

        Self.position = Target.position;
    }
}
